package mousefollow;

import guru.bug.game.Game;
import guru.bug.game.GameApplication;
import guru.bug.game.background.VolcanoBackground;
import guru.bug.game.sprite.*;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class MouseFollow extends Game {

    private static final char CH_WALL = '#';

    public static void main(String[] args) {
        GameApplication.launch(new MouseFollow(), args);
    }
    @Override
    public void setup() {
        setBackground(new VolcanoBackground());
        register(CH_WALL, WallSprite::new);
        register('1', DotSprite::new)
                .onInit(d -> d.setColor(DotColor.GREEN))
                .onCollision(ai::halt);
        register('2', DotSprite::new)
                .onInit(d -> d.setColor(DotColor.ORANGE));
        register('3', LockSprite::new);
        register('4', PlateSprite::new)
                .onInit(d -> d.setFood(PlateFood.SAUSAGE));

        register('o', ChickenSprite::new)
                .onCollision(ai::stopXY, CH_WALL)
                .onInit(o -> o.setSpeed(5))
                .onLoop(ai::followMouseXY)
                .onLoop(ai::turnToMouse);

        load("/mousefollow/level.txt");
    }

    @Override
    public void loop() {

    }
}
