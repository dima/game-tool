package arkanoid;

import guru.bug.game.Direction;
import guru.bug.game.Game;
import guru.bug.game.GameApplication;
import guru.bug.game.background.DesertBackground;
import guru.bug.game.sprite.*;
import javafx.scene.input.MouseButton;

import java.util.concurrent.ThreadLocalRandom;

import static guru.bug.game.sprite.BlockMaterial.CITRINE;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class Arkanoid extends Game {


    public static void main(String[] args) {
        GameApplication.launch(new Arkanoid(), args);
    }

    @Override
    public void setup() {
        setBackground(new DesertBackground());

        // Зарегистрировать # как стенку голубого цвета.
        register('#', WallSprite::new)
                .onInit(w -> w.setColor(WallColor.BLUE));

        // Зарегистрировать # как стенку голубого цвета.
        register('+', WallSprite::new)
                .onInit(w -> w.setColor(WallColor.ORANGE));

        // Зарегистрировать w для отображения огня.
        register('w', TrapSprite::new)
                .onInit(t -> t.setMaterial(TrapMaterial.FIRE));

        // x - простой блок из материала AMBER
        register('x', BlockSprite::new)
                .onInit(b -> b.setMaterial(BlockMaterial.AMBER))
                .onCollision(ai::halt, BallSprite.class)
                .onHalt(b ->
                        sprite(CollapseBlastSprite::new, b.getX(), b.getY())
                                .onInit(e -> e.setColor(CollapseBlastColor.RED))
                                .onInit(CollapseBlastSprite::explode))
                .onHalt(this::dropGift);

        // q - простой блок (CITRINE)
        register('q', BlockSprite::new)
                .onInit(b -> b.setMaterial(CITRINE))
                .onCollision(ai::halt, BallSprite.class)
                .onHalt(b ->
                        sprite(CollapseBlastSprite::new, b.getX(), b.getY())
                                .onInit(e -> e.setColor(CollapseBlastColor.GREEN))
                                .onInit(CollapseBlastSprite::explode));

        // y - блок который выедерживает два удара из BUBBLESTONE
        register('y', BlockSprite::new)
                .onInit(b -> b.setMaterial(BlockMaterial.BUBBLESTONE))
                .onCollision(b -> b.activate("crack1"), BallSprite.class)
                .onMessage(b -> b.activate("crack1"), "crack")
                // Определение полу-сломанного состояния
                .defineState("crack1")
                .onActivation(b -> b.setCrackLevel(2))
                .onCollision(ai::halt, BallSprite.class)
                .onMessage(ai::halt, "crack")
                .onHalt(b ->
                        sprite(RoundBlastSprite::new, b.getX(), b.getY())
                                .onInit(e -> e.setColor(RoundBlastColor.GREEN))
                                .onInit(RoundBlastSprite::explode));

        // z - блок выдерживает три удара из METAL
        register('z', BlockSprite::new)
                .onInit(b -> b.setMaterial(BlockMaterial.METAL))
                .onCollision(b -> b.activate("crack1"), BallSprite.class)
                .onMessage(b -> b.activate("crack1"), "crack")
                // Определение состояния "сломанности" на треть
                .defineState("crack1")
                .onActivation(b -> b.setCrackLevel(1))
                .onCollision(b -> b.activate("crack2"), BallSprite.class)
                .onMessage(b -> b.activate("crack2"), "crack")
                // Определение состояния "сломанности" на две трети
                .defineState("crack2")
                .onActivation(b -> b.setCrackLevel(4))
                .onCollision(ai::halt, BallSprite.class)
                .onHalt(b ->
                        sprite(RoundBlastSprite::new, b.getX(), b.getY())
                                .onInit(e -> e.setColor(RoundBlastColor.BLUE))
                                .onInit(RoundBlastSprite::explode));

        // загрузить уровень
        load("/arkanoid/level.txt", this::levelSetup);
    }

    private void levelSetup() {
        // Создать каретку внизу экрана
        sprite(CaretSprite::new, getWidth() / 2, getHeight() - 1.5)
                .onInit(c -> c.setSpeed(20))
                .onLoop(ai::followMouseX)
                .onCollision(ai::stopX, WallSprite.class);

        // метод создающий новый шарик
        newBall();
    }

    private void dropGift(BlockSprite block) {
        ThreadLocalRandom r = ThreadLocalRandom.current();
        if (r.nextInt(5) > 0) {
            return;
        }
        int i = r.nextInt(GemIcon.values().length);
        int c = r.nextInt(GemColor.values().length - 1);
        GemIcon icon = GemIcon.values()[i];
        GemColor color = GemColor.values()[c];
        sprite(GemSprite::new, block.getX(), block.getY())
                .onInit(g -> {
                    g.setColor(color);
                    g.setIcon(icon);
                    g.setDirection(Direction.S);
                    g.setSpeed(7);
                })
                .onLoop(ai::followDirection)
                .onCollision(ai::halt, CaretSprite.class, TrapSprite.class)
                .onCollision(b -> message("crack"), CaretSprite.class);
    }

    // создание нового шара
    private void newBall() {
        sprite(BallSprite::new, getWidth() / 2, getHeight() - 2.5)
                .onInit(b -> {
                    b.setDirection(Direction.NE);
                    b.setRotation(Direction.NE);
                    b.setColor(BallColor.GREEN);
                })
                .onActivation(b -> b.activate("magnet"))
                .defineState("magnet")
                .onActivation(b -> b.setSpeed(20))
                .onLoop(ai::followMouseX)
                .onCollision(ai::stopX)
                .onMouseButtonPressed(b -> {
                    b.deactivate("magnet");
                    b.activate("normal");
                    b.activate("default");
                }, MouseButton.PRIMARY)

                .defineState("normal")
                .onLoop(ai::followDirection)
                .onCollision(ai::bounce, CaretSprite.class, WallSprite.class)
                .onCollision(ai::turnToDirection)
                .onCollision(ai::halt, TrapSprite.class)
                .onHalt(this::ballOut)

                // Определить обычное состояние шарика
                .defineState("default")
                .onActivation(b -> b.setSpeed(15))
                .onMouseButtonPressed(b -> {
                    b.deactivate("default");
                    b.activate("fireball");
                }, MouseButton.PRIMARY)
                .onCollision(ai::bounce, BlockSprite.class)
                // Определить состояние "огненный шар"
                .defineState("fireball")
                .onActivation(b -> b.setSpeed(17))
                .onActivation(b -> b.setFlame(true))
                .onCollision(ai::bounce, 'z')
                .onMouseButtonReleased(b -> {
                    b.deactivate("fireball");
                    b.activate("default");
                }, MouseButton.PRIMARY)
                .onDeactivation(b -> b.setFlame(false));
    }

    // взрыв старого мяча и создание нового
    private void ballOut(BallSprite ball) {
        sprite(DirectedBlastSprite::new, ball.getX(), ball.getY())
                .onInit(e -> e.setRotation(Direction.S))
                .onInit(e -> e.setColor(DirectedBlastColor.YELLOW))
                .onInit(Explosion::explode);
        newBall();
    }

    @Override
    public void loop() {
    }
}
