package guru.bug.game.spriteeditor.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by dima on 11/9/16.
 */
public class DimensionModelTest {
    private final ValueRouter<DoubleProperty, Number> selector = new ValueRouter<>(SimpleDoubleProperty::new);
    private final DimensionModel dm = new DimensionModel();
    private final MeasureModel mm1 = new MeasureModel();
    private final MeasureModel mm2 = new MeasureModel();
    private final MeasureModel mm3 = new MeasureModel();
    private final MeasureModel mm4 = new MeasureModel();

    public DimensionModelTest() {
        dm.getMeasures().addAll(mm1, mm2, mm3, mm4);
        selector.setDimension(dm);
    }

    @Test
    public void measureActivationTest() {
        assertAllInactive();

        selector.valueProperty().set(5.0d);
        selector.ensureSet(mm1);
        dm.setActive(mm1);
        assertCurrentValue(5.0d);
        selector.valueProperty().set(6.0d);
        assertCurrentValue(6.0d);
        assertOnlyActive(mm1);
        selector.ensureSet(mm2);
        assertOnlyActive(mm1);
        dm.setActive(null);
        assertCurrentValue(5.0d);
        assertAllInactive();
        dm.setActive(mm2);
        assertCurrentValue(5.0d);
        selector.setValue(mm3, 20.0d);
        assertCurrentValue(5.0d);
        assertOnlyActive(mm2);
        dm.setActive(mm4);
        assertCurrentValue(5.0d);
        assertOnlyActive(mm4);
        dm.setActive(mm3);
        assertCurrentValue(20.0d);
        selector.setValue(mm3, 10.0d);
        assertCurrentValue(10.0d);
        assertOnlyActive(mm3);
        dm.setActive(null);
        assertCurrentValue(5.0d);
        assertAllInactive();
    }

    private void assertOnlyActive(MeasureModel mm) {
        assertSame(mm, dm.getActive());
        assertEquals(mm == mm1, mm1.isActive());
        assertEquals(mm == mm2, mm2.isActive());
        assertEquals(mm == mm3, mm3.isActive());
        assertEquals(mm == mm4, mm4.isActive());
    }

    private void assertAllInactive() {
        assertNull(dm.getActive());
        assertFalse(mm1.isActive());
        assertFalse(mm2.isActive());
        assertFalse(mm3.isActive());
        assertFalse(mm4.isActive());
    }

    private void assertCurrentValue(double expected) {
        assertEquals(expected, selector.valueProperty().get(), 0.0001);
    }

}