package guru.bug.game;

import org.junit.Test;

import static guru.bug.game.Direction.*;
import static org.junit.Assert.assertSame;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class DirectionTest {

    @Test
    public void subtract() throws Exception {
        assertSame(E, N.subtract(N));
        assertSame(N, N.subtract(E));
        assertSame(W, S.subtract(N));
    }

    @Test
    public void reboundTest() {
        assertSame(W, NE.bounce(N));
        assertSame(S, N.bounce(N));
        assertSame(SW, N.bounce(NW));
        assertSame(NW, S.bounce(SW));
    }

    @Test
    public void findClosestTest() {
        assertSame(E, Direction.findClosest(0));
        assertSame(E, Direction.findClosest(0.1));
        assertSame(ESE, Direction.findClosest(0.2));
        assertSame(W, Direction.findClosest(3));
        assertSame(ENE, Direction.findClosest(5.8));
        assertSame(ENE, Direction.findClosest(6.0));
        assertSame(E, Direction.findClosest(2.0d * Math.PI));
        assertSame(W, Direction.findClosest(3.0d * Math.PI));
        assertSame(W, Direction.findClosest(3.0d * Math.PI + 0.02));
        assertSame(E, Direction.findClosest(2.0d * Math.PI - 0.01));
        assertSame(N, Direction.findClosest(-Math.PI / 2d));
    }

}