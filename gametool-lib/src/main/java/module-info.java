open module gametool.lib {
    exports guru.bug.game;
    exports guru.bug.game.background;
    exports guru.bug.game.sprite;
    exports guru.bug.game.ai;
    requires javafx.controls;
    requires javafx.base;
    requires javafx.fxml;
    requires java.logging;
    requires java.desktop;
    requires javafx.swing;
}