package guru.bug.game;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public abstract class AIBase {
    private Game game;
    private SpriteStateBuilder<Sprite> builder = new SpriteStateBuilder<>();

    void init(Game game) {
        if (this.game == null) {
            this.game = game;
            init(builder);
        }
    }

    protected abstract void init(SpriteStateBuilder<Sprite> builder);

    protected abstract void setup(Sprite sprite);

    protected abstract void loop(Sprite sprite);

    protected final Game getGame() {
        return game;
    }

    SpriteState buildState() {
        return builder.build();
    }

}
