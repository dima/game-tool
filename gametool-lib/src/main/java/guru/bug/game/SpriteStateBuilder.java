package guru.bug.game;

import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class SpriteStateBuilder<T extends Sprite> {
    private final Map<String, StateHandlers> states = new HashMap<>(4);
    private String currentStateName = SpriteState.ROOT;
    private StateHandlers currentStateHandlers = getOrCreateStateHandlers(currentStateName);

    private StateHandlers getOrCreateStateHandlers(String currentStateName) {
        return states.computeIfAbsent(currentStateName, k -> new StateHandlers());
    }

    public final SpriteStateBuilder<T> defineState(String stateName) {
        if (stateName.equals(this.currentStateName)) {
            return this;
        }
        this.currentStateName = stateName;
        this.currentStateHandlers = getOrCreateStateHandlers(stateName);
        return this;
    }

    public final SpriteStateBuilder<T> onInit(Consumer<T> handler) {
        //noinspection unchecked
        currentStateHandlers.onInit((Consumer<Sprite>) handler);
        return this;
    }

    public final SpriteStateBuilder<T> onKeyHold(Consumer<T> handler, KeyCode keyCode) {
        //noinspection unchecked
        currentStateHandlers.onKeyHold((Consumer<Sprite>) handler, keyCode);
        return this;
    }

    public final SpriteStateBuilder<T> onKeyPressed(Consumer<T> handler, KeyCode keyCode) {
        //noinspection unchecked
        currentStateHandlers.onKeyPressed((Consumer<Sprite>) handler, keyCode);
        return this;
    }

    public final SpriteStateBuilder<T> onKeyReleased(Consumer<T> handler, KeyCode keyCode) {
        //noinspection unchecked
        currentStateHandlers.onKeyReleased((Consumer<Sprite>) handler, keyCode);
        return this;
    }

    public final SpriteStateBuilder<T> onMouseButtonHold(Consumer<T> handler, MouseButton mouseButton) {
        //noinspection unchecked
        currentStateHandlers.onMouseButtonHold((Consumer<Sprite>) handler, mouseButton);
        return this;
    }

    public final SpriteStateBuilder<T> onMouseButtonPressed(Consumer<T> handler, MouseButton mouseButton) {
        //noinspection unchecked
        currentStateHandlers.onMouseButtonPressed((Consumer<Sprite>) handler, mouseButton);
        return this;
    }

    public final SpriteStateBuilder<T> onMessage(Consumer<T> handler, String message) {
        //noinspection unchecked
        currentStateHandlers.onMessage((Consumer<Sprite>) handler, message);
        return this;
    }

    public final SpriteStateBuilder<T> onMouseButtonReleased(Consumer<T> handler, MouseButton mouseButton) {
        //noinspection unchecked
        currentStateHandlers.onMouseButtonReleased((Consumer<Sprite>) handler, mouseButton);
        return this;
    }

    public final SpriteStateBuilder<T> onLoop(Consumer<T> handler) {
        //noinspection unchecked
        currentStateHandlers.onLoop((Consumer<Sprite>) handler);
        return this;
    }

    public final SpriteStateBuilder<T> onInteraction(Consumer<T> handler) {
        //noinspection unchecked
        currentStateHandlers.onInteraction((Consumer<Sprite>) handler);
        return this;
    }

    public final SpriteStateBuilder<T> onTimer(Consumer<T> handler) {
        //noinspection unchecked
        currentStateHandlers.onTimer((Consumer<Sprite>) handler);
        return this;
    }

    public final SpriteStateBuilder<T> onActivation(Consumer<T> handler) {
        //noinspection unchecked
        currentStateHandlers.onActivation((Consumer<Sprite>) handler);
        return this;
    }

    public final SpriteStateBuilder<T> onDeactivation(Consumer<T> handler) {
        //noinspection unchecked
        currentStateHandlers.onDeactivation((Consumer<Sprite>) handler);
        return this;
    }

    @SafeVarargs
    public final SpriteStateBuilder<T> onCollision(BiConsumer<T, CollisionSet> handler, Class<? extends Sprite>... collidedClasses) {
        //noinspection unchecked
        currentStateHandlers.onCollision((BiConsumer<Sprite, CollisionSet>) handler, collidedClasses);
        return this;
    }

    @SafeVarargs
    public final SpriteStateBuilder<T> onCollision(Consumer<T> handler, Class<? extends Sprite>... collidedClasses) {
        //noinspection unchecked
        currentStateHandlers.onCollision((Consumer<Sprite>) handler, collidedClasses);
        return this;
    }

    public final SpriteStateBuilder<T> onCollision(BiConsumer<T, CollisionSet> handler, char... symbols) {
        //noinspection unchecked
        currentStateHandlers.onCollision((BiConsumer<Sprite, CollisionSet>) handler, symbols);
        return this;
    }

    public final SpriteStateBuilder<T> onCollision(BiConsumer<T, CollisionSet> handler, Collection<Character> symbols) {
        //noinspection unchecked
        currentStateHandlers.onCollision((BiConsumer<Sprite, CollisionSet>) handler, symbols);
        return this;
    }

    public final SpriteStateBuilder<T> onCollision(BiConsumer<T, CollisionSet> handler) {
        //noinspection unchecked
        currentStateHandlers.onCollision((BiConsumer<Sprite, CollisionSet>) handler, (char[]) null);
        return this;
    }

    public final SpriteStateBuilder<T> onCollision(Consumer<T> handler, char... symbols) {
        //noinspection unchecked
        currentStateHandlers.onCollision((Consumer<Sprite>) handler, symbols);
        return this;
    }

    public final SpriteStateBuilder<T> onCollision(Consumer<T> handler) {
        //noinspection unchecked
        currentStateHandlers.onCollision((Consumer<Sprite>) handler, (char[]) null);
        return this;
    }

    public final SpriteStateBuilder<T> onHalt(Consumer<T> handler) {
        //noinspection unchecked
        currentStateHandlers.onHalt((Consumer<Sprite>) handler);
        return this;
    }

    public final SpriteStateBuilder<T> onEvent(Consumer<T> handler, CustomEvent event) {
        //noinspection unchecked
        currentStateHandlers.onEvent((Consumer<Sprite>) handler, event);
        return this;
    }

    SpriteState build() {
        return new SpriteState(states);
    }
}
