package guru.bug.game;

import javafx.scene.input.KeyCode;

import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class KeyboardHandlerInfo {
    private final Consumer<Sprite> handler;
    private final KeyCode keyCode;

    KeyboardHandlerInfo(Consumer<Sprite> handler, KeyCode keyCode) {
        this.handler = handler;
        this.keyCode = keyCode;
    }

    KeyCode getKeyCode() {
        return keyCode;
    }

    Consumer<Sprite> getHandler() {
        return handler;
    }

}
