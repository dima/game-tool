package guru.bug.game;

import guru.bug.game.utils.Pool;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

import java.util.*;
import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class SpriteRegistry {
    private final Queue<Runnable> scheduledOperations = new LinkedList<>();
    private final List<Sprite> sprites = new ArrayList<>(512);
    private final List<Node> outlines = new ArrayList<>(1024);
    private final CollisionSetRoot collisionSet = new CollisionSetRoot();
    private final Pool<Bumper> bumperPool = new Pool<>(Bumper::new);
    private final Pool<Node> nodePool = new Pool<>(Node::new);
    private boolean sorted = true;

    void add(List<Sprite> sprites) {
        this.sprites.addAll(sprites);
        sprites.forEach(s -> s.init(this));
        sorted = false;
    }

    void scheduleOperation(Runnable operation) {
        scheduledOperations.add(operation);
    }

    void scheduleAddSprite(SpriteFactory<Sprite> factory) {
        scheduledOperations.add(() -> {
            Sprite newSprite = factory.create();
            this.sprites.add(newSprite);
            newSprite.init(this);
            sorted = false;
        });
    }

    void doLoop() {
        bumperPool.reset();
        outlines.clear();
        nodePool.reset();
        doScheduled();
        sortSprites();

        forEachActiveSprite(s -> {
            Node node = nodePool.get();
            node.reset(s);
            outlines.add(node);
            s.doLoop();
            AIBase ai = s.getAI();
            if (ai != null) {
                ai.loop(s);
            }
        });
    }

    private void forEachActiveSprite(Consumer<Sprite> consumer) {
        sprites.stream()
                .filter(s -> !s.isInactive())
                .forEach(consumer);
    }

    void doInteractive() {
        forEachActiveSprite(Sprite::doInteractive);
    }

    private void sortSprites() {
        if (sorted) {
            return;
        }
        sprites.sort(Comparator.comparingInt(Sprite::getLayer));
        sorted = true;
    }

    private void doScheduled() {
        while (!scheduledOperations.isEmpty()) {
            Runnable op = scheduledOperations.remove();
            op.run();
        }
    }

    void doCollisions() {
        updateCollisions();
        processCollisions();
        sprites.forEach(Sprite::processHalt);
    }

    void updateCollisions() {
        collisionSet.clear();
        findCollisions();
    }

    void doTimer() {
        forEachActiveSprite(Sprite::doTimer);
    }

    private void processCollisions() {
        sprites.stream()
                .filter(s -> !s.isInactive())
                .forEach(s -> s.doCollisions(collisionSet));
    }

    private void findCollisions() {
        for (Node node1 : outlines) {
            Sprite sprite1 = node1.getSprite();
            if (sprite1.getCollisionDetectionType() != CollisionDetectionType.ACTIVE || sprite1.isInactive()) {
                continue;
            }
            node1.markTested();
            for (Node node2 : outlines) {
                if (node2.isTested()) {
                    continue;
                }
                Sprite sprite2 = node2.getSprite();
                if (sprite2.getCollisionDetectionType() == CollisionDetectionType.NONE || sprite2.isInactive()) {
                    continue;
                }
                sprite1.findCollisions(sprite2,
                        (b1, b2) -> collisionSet.add(sprite1, b1, sprite2, b2), bumperPool);
            }
        }
    }

    CollisionSet findCollisions(Sprite sprite1) {
        CollisionSetRoot result = new CollisionSetRoot();
        for (Node node2 : outlines) {
            Sprite sprite2 = node2.getSprite();
            if (sprite1 == sprite2) {
                continue;
            }
            if (sprite2.getCollisionDetectionType() == CollisionDetectionType.NONE || sprite2.isInactive()) {
                continue;
            }
            sprite1.findCollisions(sprite2,
                    (b1, b2) -> result.add(sprite1, b1, sprite2, b2), bumperPool);
        }
        return result;
    }

    void removeHalted() {
        Iterator<Sprite> i = sprites.iterator();
        while (i.hasNext()) {
            Sprite s = i.next();
            s.processHalt();
            if (s.isHalted()) {
                i.remove();
            }
        }
    }

    void draw(GraphicsContext gc) {
        boolean debug = GameEngine.isDebug();
        sprites.forEach(s -> s.draw(gc, debug));
    }

    void clear() {
        sprites.forEach(Sprite::silentHalt);
        sprites.clear();
        outlines.clear();
        collisionSet.clear();
        bumperPool.reset();
        nodePool.reset();
    }

    void doKeys(Set<KeyCode> keysHold, Set<KeyCode> keysPressed, Set<KeyCode> keysReleased) {
        sprites.stream()
                .filter(s -> !s.isInactive())
                .forEach(s -> s.doKeys(keysHold, keysPressed, keysReleased));
    }

    void doMouseButtons(Set<MouseButton> buttonsHold, Set<MouseButton> buttonsPressed, Set<MouseButton> buttonsReleased) {
        sprites.stream()
                .filter(s -> !s.isInactive())
                .forEach(s -> s.doMouseButtons(buttonsHold, buttonsPressed, buttonsReleased));
    }

    void doMessage(String message) {
        sprites.stream()
                .filter(s -> !s.isInactive())
                .forEach(s -> s.doMessage(message));
    }

    boolean hasCollision(Sprite sprite, String area) {
        return collisionSet.subsetBySprite(sprite, area).collisionStream().findAny().isPresent();
    }

    CollisionSet getCollisions() {
        return collisionSet;
    }

    private static class Node {
        private Sprite sprite;
        private boolean tested;

        void reset(Sprite sprite) {
            this.sprite = sprite;
            tested = false;
        }

        void markTested() {
            tested = true;
        }

        boolean isTested() {
            return tested;
        }

        Sprite getSprite() {
            return sprite;
        }
    }
}
