package guru.bug.game;

import guru.bug.game.utils.ClassScanner;
import javafx.application.Application;
import javafx.scene.CacheHint;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;


/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class GameApplication extends Application {

    static Game game;

    public static void main(String[] args) {
        GameApplication.launch(args);
    }

    public static void launch(Game game, String[] args) {
        GameApplication.game = game;
        GameApplication.launch(args);
    }

    @Override
    public void start(Stage stage) {
        if (game == null) {
            game = ClassScanner.findGameImplementation();
        }
        GameEngine gameEngine = new GameEngine(game);
        Scene scene = new Scene(gameEngine);
        gameEngine.bindSize(scene.widthProperty(), scene.heightProperty());
        stage.setTitle("My Game Application");
        gameEngine.setCacheHint(CacheHint.SPEED);
        gameEngine.setCache(false);
        stage.setScene(scene);
        stage.setWidth(1088);
        stage.setHeight(768);
        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        stage.setFullScreenExitHint("Press F11 to exit full-screen mode.");
        scene.setOnKeyPressed(e -> {
            if (e.getCode() == KeyCode.F11) {
                stage.setFullScreen(!stage.isFullScreen());
            } else {
                gameEngine.keyPressed(e.getCode());
            }
        });
        scene.setOnKeyReleased(e -> {
            if (e.getCode() != KeyCode.F11) {
                gameEngine.keyReleased(e.getCode());
            }
        });
        scene.setOnMousePressed(e -> gameEngine.mousePressed(e.getButton()));
        scene.setOnMouseReleased(e -> gameEngine.mouseReleased(e.getButton()));
        String debug = getParameters().getNamed().get("debug");
        GameEngine.setDebug("true".equals(debug));
        gameEngine.start();
        stage.show();
    }
}
