package guru.bug.game;

import guru.bug.game.utils.Pool;
import javafx.beans.property.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public abstract class Sprite {
    private CollisionDetectionType collisionDetectionType = CollisionDetectionType.ACTIVE;
    private final IntegerProperty layer = new SimpleIntegerProperty();
    private final ReadOnlyDoubleWrapper x = new ReadOnlyDoubleWrapper();
    private final ReadOnlyDoubleWrapper y = new ReadOnlyDoubleWrapper();
    private final DoubleProperty targetX = new SimpleDoubleProperty();
    private final DoubleProperty targetY = new SimpleDoubleProperty();
    private final ObjectProperty<Direction> direction = new SimpleObjectProperty<>(Direction.N);
    private final ObjectProperty<Direction> rotation = new SimpleObjectProperty<>(Direction.E);
    private final DoubleProperty speed = new SimpleDoubleProperty();
    private final SpritePainter painter;
    private final SimpleTimer timer = new SimpleTimer();
    private final UserData data = new UserData();
    private SpritePainter.State spritePainterState;
    private SpriteState stateHolder;
    private boolean halted;
    private boolean halting;
    private Character symbol;
    private SpriteRegistry registry;
    private AIBase ai;

    protected Sprite() {
        Painter p = this.getClass().getAnnotation(Painter.class);
        String path = p.value();
        this.painter = SpritePainterCache.get(path);
    }

    SpritePainter getPainter() {
        return painter;
    }

    private SpritePainter.State state() {
        if (spritePainterState == null) {
            this.spritePainterState = painter.createState(x, y);
        }
        return spritePainterState;
    }

    protected abstract void setPosCell(double col, double row);

    final void init(SpriteState state) {
        this.stateHolder = state;
    }

    final void init(SpriteRegistry registry) {
        this.registry = registry;
    }

    final void init(AIBase ai) {
        this.ai = ai;
    }

    protected abstract void setup();

    final void init(Character symbol) {
        this.symbol = symbol;
    }

    protected final int getPainterState(int dimension) {
        return state().getState(dimension);
    }

    protected final IntegerProperty painterStateProperty(int dimension) {
        return state().stateProperty(dimension);
    }

    protected final void setPainterState(int dimension, int value) {
        state().setState(dimension, value);
    }

    protected abstract void loop();

    public void activate(String stateName) {
        stateHolder.scheduleActivate(stateName);
    }

    public void deactivate(String stateName) {
        stateHolder.scheduleDeactivate(stateName);
    }

    public void switchActive(String deactivate, String activate) {
        deactivate(deactivate);
        activate(activate);
    }

    public boolean isActivated(String stateName) {
        return stateHolder.isActivated(stateName);
    }

    public void startTimer(double seconds) {
        timer.set(seconds);
    }

    public void stopTimer() {
        timer.stop();
    }

    void doLoop() {
        stateHolder.updateActive(this);
        loop();
        stateHolder.doLoop(this);
    }

    void doInteractive() {
        stateHolder.doInteraction(this);
    }

    void doTimer() {
        if (timer.isAlarmed()) {
            timer.stop();
            stateHolder.doTimer(this);
        }
    }

    void doCollisions(CollisionSetRoot collisionSet) {
        stateHolder.doCollisions(this, collisionSet);
    }

    void doKeys(Set<KeyCode> keysHold, Set<KeyCode> keysPressed, Set<KeyCode> keysReleased) {
        stateHolder.doKeys(this, keysHold, keysPressed, keysReleased);
    }

    void doMouseButtons(Set<MouseButton> buttonsHold, Set<MouseButton> buttonsPressed, Set<MouseButton> buttonsReleased) {
        stateHolder.doMouseButtons(this, buttonsHold, buttonsPressed, buttonsReleased);
    }

    void doMessage(String message) {
        stateHolder.doMessage(this, message);
    }

    protected final void doEvent(CustomEvent event) {
        stateHolder.doEvent(this, event);
    }

    void draw(GraphicsContext gc, boolean debug) {
        state().draw(gc, debug);
    }

    void findCollisions(Sprite other, BiConsumer<Bumper, Bumper> collisionConsumer, Pool<Bumper> bumperPool) {
        state().findCollisions(other.state(), collisionConsumer, bumperPool);
    }

    public void halt() {
        halting = true;
    }

    protected final boolean hasCollision(String area) {
        return registry.hasCollision(this, area);
    }

    protected final CollisionSet getCollisions(Character... ch) {
        return registry.getCollisions().subsetBySpriteAndSymbols(this, null, Arrays.asList(ch));
    }

    public boolean hasCollision(Character... ch) {
        return registry.getCollisions().subsetBySpriteAndSymbols(this, null, Arrays.asList(ch)).hasCollisions();
    }

    public boolean hasCollision(List<Character> ch) {
        return registry.getCollisions().subsetBySpriteAndSymbols(this, null, ch).hasCollisions();
    }

    final void silentHalt() {
        halted();
    }

    protected abstract void halted();

    void processHalt() {
        boolean oldHalt = halted;
        halted = isInactive();
        if (!oldHalt && halted) {
            stateHolder.doHalt(this);
            silentHalt();
        }
    }

    boolean isHalted() {
        return halted;
    }

    boolean isInactive() {
        return halted || halting;
    }

    public CollisionDetectionType getCollisionDetectionType() {
        return collisionDetectionType;
    }

    public void setCollisionDetectionType(CollisionDetectionType collisionDetectionType) {
        this.collisionDetectionType = collisionDetectionType;
    }

    public int getLayer() {
        return layer.get();
    }

    public IntegerProperty layerProperty() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer.set(layer);
    }

    public double getX() {
        return x.get();
    }

    public ReadOnlyDoubleProperty xProperty() {
        return x.getReadOnlyProperty();
    }

    public void setX(double x) {
        this.x.set(x);
    }

    public double getY() {
        return y.get();
    }

    public ReadOnlyDoubleProperty yProperty() {
        return y.getReadOnlyProperty();
    }

    public void setY(double y) {
        this.y.set(y);
    }

    public double getTargetX() {
        return targetX.get();
    }

    public DoubleProperty targetXProperty() {
        return targetX;
    }

    public void setTargetX(double targetX) {
        this.targetX.set(targetX);
    }

    public double getTargetY() {
        return targetY.get();
    }

    public DoubleProperty targetYProperty() {
        return targetY;
    }

    public void setTargetY(double targetY) {
        this.targetY.set(targetY);
    }

    public Direction getDirection() {
        return direction.get();
    }

    public ObjectProperty<Direction> directionProperty() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction.set(direction);
    }

    public double getSpeed() {
        return speed.get();
    }

    public DoubleProperty speedProperty() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed.set(speed);
    }

    public Direction getRotation() {
        return rotation.get();
    }

    public ObjectProperty<Direction> rotationProperty() {
        return rotation;
    }

    public void setRotation(Direction rotation) {
        this.rotation.set(rotation);
    }

    Character getSymbol() {
        return symbol;
    }

    AIBase getAI() {
        return ai;
    }

    public UserData data() {
        return data;
    }
}
