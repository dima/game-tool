package guru.bug.game;

import java.util.stream.Stream;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
abstract class CollisionSetChild extends CollisionSet {
    private final CollisionSet parent;

    @Override
    Stream<Collision> collisionStream() {
        return parent.collisionStream();
    }

    CollisionSetChild(CollisionSet parent) {
        this.parent = parent;
    }
}
