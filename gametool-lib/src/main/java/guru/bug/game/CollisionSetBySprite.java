package guru.bug.game;

import java.util.Objects;
import java.util.stream.Stream;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class CollisionSetBySprite extends CollisionSetChild {
    private final Sprite party;
    private final String area;

    CollisionSetBySprite(CollisionSet parent, Sprite party) {
        this(parent, party, null);
    }

    CollisionSetBySprite(CollisionSet parent, Sprite party, String area) {
        super(parent);
        this.party = party;
        this.area = area;
    }

    @Override
    protected Stream<Collision> collisionStream() {
        return super.collisionStream()
                .filter(c -> c.party(party))
                .filter(c -> Objects.equals(c.getParty().getBumper().getAreaName(), area));
    }


}
