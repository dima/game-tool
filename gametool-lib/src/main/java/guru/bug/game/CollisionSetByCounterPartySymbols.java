package guru.bug.game;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class CollisionSetByCounterPartySymbols extends CollisionSetChild {
    private final Set<Character> counterparty;

    CollisionSetByCounterPartySymbols(CollisionSet parent, Collection<Character> counterparty) {
        super(parent);
        this.counterparty = new HashSet<>(counterparty);
    }

    @Override
    protected Stream<Collision> collisionStream() {
        return super.collisionStream()
                .filter(c -> counterparty.contains(c.getCounterparty().getSprite().getSymbol()));
    }


}
