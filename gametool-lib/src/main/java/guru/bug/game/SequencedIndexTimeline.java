package guru.bug.game;

import javafx.beans.binding.IntegerBinding;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class SequencedIndexTimeline extends IntegerBinding {
    private static final double NANOS_IN_SEC = 1_000_000_000;
    private final int[] frames;
    private long stepDuration;
    private long zeroTime;
    private int currentIndex;
    private final DoubleProperty period = new SimpleDoubleProperty() {
        @Override
        protected void invalidated() {
            double value = get();
            if (value <= 0) {
                return;
            }
            long oldStepDuration = stepDuration;
            stepDuration = (long) (value * NANOS_IN_SEC / frames.length);
            zeroTime = GameEngine.getNow();
            if (oldStepDuration > 0) {
                long time = GameEngine.getNow() - zeroTime;
                long oldTimeOfs = stepDuration == 0 ? 0 : time % stepDuration;
                zeroTime -= oldTimeOfs * stepDuration / oldStepDuration + stepDuration * currentIndex;
            }
        }
    };

    public SequencedIndexTimeline(int[] frames) {
        this(0.0, frames);
    }

    public SequencedIndexTimeline(double period, int[] frames) {
        this.zeroTime = GameEngine.getNow();
        this.frames = frames;
        this.period.setValue(period);
        bind(GameEngine.nowProperty(), this.period);
    }

    public SequencedIndexTimeline(double period, int startFrom, int limit, boolean reverse) {
        this.zeroTime = GameEngine.getNow();
        List<Integer> steps = new LinkedList<>();
        if (startFrom > limit) {
            initBackward(steps, startFrom, limit, reverse);
        } else {
            initForward(steps, startFrom, limit, reverse);
        }
        this.frames = steps.stream().mapToInt(i -> i).toArray();
        this.zeroTime = GameEngine.getNow();
        this.period.setValue(period);
        bind(GameEngine.nowProperty(), this.period);
    }

    private void initBackward(List<Integer> steps, int startFrom, int limit, boolean reverse) {
        for (int i = startFrom; i > limit; i--) {
            steps.add(i);
        }
        if (reverse) {
            for (int i = limit + 2; i < startFrom; i++) {
                steps.add(i);
            }
        }
    }

    private void initForward(List<Integer> steps, int startFrom, int limit, boolean reverse) {
        for (int i = startFrom; i < limit; i++) {
            steps.add(i);
        }
        if (reverse) {
            for (int i = limit - 2; i > startFrom; i--) {
                steps.add(i);
            }
        }
    }

    public void halt() {
        unbind(GameEngine.nowProperty(), period);
    }

    public SequencedIndexTimeline(int startFrom, int limit, boolean reverse) {
        this(0.0, startFrom, limit, reverse);
    }

    public SequencedIndexTimeline(int startFrom, int limit) {
        this(0.0, startFrom, limit, false);
    }

    @Override
    protected int computeValue() {
        double p = period.get();
        if (p > 0.0) {
            long now = GameEngine.getNow() - zeroTime;
            currentIndex = Math.toIntExact(now / stepDuration) % frames.length;
        }
        return frames[currentIndex];
    }

    public double getPeriod() {
        return period.get();
    }

    public DoubleProperty periodProperty() {
        return period;
    }

    public void setPeriod(double period) {
        this.period.set(period);
    }
}
