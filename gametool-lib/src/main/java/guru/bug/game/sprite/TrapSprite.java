package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.SequencedIndexTimeline;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/trap.sprite")
public class TrapSprite extends Sprite {
    private static final int MATERIAL_DIMENSION = 0;
    private static final int ANIMATION_DIMENSION = 1;
    private static final double ANIMATION_DURATION = 0.8;
    private static final int MAX_FRAME = 4;
    private static final SequencedIndexTimeline animation = new SequencedIndexTimeline(ANIMATION_DURATION, 0, MAX_FRAME, true);
    private final ObjectProperty<TrapMaterial> material = new SimpleObjectProperty<>(TrapMaterial.LAVA);

    public TrapSprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(MATERIAL_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            TrapMaterial m = getMaterial();
            return m == null ? 0 : m.ordinal();
        }, material));
        painterStateProperty(ANIMATION_DIMENSION).bind(animation);
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 2);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    public TrapMaterial getMaterial() {
        return material.get();
    }

    public ObjectProperty<TrapMaterial> materialProperty() {
        return material;
    }

    public void setMaterial(TrapMaterial material) {
        this.material.set(material);
    }
}
