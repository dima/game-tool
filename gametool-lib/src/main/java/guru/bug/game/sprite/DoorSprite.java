package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/door.sprite")
public class DoorSprite extends Sprite {
    private static final int COLOR_DIMENSION = 0;
    private static final int SHAPE_DIMENSION = 1;
    private final ObjectProperty<DoorColor> color = new SimpleObjectProperty<>();
    private final ObjectProperty<DoorShape> shape = new SimpleObjectProperty<>();

    public DoorSprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(COLOR_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            DoorColor c = getColor();
            return c == null ? 0 : c.ordinal();
        }, color));
        painterStateProperty(SHAPE_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            DoorShape c = getShape();
            return c == null ? 0 : c.ordinal();
        }, shape));
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    public DoorColor getColor() {
        return color.get();
    }

    public ObjectProperty<DoorColor> colorProperty() {
        return color;
    }

    public void setColor(DoorColor color) {
        this.color.set(color);
    }

    public DoorShape getShape() {
        return shape.get();
    }

    public ObjectProperty<DoorShape> shapeProperty() {
        return shape;
    }

    public void setShape(DoorShape shape) {
        this.shape.set(shape);
    }
}
