package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/switch.sprite")
public class SwitchSprite extends Sprite {
    private final ObjectProperty<SwitchState> state = new SimpleObjectProperty<>(SwitchState.RIGHT);

    public SwitchSprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(0).bind(Bindings.createIntegerBinding(() -> {
            SwitchState c = getState();
            if (c == null) {
                return 0;
            }
            return c.ordinal();
        }, state));
    }


    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    public SwitchState getState() {
        return state.get();
    }

    public ObjectProperty<SwitchState> stateProperty() {
        return state;
    }

    public void setState(SwitchState state) {
        this.state.set(state);
    }
}
