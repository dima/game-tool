package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/mouse.sprite")
public class MouseSprite extends Sprite {
    private static final int ROTATION_DIMENSION = 1;
    private static final int ANIMATION_DIMENSION = 0;
    private static final double ROTATE_PERIOD = 0.5;
    private final SequencedIndexTimeline rotateIndex = new SequencedIndexTimeline(ROTATE_PERIOD, 0, 2, false);

    public MouseSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        painterStateProperty(ROTATION_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            Direction dir = getRotation();
            if (dir == null) {
                return 0;
            } else {
                return dir.ordinal();
            }
        }, rotationProperty()));
    }

    @Override
    protected void halted() {
        rotateIndex.halt();
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
        if (getSpeed() > 0) {
            setPainterState(ANIMATION_DIMENSION, rotateIndex.get());
        }
    }
}
