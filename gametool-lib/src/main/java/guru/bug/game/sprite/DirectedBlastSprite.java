package guru.bug.game.sprite;

import guru.bug.game.AnimationHandler;
import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Direction;
import guru.bug.game.Painter;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.util.Duration;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/directed-blast.sprite")
public class DirectedBlastSprite extends Explosion {

    private final ObjectProperty<DirectedBlastColor> color = new SimpleObjectProperty<>(DirectedBlastColor.YELLOW);

    public DirectedBlastSprite() {
        setCollisionDetectionType(CollisionDetectionType.NONE);
        painterStateProperty(0).bind(Bindings.createIntegerBinding(() -> {
            DirectedBlastColor t = getColor();
            return t == null ? null : t.ordinal();
        }, color));

        painterStateProperty(1).bind(Bindings.createIntegerBinding(() -> {
            Direction t = getRotation();
            return t == null ? null : t.ordinal();
        }, rotationProperty()));

    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    @Override
    public AnimationHandler explode() {
        KeyFrame frame = new KeyFrame(Duration.seconds(0.7), new KeyValue(painterStateProperty(2), 7, Interpolator.EASE_OUT));
        Timeline timeline = new Timeline(frame);
        timeline.setAutoReverse(true);
        timeline.setCycleCount(1);
        AnimationHandler handler = new AnimationHandler();
        timeline.setOnFinished(e -> {
            handler.fire();
            halt();
        });
        timeline.play();
        return handler;
    }

    public DirectedBlastColor getColor() {
        return color.get();
    }

    public ObjectProperty<DirectedBlastColor> colorProperty() {
        return color;
    }

    public void setColor(DirectedBlastColor color) {
        this.color.set(color);
    }
}
