package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Direction;
import guru.bug.game.Painter;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/truck.sprite")
public class TruckSprite extends Sprite {
    private final ObjectProperty<CarColor> color = new SimpleObjectProperty<>(CarColor.BLUE);

    public TruckSprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(1).bind(Bindings.createIntegerBinding(() -> {
            CarColor c = getColor();
            if (c == null) {
                return 0;
            }
            return c.ordinal();
        }, color));
        painterStateProperty(0).bind(Bindings.createIntegerBinding(() -> {
            Direction d = getRotation();
            if (d == Direction.W) {
                return 1;
            } else {
                return 0;
            }
        }, rotationProperty()));
    }


    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    public CarColor getColor() {
        return color.get();
    }

    public ObjectProperty<CarColor> colorProperty() {
        return color;
    }

    public void setColor(CarColor color) {
        this.color.set(color);
    }
}
