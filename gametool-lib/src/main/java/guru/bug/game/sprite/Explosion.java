package guru.bug.game.sprite;

import guru.bug.game.AnimationHandler;
import guru.bug.game.Sprite;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public abstract class Explosion extends Sprite {

    public abstract AnimationHandler explode();
}
