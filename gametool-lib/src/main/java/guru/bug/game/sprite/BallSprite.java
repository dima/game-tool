package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/ball.sprite")
public class BallSprite extends Sprite {
    private static final int ROTATION_DIMENSION = 0;
    private static final int ANIMATION_DIMENSION = 1;
    private static final int FLAME_DIMENSION = 2;
    private static final int COLOR_DIMENSION = 3;
    private static final double BALL_ROTATE_PERIOD = 0.5;
    private static final int BALL_MAX_FRAME = 4;
    private static final double FLAME_ROTATE_PERIOD = 0.3;
    private static final int FLAME_MAX_FRAME = 4;
    private final SequencedIndexTimeline flamePos = new SequencedIndexTimeline(FLAME_ROTATE_PERIOD, 1, FLAME_MAX_FRAME, false);
    private final SequencedIndexTimeline rotateIndex = new SequencedIndexTimeline(BALL_ROTATE_PERIOD, 0, BALL_MAX_FRAME, false);
    private final BooleanProperty flame = new SimpleBooleanProperty(this, "flame");
    private final ObjectProperty<BallColor> color = new SimpleObjectProperty<>();

    public BallSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        painterStateProperty(ANIMATION_DIMENSION).bind(rotateIndex);
        painterStateProperty(ROTATION_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            Direction dir = getRotation();
            if (dir == null) {
                return 0;
            } else {
                return dir.ordinal();
            }
        }, rotationProperty()));
        painterStateProperty(COLOR_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            BallColor c = getColor();
            return c == null ? 0 : c.ordinal();
        }, color));
    }

    @Override
    protected void halted() {
        flamePos.halt();
        rotateIndex.halt();
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
        if (flame.get() ^ painterStateProperty(FLAME_DIMENSION).isBound()) {
            if (flame.get()) {
                painterStateProperty(2).bind(flamePos);
            } else {
                painterStateProperty(2).unbind();
                painterStateProperty(2).set(0);
            }
        }
    }

    private double distance(double x, double y, double targetX, double targetY) {
        double dx = targetX - x;
        double dy = targetY - y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    private void changeDirectionToTarget() {
        double rad = Direction.calcAngle(getX(), getY(), getTargetX(), getTargetY());
        Direction dir = Direction.findClosest(rad);
        setDirection(dir);
    }

    public boolean isFlame() {
        return flame.get();
    }

    public BooleanProperty flameProperty() {
        return flame;
    }

    public void setFlame(boolean flame) {
        this.flame.set(flame);
    }

    public BallColor getColor() {
        return color.get();
    }

    public ObjectProperty<BallColor> colorProperty() {
        return color;
    }

    public void setColor(BallColor color) {
        this.color.set(color);
    }
}
