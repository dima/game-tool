package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/cat.sprite")
public class CatSprite extends Sprite {
    private static final int ROTATION_DIMENSION = 2;
    private static final int ANIMATION_DIMENSION = 1;
    private static final int COLOR_DIMENSION = 0;
    private static final double ROTATE_PERIOD = 0.5;
    private final SequencedIndexTimeline rotateIndex = new SequencedIndexTimeline(ROTATE_PERIOD, 0, 2, false);
    private final ObjectProperty<CatColor> color = new SimpleObjectProperty<>();

    public CatSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        painterStateProperty(ROTATION_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            Direction dir = getRotation();
            if (dir == null) {
                return 0;
            } else {
                return dir.ordinal();
            }
        }, rotationProperty()));
        painterStateProperty(COLOR_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            CatColor c = getColor();
            return c == null ? 0 : c.ordinal();
        }, color));
    }

    @Override
    protected void halted() {
        rotateIndex.halt();
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
        if (getSpeed() > 0) {
            setPainterState(ANIMATION_DIMENSION, rotateIndex.get());
        }
    }

    private double distance(double x, double y, double targetX, double targetY) {
        double dx = targetX - x;
        double dy = targetY - y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    private void changeDirectionToTarget() {
        double rad = Direction.calcAngle(getX(), getY(), getTargetX(), getTargetY());
        Direction dir = Direction.findClosest(rad);
        setDirection(dir);
    }


    public CatColor getColor() {
        return color.get();
    }

    public ObjectProperty<CatColor> colorProperty() {
        return color;
    }

    public void setColor(CatColor color) {
        this.color.set(color);
    }
}
