package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.SequencedIndexTimeline;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleExpression;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/bigredmonster.sprite")
public class BigRedMonsterSprite extends Sprite {
    private static final int ANIMATION_DIMENSION = 0;
    private final SequencedIndexTimeline index = new SequencedIndexTimeline(0, 3, true);

    public BigRedMonsterSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        DoubleExpression period = Bindings.createDoubleBinding(() -> 1.5 / getSpeed(), speedProperty());
        index.periodProperty().bind(period);
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
        if (getSpeed() > 0) {
            setPainterState(ANIMATION_DIMENSION, index.get());
        }
    }

    @Override
    protected void halted() {
        index.halt();
    }
}
