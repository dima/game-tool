package guru.bug.game.sprite;

import guru.bug.game.AnimationHandler;
import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.util.Duration;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/collapse-blast.sprite")
public class CollapseBlastSprite extends Explosion {
    private final ObjectProperty<CollapseBlastColor> color = new SimpleObjectProperty<>(CollapseBlastColor.RED);

    public CollapseBlastSprite() {
        setCollisionDetectionType(CollisionDetectionType.NONE);
        painterStateProperty(0).bind(Bindings.createIntegerBinding(() -> {
            CollapseBlastColor m = getColor();
            return m == null ? 0 : m.ordinal();
        }, color));
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 1);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    @Override
    public AnimationHandler explode() {
        KeyFrame frame = new KeyFrame(Duration.seconds(0.4), new KeyValue(painterStateProperty(1), 4, Interpolator.EASE_OUT));
        Timeline timeline = new Timeline(frame);
        timeline.setAutoReverse(false);
        timeline.setCycleCount(1);
        AnimationHandler handler = new AnimationHandler();
        timeline.setOnFinished(e -> {
            handler.fire();
            halt();
        });
        timeline.play();
        return handler;
    }

    public CollapseBlastColor getColor() {
        return color.get();
    }

    public ObjectProperty<CollapseBlastColor> colorProperty() {
        return color;
    }

    public void setColor(CollapseBlastColor color) {
        this.color.set(color);
    }
}
