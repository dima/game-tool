package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/chicken.sprite")
public class ChickenSprite extends Sprite {
    private static final int ROTATION_DIMENSION = 2;
    private static final int ANIMATION_DIMENSION = 1;
    private static final int DIRECTION_DIMENSION = 0;
    private static final double ROTATE_PERIOD = 0.5;
    private final SequencedIndexTimeline rotateIndex = new SequencedIndexTimeline(ROTATE_PERIOD, 0, 2, false);
    private final BooleanProperty reverse = new SimpleBooleanProperty();

    public ChickenSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        painterStateProperty(ROTATION_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            Direction dir = getRotation();
            if (dir == null) {
                return 0;
            } else {
                return dir.ordinal();
            }
        }, rotationProperty()));
        painterStateProperty(DIRECTION_DIMENSION).bind(Bindings.createIntegerBinding(() -> isReverse() ? 0 : 1, reverse));
    }

    @Override
    protected void halted() {
        rotateIndex.halt();
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
        setPainterState(ANIMATION_DIMENSION, rotateIndex.get());
    }

    public boolean isReverse() {
        return reverse.get();
    }

    public BooleanProperty reverseProperty() {
        return reverse;
    }

    public void setReverse(boolean reverse) {
        this.reverse.set(reverse);
    }
}
