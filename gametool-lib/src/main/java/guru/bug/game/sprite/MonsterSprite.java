package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/monster.sprite")
public class MonsterSprite extends Sprite {
    private static final int ANIMATION_DIMENSION = 2;
    private static final int DIR_DIMENSION = 1;
    private static final int OUTFIT_DIMENSION = 0;
    private static final double ANIM_PERIOD = 0.8;
    private final SequencedIndexTimeline animationIndex = new SequencedIndexTimeline(0, 2);
    private final ObjectProperty<MonsterOutfit> outfit = new SimpleObjectProperty<>(MonsterOutfit.ROUND_HEAD);
    private int lastUsed = 0;

    public MonsterSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        DoubleExpression period = Bindings.createDoubleBinding(() -> ANIM_PERIOD / getSpeed(), speedProperty());
        animationIndex.periodProperty().bind(period);
        painterStateProperty(ANIMATION_DIMENSION).bind(animationIndex);
        painterStateProperty(OUTFIT_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            MonsterOutfit c = getOutfit();
            return c == null ? 0 : c.ordinal();
        }, outfit));
        painterStateProperty(DIR_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            Direction dir = getRotation();
            if (dir == Direction.E) {
                lastUsed = 1;
            } else if (dir == Direction.W) {
                lastUsed = 0;
            }
            return lastUsed;
        }, rotationProperty()));
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
    }

    @Override
    protected void halted() {
        animationIndex.halt();
    }

    public MonsterOutfit getOutfit() {
        return outfit.get();
    }

    public ObjectProperty<MonsterOutfit> outfitProperty() {
        return outfit;
    }

    public void setOutfit(MonsterOutfit outfit) {
        this.outfit.set(outfit);
    }
}
