package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/block.sprite")
public class BlockSprite extends Sprite {
    private final ObjectProperty<BlockMaterial> material = new SimpleObjectProperty<>(BlockMaterial.AMBER);
    private final IntegerProperty crackLevel = new SimpleIntegerProperty();

    public BlockSprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(0).bind(Bindings.createIntegerBinding(() -> {
            BlockMaterial m = getMaterial();
            return m == null ? 0 : m.ordinal();
        }, material));
        painterStateProperty(1).bind(Bindings.createIntegerBinding(() -> {
            int result = getCrackLevel();
            if (result < 0) {
                result = 0;
            }
            if (result > 3) {
                result = 3;
            }
            return result;
        }, crackLevel));
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 1);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    public BlockMaterial getMaterial() {
        return material.get();
    }

    public ObjectProperty<BlockMaterial> materialProperty() {
        return material;
    }

    public void setMaterial(BlockMaterial material) {
        this.material.set(material);
    }

    public int getCrackLevel() {
        return crackLevel.get();
    }

    public IntegerProperty crackLevelProperty() {
        return crackLevel;
    }

    public void setCrackLevel(int crackLevel) {
        this.crackLevel.set(crackLevel);
    }
}
