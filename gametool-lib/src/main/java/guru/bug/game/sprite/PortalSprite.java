package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.SequencedIndexTimeline;
import guru.bug.game.Sprite;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/portal.sprite")
public class PortalSprite extends Sprite {
    private static final int ANIMATION_DIMENSION = 0;
    private static final double ROTATE_PERIOD = 0.75;
    private final SequencedIndexTimeline rotateIndex = new SequencedIndexTimeline(ROTATE_PERIOD, 0, 16, false);

    public PortalSprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(ANIMATION_DIMENSION).bind(rotateIndex);
    }

    @Override
    protected void halted() {
        rotateIndex.halt();
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
    }
}
