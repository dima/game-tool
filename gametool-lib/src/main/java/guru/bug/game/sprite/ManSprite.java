package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/man.sprite")
public class ManSprite extends Sprite {
    private static final int ANIMATION_DIMENSION = 1;
    private static final int OUTFIT_DIMENSION = 0;
    private static final double ROTATE_PERIOD = 1.5;
    private static final double ROTATE_LADDER_PERIOD = 2;
    private final SequencedIndexTimeline leftWalkIndex = new SequencedIndexTimeline(0, 8);
    private final SequencedIndexTimeline rightWalkIndex = new SequencedIndexTimeline(8, 16);
    private final SequencedIndexTimeline leftRunIndex = new SequencedIndexTimeline(16, 23);
    private final SequencedIndexTimeline rightRunIndex = new SequencedIndexTimeline(23, 30);
    private final SequencedIndexTimeline ladderUpIndex = new SequencedIndexTimeline(76, 80);
    private final SequencedIndexTimeline ladderDownIndex = new SequencedIndexTimeline(79, 75);
    private final BooleanProperty running = new SimpleBooleanProperty();
    private final ObjectProperty<ManOutfit> outfit = new SimpleObjectProperty<>(ManOutfit.LIGHT);

    public ManSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        DoubleExpression period = Bindings.createDoubleBinding(() -> ROTATE_PERIOD / getSpeed(), speedProperty());
        DoubleExpression laderUpPeriod = Bindings.createDoubleBinding(() -> ROTATE_LADDER_PERIOD / getSpeed(), speedProperty());
        DoubleExpression laderDownPeriod = Bindings.createDoubleBinding(() -> ROTATE_LADDER_PERIOD / getSpeed(), speedProperty());
        rightWalkIndex.periodProperty().bind(period);
        leftWalkIndex.periodProperty().bind(period);
        rightRunIndex.periodProperty().bind(period);
        leftRunIndex.periodProperty().bind(period);
        ladderUpIndex.periodProperty().bind(laderUpPeriod);
        ladderDownIndex.periodProperty().bind(laderDownPeriod);
        painterStateProperty(OUTFIT_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            ManOutfit c = getOutfit();
            return c == null ? 0 : c.ordinal();
        }, outfit));
        speedProperty().addListener((o, ov, nv) -> {
            if (nv == null || nv.doubleValue() > 0) {
                return;
            }
            Direction dir = getRotation();
            switch (dir) {
                case E:
                    setPainterState(ANIMATION_DIMENSION, 74);
                    break;
                case W:
                    setPainterState(ANIMATION_DIMENSION, 75);
                    break;
                case N:
                case S:
                    setPainterState(ANIMATION_DIMENSION, 72);
                    break;
                default:
                    setPainterState(ANIMATION_DIMENSION, 73);
            }
        });
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
        if (getSpeed() == 0) {
            return;
        }
        Direction dir = getRotation();
        switch (dir) {
            case E:
                setPainterState(ANIMATION_DIMENSION, isRunning() ? rightRunIndex.get() : rightWalkIndex.get());
                break;
            case W:
                setPainterState(ANIMATION_DIMENSION, isRunning() ? leftRunIndex.get() : leftWalkIndex.get());
                break;
            case N:
                setPainterState(ANIMATION_DIMENSION, ladderUpIndex.get());
                break;
            case S:
                setPainterState(ANIMATION_DIMENSION, ladderDownIndex.get());
                break;
            default:
                setPainterState(ANIMATION_DIMENSION, 73);
        }
    }

    @Override
    protected void halted() {
        rightWalkIndex.halt();
        leftWalkIndex.halt();
        rightRunIndex.halt();
        leftRunIndex.halt();
        ladderUpIndex.halt();
        ladderDownIndex.halt();
    }

    public boolean isRunning() {
        return running.get();
    }

    public BooleanProperty runningProperty() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running.set(running);
    }

    public ManOutfit getOutfit() {
        return outfit.get();
    }

    public ObjectProperty<ManOutfit> outfitProperty() {
        return outfit;
    }

    public void setOutfit(ManOutfit outfit) {
        this.outfit.set(outfit);
    }
}
