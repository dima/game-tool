package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/packman.sprite")
public class PackSprite extends Sprite {
    private static final int COLOR_DIMENSION = 0;
    private static final int ROTATION_DIMENSION = 2;
    private static final int ANIMATION_DIMENSION = 1;
    private static final double ANIM_PERIOD = 0.4;
    private final SequencedIndexTimeline rotateIndex = new SequencedIndexTimeline(ANIM_PERIOD, 0, 3, true);
    private final ObjectProperty<PackColor> color = new SimpleObjectProperty<>();

    public PackSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        painterStateProperty(ROTATION_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            Direction dir = getRotation();
            if (dir == null) {
                return 0;
            } else {
                return dir.ordinal();
            }
        }, rotationProperty()));
        painterStateProperty(COLOR_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            PackColor c = getColor();
            return c == null ? 0 : c.ordinal();
        }, color));
    }

    @Override
    protected void halted() {
        rotateIndex.halt();
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
        if (getSpeed() > 0) {
            setPainterState(ANIMATION_DIMENSION, rotateIndex.get());
        }
    }

    public PackColor getColor() {
        return color.get();
    }

    public ObjectProperty<PackColor> colorProperty() {
        return color;
    }

    public void setColor(PackColor color) {
        this.color.set(color);
    }
}
