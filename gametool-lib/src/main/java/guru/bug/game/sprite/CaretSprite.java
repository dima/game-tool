package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/caret.sprite")
public class CaretSprite extends Sprite {
    private static final double ROTATE_PERIOD = 0.17;
    private static final int MAX_FRAME = 4;
    private final SequencedIndexTimeline index = new SequencedIndexTimeline(ROTATE_PERIOD, 0, MAX_FRAME, false);


    public CaretSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        painterStateProperty(0).bind(index);
        painterStateProperty(1).bind(Bindings.createIntegerBinding(() -> {
            Direction dir = getRotation();
            if (dir == null) {
                return 0;
            } else {
                return dir.ordinal();
            }
        }, rotationProperty()));
    }


    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {
        index.halt();
    }

}
