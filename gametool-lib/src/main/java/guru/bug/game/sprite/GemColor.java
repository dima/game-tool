package guru.bug.game.sprite;

/**
 * Created by dima on 16.11.10.
 */
public enum GemColor {
    VIOLET,
    BLUE,
    YELLOW,
    WHITE,
    GREEN,
    NONE
}
