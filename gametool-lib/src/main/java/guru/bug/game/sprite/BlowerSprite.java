package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.SequencedIndexTimeline;
import guru.bug.game.Sprite;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/blower.sprite")
public class BlowerSprite extends Sprite {
    private static final int ANIMATION_DIMENSION = 0;
    private static final double ROTATE_PERIOD = 0.3;
    private final SequencedIndexTimeline rotateIndex = new SequencedIndexTimeline(ROTATE_PERIOD, 0, 2, false);

    public BlowerSprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(ANIMATION_DIMENSION).bind(rotateIndex);
    }

    @Override
    protected void halted() {
        rotateIndex.halt();
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
    }
}
