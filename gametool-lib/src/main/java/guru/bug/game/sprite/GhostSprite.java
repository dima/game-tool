package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/ghost.sprite")
public class GhostSprite extends Sprite {
    private static final int ANIMATION_DIMENSION = 1;
    private static final int COLOR_DIMENSION = 0;
    private static final double ROTATE_PERIOD = 0.3;
    private final SequencedIndexTimeline rotateIndex = new SequencedIndexTimeline(ROTATE_PERIOD, 0, 2, false);
    private final ObjectProperty<GhostColor> color = new SimpleObjectProperty<>();

    public GhostSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        painterStateProperty(COLOR_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            GhostColor c = getColor();
            return c == null ? 0 : c.ordinal();
        }, color));
        painterStateProperty(ANIMATION_DIMENSION).bind(rotateIndex);
    }

    @Override
    protected void halted() {
        rotateIndex.halt();
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
    }

    private double distance(double x, double y, double targetX, double targetY) {
        double dx = targetX - x;
        double dy = targetY - y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    private void changeDirectionToTarget() {
        double rad = Direction.calcAngle(getX(), getY(), getTargetX(), getTargetY());
        Direction dir = Direction.findClosest(rad);
        setDirection(dir);
    }


    public GhostColor getColor() {
        return color.get();
    }

    public ObjectProperty<GhostColor> colorProperty() {
        return color;
    }

    public void setColor(GhostColor color) {
        this.color.set(color);
    }
}
