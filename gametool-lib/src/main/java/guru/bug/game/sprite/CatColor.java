package guru.bug.game.sprite;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public enum CatColor {
    BLACK,
    GINGER,
    WHITE
}
