package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/ladder.sprite")
public class LadderSprite extends Sprite {
    private static final int COLOR_DIMENSION = 0;
    private final ObjectProperty<LadderColor> color = new SimpleObjectProperty<>();

    public LadderSprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(COLOR_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            LadderColor c = getColor();
            return c == null ? 0 : c.ordinal();
        }, color));
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    public LadderColor getColor() {
        return color.get();
    }

    public ObjectProperty<LadderColor> colorProperty() {
        return color;
    }

    public void setColor(LadderColor color) {
        this.color.set(color);
    }
}
