package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/flame.sprite")
public class FlameSprite extends Sprite {
    private static final double ROTATE_PERIOD_MIN = 0.25;
    private static final double ROTATE_PERIOD_MAX = 0.5;
    private static final int ANIMATION_DIMENSION = 0;
    private static final int ROTATION_DIMENSION = 1;
    private final SequencedIndexTimeline index = new SequencedIndexTimeline(0, 2);


    public FlameSprite() {
        double period = ThreadLocalRandom.current().nextDouble(ROTATE_PERIOD_MIN, ROTATE_PERIOD_MAX);
        index.setPeriod(period);
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(ANIMATION_DIMENSION).bind(index);
        painterStateProperty(ROTATION_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            Direction dir = getRotation();
            if (dir == null) {
                return 0;
            } else {
                return dir.ordinal();
            }
        }, rotationProperty()));
    }


    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {
        index.halt();
    }

}
