package guru.bug.game.sprite;

/**
 * Created by dima on 16.11.10.
 */
public enum GemIcon {
    QUESTION,
    HEART,
    SKULL,
    FIRE,
    ONE_BALL,
    SNAIL_CIRCLE,
    LIGHTNING_SQUARE,
    EXPAND,
    SNAIL_SQUARE,
    LIGHTNING_CIRCLE,
    SHRINK,
    THREE_BALLS,
    NONE
}
