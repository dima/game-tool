package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/plate.sprite")
public class PlateSprite extends Sprite {
    private static final int FOOD_DIMENSION = 0;
    private final ObjectProperty<PlateFood> food = new SimpleObjectProperty<>();

    public PlateSprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(FOOD_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            PlateFood c = getFood();
            return c == null ? 0 : c.ordinal();
        }, food));
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    public PlateFood getFood() {
        return food.get();
    }

    public ObjectProperty<PlateFood> foodProperty() {
        return food;
    }

    public void setFood(PlateFood food) {
        this.food.set(food);
    }
}
