package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/key.sprite")
public class KeySprite extends Sprite {
    private final ObjectProperty<KeyColor> color = new SimpleObjectProperty<>(KeyColor.GOLD);

    public KeySprite() {
        setCollisionDetectionType(CollisionDetectionType.PASSIVE);
        painterStateProperty(0).bind(Bindings.createIntegerBinding(() -> {
            KeyColor c = getColor();
            if (c == null) {
                return 0;
            }
            return c.ordinal();
        }, color));
    }


    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    public KeyColor getColor() {
        return color.get();
    }

    public ObjectProperty<KeyColor> colorProperty() {
        return color;
    }

    public void setColor(KeyColor color) {
        this.color.set(color);
    }
}
