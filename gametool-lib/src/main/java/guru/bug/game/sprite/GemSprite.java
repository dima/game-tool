package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/gem.sprite")
public class GemSprite extends Sprite {
    private final ObjectProperty<GemIcon> icon = new SimpleObjectProperty<>();
    private final ObjectProperty<GemColor> color = new SimpleObjectProperty<>();

    public GemSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        painterStateProperty(0).bind(Bindings.createIntegerBinding(() -> {
            GemIcon i = getIcon();
            return i == null ? 0 : i.ordinal();
        }, icon));
        painterStateProperty(1).bind(Bindings.createIntegerBinding(() -> {
            GemColor c = getColor();
            return c == null ? 0 : c.ordinal();
        }, color));
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row + 0.5);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {

    }

    @Override
    protected void halted() {

    }

    public GemIcon getIcon() {
        return icon.get();
    }

    public ObjectProperty<GemIcon> iconProperty() {
        return icon;
    }

    public void setIcon(GemIcon icon) {
        this.icon.set(icon);
    }

    public GemColor getColor() {
        return color.get();
    }

    public ObjectProperty<GemColor> colorProperty() {
        return color;
    }

    public void setColor(GemColor color) {
        this.color.set(color);
    }
}
