package guru.bug.game.sprite;

import guru.bug.game.*;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/pony.sprite")
public class PonySprite extends Sprite {
    private static final int ANIMATION_DIMENSION = 0;
    private static final int OUTFIT_DIMENSION = 1;
    private static final double ROTATE_PERIOD = 1.5;
    private final SequencedIndexTimeline leftIndex = new SequencedIndexTimeline(0, 10);
    private final SequencedIndexTimeline rightIndex = new SequencedIndexTimeline(10, 20);
    private final ObjectProperty<PonyOutfit> outfit = new SimpleObjectProperty<>(PonyOutfit.PINK);
    private SequencedIndexTimeline lastUsed;

    public PonySprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        DoubleExpression period = Bindings.createDoubleBinding(() -> ROTATE_PERIOD / getSpeed(), speedProperty());
        rightIndex.periodProperty().bind(period);
        leftIndex.periodProperty().bind(period);
        painterStateProperty(OUTFIT_DIMENSION).bind(Bindings.createIntegerBinding(() -> {
            PonyOutfit c = getOutfit();
            return c == null ? 0 : c.ordinal();
        }, outfit));
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
        Direction dir = getRotation();
        switch (dir) {
            case E:
                lastUsed = rightIndex;
                break;
            case W:
                lastUsed = leftIndex;
                break;
        }
        if (lastUsed != null) {
            setPainterState(ANIMATION_DIMENSION, lastUsed.get());
        }
    }

    @Override
    protected void halted() {
        rightIndex.halt();
        leftIndex.halt();
    }

    public PonyOutfit getOutfit() {
        return outfit.get();
    }

    public ObjectProperty<PonyOutfit> outfitProperty() {
        return outfit;
    }

    public void setOutfit(PonyOutfit outfit) {
        this.outfit.set(outfit);
    }
}
