package guru.bug.game.sprite;

import guru.bug.game.CollisionDetectionType;
import guru.bug.game.Painter;
import guru.bug.game.SequencedIndexTimeline;
import guru.bug.game.Sprite;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleExpression;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
@Painter("guru/bug/game/sprite/hedgehog.sprite")
public class HedgehogSprite extends Sprite {
    private static final int ANIMATION_DIMENSION = 0;
    private static final double ROTATE_PERIOD = 1;
    private final SequencedIndexTimeline animIndex = new SequencedIndexTimeline(0, 2);

    public HedgehogSprite() {
        setCollisionDetectionType(CollisionDetectionType.ACTIVE);
        DoubleExpression period = Bindings.createDoubleBinding(() -> ROTATE_PERIOD / getSpeed(), speedProperty());
        animIndex.periodProperty().bind(period);
    }

    @Override
    protected void setPosCell(double col, double row) {
        setX(col + 0.5);
        setY(row);
    }

    @Override
    protected void setup() {

    }

    @Override
    protected void loop() {
        if (getSpeed() > 0) {
            setPainterState(ANIMATION_DIMENSION, animIndex.get());
        }
    }

    @Override
    protected void halted() {
        animIndex.halt();
    }
}
