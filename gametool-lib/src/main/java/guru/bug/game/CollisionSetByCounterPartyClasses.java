package guru.bug.game;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class CollisionSetByCounterPartyClasses extends CollisionSetChild {
    private final Set<Class<? extends Sprite>> counterparty;

    CollisionSetByCounterPartyClasses(CollisionSet parent, Collection<Class<? extends Sprite>> counterparty) {
        super(parent);
        this.counterparty = new HashSet<>(counterparty);
    }

    @Override
    protected Stream<Collision> collisionStream() {
        return super.collisionStream()
                .filter(c -> counterparty.contains(c.getCounterparty().getSprite().getClass()));
    }


}
