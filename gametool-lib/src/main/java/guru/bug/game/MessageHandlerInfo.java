package guru.bug.game;

import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class MessageHandlerInfo {
    private final Consumer<Sprite> handler;
    private final String message;

    MessageHandlerInfo(Consumer<Sprite> handler, String message) {
        this.handler = handler;
        this.message = message;
    }

    String getMessage() {
        return message;
    }

    Consumer<Sprite> getHandler() {
        return handler;
    }

}
