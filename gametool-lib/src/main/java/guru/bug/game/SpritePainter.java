package guru.bug.game;

import guru.bug.game.utils.ImageExternalizer;
import guru.bug.game.utils.Pool;
import javafx.beans.Observable;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.IntegerExpression;
import javafx.beans.binding.NumberBinding;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableDoubleValue;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class SpritePainter {
    private static final long MAGIC_HEADER = 0x4255_472D_4741_4D45L;
    private String resourcePath;
    private Frame[] frames;
    private int[] dimensionSizes;

    private SpritePainter() {
        resourcePath = null;
    }

    public SpritePainter(String path) {
        this.resourcePath = path;
    }

    public synchronized void load() {
        if (frames == null) {
            try {
                URL resourceUrl = SpritePainter.class.getClassLoader().getResource(resourcePath);
                if (resourceUrl == null) {
                    throw new FileNotFoundException(resourcePath);
                }
                try (InputStream is = resourceUrl.openStream();
                     ObjectInputStream ois = new ObjectInputStream(is)) {
                    long header = ois.readLong();
                    if (header != MAGIC_HEADER) {
                        throw new IllegalStateException("Incorrect header");
                    }
                    readExternal(ois);
                }
            } catch (IOException | ClassNotFoundException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public static Builder builder() {
        SpritePainter sprite = new SpritePainter();
        return sprite.createBuilder();
    }

    public void write(File file) throws IOException {
        try (OutputStream os = new FileOutputStream(file);
             ObjectOutputStream oos = new ObjectOutputStream(os)) {
            oos.writeLong(MAGIC_HEADER);
            writeExternal(oos);
        }
    }

    private Builder createBuilder() {
        return new Builder();
    }

    State createState(ObservableDoubleValue x, ObservableDoubleValue y) {
        return new State(x, y);
    }

    private void writeExternal(ObjectOutputStream out) throws IOException {
        out.writeInt(1); // version
        out.writeInt(frames.length);
        for (Frame f : frames) {
            f.writeExternal(out);
        }
        out.writeInt(dimensionSizes.length);
        for (int s : dimensionSizes) {
            out.writeInt(s);
        }
    }

    private void readExternal(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.readInt(); // version
        int size = in.readInt();
        frames = new Frame[size];
        for (int i = 0; i < size; i++) {
            Frame f = new Frame();
            f.readExternal(in);
            frames[i] = f;
        }
        size = in.readInt();
        dimensionSizes = new int[size];
        for (int i = 0; i < size; i++) {
            dimensionSizes[i] = in.readInt();
        }
    }

    public String getResourcePath() {
        return resourcePath;
    }

    private class Frame {
        private Image image;
        private double offsetX;
        private double offsetY;
        private double width;
        private double height;
        private LocalBumper[] draftBumpers;
        private LocalBumper[] preciseBumpers;

        Frame() {
        }

        void draw(GraphicsContext gc, double x, double y, boolean debug) {
            gc.drawImage(image,
                    x - offsetX, y - offsetY,
                    width, height);
            if (debug) {
                gc.save();
                double scale = gc.getTransform().getMxx();
                gc.setLineWidth(0.7 / scale);
                gc.setStroke(Color.GREEN);
                for (LocalBumper c : preciseBumpers) {
                    c.draw(gc, x, y);
                }
                if (draftBumpers != null) {
                    gc.setStroke(Color.RED);
                    for (LocalBumper c : draftBumpers) {
                        c.draw(gc, x, y);
                    }
                }
                gc.restore();
            }
        }

        private void writeExternal(ObjectOutputStream out) throws IOException {
            out.writeInt(1); // version
            ImageExternalizer.writeImage(out, image);
            out.writeDouble(offsetX);
            out.writeDouble(offsetY);
            out.writeDouble(width);
            out.writeDouble(height);
            writeBumpers(out, draftBumpers);
            writeBumpers(out, preciseBumpers);
        }

        private void writeBumpers(ObjectOutputStream out, LocalBumper[] bumpers) throws IOException {
            if (bumpers == null) {
                out.writeInt(0);
            } else {
                out.writeInt(bumpers.length);
                for (LocalBumper b : bumpers) {
                    b.writeExternal(out);
                }
            }
        }

        private void readExternal(ObjectInputStream in) throws IOException, ClassNotFoundException {
            in.readInt(); // version
            image = ImageExternalizer.readImage(in);
            offsetX = in.readDouble();
            offsetY = in.readDouble();
            width = in.readDouble();
            height = in.readDouble();
            draftBumpers = readBumpers(in);
            preciseBumpers = readBumpers(in);
        }

        private LocalBumper[] readBumpers(ObjectInputStream in) throws IOException {
            int size = in.readInt();
            if (size == 0) {
                return null;
            }
            LocalBumper[] result = new LocalBumper[size];
            for (int i = 0; i < size; i++) {
                result[i] = new LocalBumper();
                result[i].readExternal(in);
            }
            return result;
        }
    }

    private class LocalBumper {
        private double centerOffsetX;
        private double centerOffsetY;
        private double radius;
        private String areaName;

        void writeExternal(ObjectOutputStream out) throws IOException {
            out.writeInt(2); // version
            out.writeDouble(centerOffsetX);
            out.writeDouble(centerOffsetY);
            out.writeDouble(radius);
            out.writeObject(areaName);
        }

        void readExternal(ObjectInputStream in) throws IOException {
            int version = in.readInt(); // version
            centerOffsetX = in.readDouble();
            centerOffsetY = in.readDouble();
            radius = in.readDouble();
            if (version > 1) {
                try {
                    areaName = (String) in.readObject();
                } catch (ClassNotFoundException e) {
                    areaName = null;
                }
            }
        }

        void draw(GraphicsContext gc, double x, double y) {
            double diam = radius * 2.0d;
            gc.strokeOval(x - centerOffsetX - radius, y - centerOffsetY - radius, diam, diam);
        }
    }

    class State {
        private IntegerProperty[] state;
        private NumberBinding[] stateCoef;
        private IntegerExpression index;
        private ObjectExpression<Frame> currentFrame;
        private ObservableDoubleValue x;
        private ObservableDoubleValue y;

        private State(ObservableDoubleValue x, ObservableDoubleValue y) {
            load();
            this.x = x;
            this.y = y;
            int dimCount = dimensionSizes.length;
            state = new IntegerProperty[dimCount];
            stateCoef = new NumberBinding[dimCount];
            int coef = 1;
            for (int i = dimCount - 1; i >= 0; i--) {
                final IntegerProperty s = new SimpleIntegerProperty(0);
                state[i] = s;
                stateCoef[i] = Bindings.multiply(coef, s);
                coef *= dimensionSizes[i];
            }
            index = Bindings.createIntegerBinding(() -> {
                int result = 0;
                for (NumberBinding n : stateCoef) {
                    result += n.intValue();
                }
                return result;
            }, (Observable[]) stateCoef);
            currentFrame = Bindings.createObjectBinding(() -> frames[index.get()], index);
        }

        void draw(GraphicsContext gc, boolean debug) {
            Frame frame = currentFrame.get();
            frame.draw(gc, x.get(), y.get(), debug);
        }

        void findCollisions(State other, BiConsumer<Bumper, Bumper> collisionConsumer, Pool<Bumper> bumperPool) {
            if (potentialCollide(other, bumperPool)) {
                collidedBumpers(other, collisionConsumer, bumperPool);
            }
        }

        private void collidedBumpers(State other, BiConsumer<Bumper, Bumper> collisionConsumer, Pool<Bumper> bumperPool) {
            Frame thisFrame = this.currentFrame.get();
            Frame otherFrame = other.currentFrame.get();
            Bumper thisPublicBumper = bumperPool.get();
            Bumper otherPublicBumper = bumperPool.get();
            for (LocalBumper thisPrivateBumper : thisFrame.preciseBumpers) {
                updatePublicBumper(thisPublicBumper, thisPrivateBumper);
                for (LocalBumper otherPrivateBumper : otherFrame.preciseBumpers) {
                    other.updatePublicBumper(otherPublicBumper, otherPrivateBumper);
                    if (thisPublicBumper.intersects(otherPublicBumper)) {
                        collisionConsumer.accept(thisPublicBumper, otherPublicBumper);
                        thisPublicBumper = bumperPool.get();
                        updatePublicBumper(thisPublicBumper, thisPrivateBumper);
                        otherPublicBumper = bumperPool.get();
                    }
                }
            }
        }

        private boolean potentialCollide(State other, Pool<Bumper> bumperPool) {
            Frame thisFrame = this.currentFrame.get();
            Frame otherFrame = other.currentFrame.get();
            LocalBumper[] thisBumpers = thisFrame.draftBumpers;
            LocalBumper[] otherBumpers = otherFrame.draftBumpers;
            Bumper thisPubBumper = bumperPool.get();
            Bumper otherPubBumper = bumperPool.get();
            boolean thisIgnoreNamed = false;
            boolean otherIgnoreNamed = false;
            if (thisBumpers == null) {
                thisBumpers = thisFrame.preciseBumpers;
                thisIgnoreNamed = true;
            }
            if (otherBumpers == null) {
                otherBumpers = otherFrame.preciseBumpers;
                otherIgnoreNamed = true;
            }
            for (LocalBumper thisBumper : thisBumpers) {
                if (thisIgnoreNamed && Objects.nonNull(thisBumper.areaName)) {
                    continue;
                }
                updatePublicBumper(thisPubBumper, thisBumper);
                for (LocalBumper otherBumper : otherBumpers) {
                    if (otherIgnoreNamed && Objects.nonNull(otherBumper.areaName)) {
                        continue;
                    }
                    other.updatePublicBumper(otherPubBumper, otherBumper);
                    if (thisPubBumper.intersects(otherPubBumper)) {
                        return true;
                    }
                }
            }
            return false;
        }

        private void updatePublicBumper(Bumper publicBumper, LocalBumper localBumper) {
            publicBumper.setCenterX(x.get() - localBumper.centerOffsetX);
            publicBumper.setCenterY(y.get() - localBumper.centerOffsetY);
            publicBumper.setRadius(localBumper.radius);
            publicBumper.setAreaName(localBumper.areaName);
        }

        int getState(int dimension) {
            return stateProperty(dimension).intValue();
        }

        IntegerProperty stateProperty(int dimension) {
            return state[dimension];
        }

        void setState(int dimension, int value) {
            stateProperty(dimension).set(value);
        }
    }

    public class Builder {
        private List<Integer> dimensions = new ArrayList<>(10);
        private List<Frame> frames = new ArrayList<>(8192);
        private List<LocalBumper> draftBumpers = new ArrayList<>(4);
        private List<LocalBumper> preciseBumpers = new ArrayList<>(16);
        private Image frameImage;
        private double frameOffsetX;
        private double frameOffsetY;
        private double frameWidth;
        private double frameHeight;
        private boolean bumperDraft;
        private double bumperCenterOffsetX;
        private double bumperCenterOffsetY;
        private double bumperRadius;
        private String bumperAreaName;

        public Builder dimensions(int... dimensions) {
            for (int d : dimensions) {
                this.dimensions.add(d);
            }
            return this;
        }

        private void check() {
            if (dimensions == null) {
                throw new IllegalStateException("Already used");
            }
        }

        public SpritePainter build() {
            check();
            SpritePainter sprite = SpritePainter.this;
            sprite.frames = new Frame[frames.size()];
            frames.toArray(sprite.frames);
            sprite.dimensionSizes = new int[dimensions.size()];
            for (int i = 0; i < dimensions.size(); i++) {
                sprite.dimensionSizes[i] = dimensions.get(i);
            }
            dimensions.clear();
            dimensions = null;
            frames.clear();
            frames = null;
            draftBumpers.clear();
            draftBumpers = null;
            preciseBumpers.clear();
            preciseBumpers = null;
            frameImage = null;
            return sprite;
        }

        public Builder frameImage(Image image) {
            check();
            this.frameImage = image;
            return this;
        }

        public Builder frameOffsetX(double frameOffsetX) {
            check();
            this.frameOffsetX = frameOffsetX;
            return this;
        }

        public Builder frameOffsetY(double frameOffsetY) {
            check();
            this.frameOffsetY = frameOffsetY;
            return this;
        }

        public Builder frameWidth(double frameWidth) {
            check();
            this.frameWidth = frameWidth;
            return this;
        }

        public Builder frameHeight(double frameHeight) {
            check();
            this.frameHeight = frameHeight;
            return this;
        }

        public Builder nextFrame() {
            check();
            Frame frame = new Frame();
            frame.image = frameImage;
            frame.offsetX = frameOffsetX;
            frame.offsetY = frameOffsetY;
            frame.width = frameWidth;
            frame.height = frameHeight;
            if (draftBumpers.isEmpty()) {
                frame.draftBumpers = null;
            } else {
                frame.draftBumpers = new LocalBumper[draftBumpers.size()];
                draftBumpers.toArray(frame.draftBumpers);
            }
            if (preciseBumpers.isEmpty()) {
                frame.preciseBumpers = null;
            } else {
                frame.preciseBumpers = new LocalBumper[preciseBumpers.size()];
                preciseBumpers.toArray(frame.preciseBumpers);
            }
            frames.add(frame);
            preciseBumpers.clear();
            draftBumpers.clear();
            return this;
        }

        public Builder draftBumper(boolean draftBumper) {
            check();
            this.bumperDraft = draftBumper;
            return this;
        }

        public Builder bumperCenterOffsetX(double bumperCenterOffsetX) {
            check();
            this.bumperCenterOffsetX = bumperCenterOffsetX;
            return this;
        }

        public Builder bumperCenterOffsetY(double bumperCenterOffsetY) {
            check();
            this.bumperCenterOffsetY = bumperCenterOffsetY;
            return this;
        }

        public Builder bumperRadius(double bumperRadius) {
            check();
            this.bumperRadius = bumperRadius;
            return this;
        }

        public Builder bumperAreaName(String name) {
            this.bumperAreaName = name;
            return this;
        }

        public Builder nextBumper() {
            check();
            LocalBumper bumper = new LocalBumper();
            bumper.centerOffsetX = bumperCenterOffsetX;
            bumper.centerOffsetY = bumperCenterOffsetY;
            bumper.radius = bumperRadius;
            bumper.areaName = bumperAreaName;
            if (bumperDraft) {
                draftBumpers.add(bumper);
            } else {
                preciseBumpers.add(bumper);
            }
            return this;
        }
    }
}
