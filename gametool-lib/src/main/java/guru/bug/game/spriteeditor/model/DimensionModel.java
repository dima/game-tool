package guru.bug.game.spriteeditor.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class DimensionModel implements Externalizable {
    private static final long serialVersionUID = 1L;

    private StringProperty name = new SimpleStringProperty();
    private ObservableList<MeasureModel> measures = FXCollections.observableArrayList();
    private ObjectProperty<MeasureModel> active = new SimpleObjectProperty<>();

    public DimensionModel() {
        measures.addListener((ListChangeListener<MeasureModel>) c -> {
            while (c.next()) {
                c.getRemoved().forEach(m -> m.writableDimensionProperty().set(null));
                c.getAddedSubList().forEach(m -> m.writableDimensionProperty().set(DimensionModel.this));
            }
        });
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public ObservableList<MeasureModel> getMeasures() {
        return measures;
    }

    public MeasureModel getActive() {
        return active.get();
    }

    public ObjectProperty<MeasureModel> activeProperty() {
        return active;
    }

    public void setActive(MeasureModel active) {
        this.active.set(active);
    }

    //<editor-fold desc="Externalization">
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(1); // version
        out.writeObject(getName());
        out.writeInt(measures.size());
        for (MeasureModel mm : measures) {
            out.writeObject(mm);
        }
        out.writeObject(getActive());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        in.readInt(); //version
        setName((String) in.readObject());
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            MeasureModel mm = (MeasureModel) in.readObject();
            measures.add(mm);
        }
        setActive((MeasureModel) in.readObject());
    }
    //</editor-fold>
}
