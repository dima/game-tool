package guru.bug.game.spriteeditor.ui;

import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class DoubleSpinner extends Spinner<Number> {
    public DoubleSpinner() {
        setEditable(true);
        SpinnerValueFactory factory = new SpinnerValueFactory.DoubleSpinnerValueFactory(-999999999, +999999999, 0d);
        //noinspection unchecked
        setValueFactory(factory);
    }
}
