package guru.bug.game.spriteeditor.model;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class ProjectModel extends LayerModel {
    private static final long serialVersionUID = 1L;

    private final ObservableList<DimensionModel> dimensions = FXCollections.observableArrayList();
    private final ObjectProperty<File> linkedWith = new SimpleObjectProperty<>();
    private final BooleanProperty modified = new SimpleBooleanProperty();

    public ProjectModel() {
        super(true);
    }

    public ObservableList<DimensionModel> getDimensions() {
        return dimensions;
    }


    public File getLinkedWith() {
        return linkedWith.get();
    }

    public ObjectProperty<File> linkedWithProperty() {
        return linkedWith;
    }

    public void setLinkedWith(File linkedWith) {
        this.linkedWith.set(linkedWith);
    }

    public boolean isModified() {
        return modified.get();
    }

    public BooleanProperty modifiedProperty() {
        return modified;
    }

    public void setModified(boolean modified) {
        this.modified.set(modified);
    }

    //<editor-fold desc="Externalization">
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(1); //version
        out.writeInt(dimensions.size());
        for (DimensionModel dm : dimensions) {
            out.writeObject(dm);
        }
        super.writeExternal(out);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        in.readInt(); // version
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            DimensionModel dm = (DimensionModel) in.readObject();
            dimensions.add(dm);
        }
        super.readExternal(in);
    }
    //</editor-fold>
}
