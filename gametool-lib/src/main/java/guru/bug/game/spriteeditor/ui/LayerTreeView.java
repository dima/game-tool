package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.LayerModel;
import guru.bug.game.spriteeditor.model.ProjectModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class LayerTreeView extends TreeView<LayerModel> {
    public static final DataFormat LAYER_DATA_FORMAT = new DataFormat("guru-bug-game-spriteeditor/layer");
    private SimpleObjectProperty<ProjectModel> project = new SimpleObjectProperty<>();

    public LayerTreeView() {
        setShowRoot(true);
        setOnDragDetected(this::dragDetected);
        setCellFactory(param -> new LayerTreeCell());
        rootProperty().bind(Bindings.createObjectBinding(() -> new LayerTreeItem(project.get()), project));
        getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    private void dragDetected(MouseEvent event) {
        TreeItem<LayerModel> item = getSelectionModel().getSelectedItem();
        if (item == null) {
            return;
        }
        LayerModel layer = item.getValue();
        if (layer instanceof ProjectModel) {
            return;
        }
        layer.getParent().children().remove(layer);
        Dragboard dragboard = startDragAndDrop(TransferMode.MOVE);
        Map<DataFormat, Object> props = new HashMap<>();
        props.put(LAYER_DATA_FORMAT, layer);
        if (layer.getImage() != null) {
            props.put(DataFormat.IMAGE, layer.getImage());
        }
        dragboard.setContent(props);
        event.consume();
    }

    public ProjectModel getProject() {
        return project.get();
    }

    public SimpleObjectProperty<ProjectModel> projectProperty() {
        return project;
    }

    public void setProject(ProjectModel project) {
        this.project.set(project);
    }
}
