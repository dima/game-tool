package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.BumperModel;
import guru.bug.game.spriteeditor.model.LayerModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.util.List;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class OutlinesView extends BorderPane {
    private final ObjectProperty<LayerModel> layer = new SimpleObjectProperty<>();

    public OutlinesView() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("OutlinesView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        disableProperty().bind(Bindings.isNull(layer));
    }

    public void addBumper() {
        BumperModel bm = new BumperModel();
        bm.setCenterX(0);
        bm.setCenterY(0);
        bm.setRadius(16);
        layer.get().getBumpers().add(bm);
    }

    public void generateBumpers() {
        BumpersGenerateDialog dialog = new BumpersGenerateDialog();
        dialog.showAndWait().ifPresent(this::addBumpers);
    }

    private void addBumpers(List<BumperModel> bumperModels) {
        layer.get().getBumpers().addAll(bumperModels);
    }

    public LayerModel getLayer() {
        return layer.get();
    }

    public ObjectProperty<LayerModel> layerProperty() {
        return layer;
    }

    public void setLayer(LayerModel layer) {
        this.layer.set(layer);
    }
}
