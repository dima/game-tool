package guru.bug.game.spriteeditor.model;

import javafx.beans.property.*;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class BumperModel implements Externalizable {
    private static final long serialVersionUID = 1L;

    private final DoubleProperty centerX = new SimpleDoubleProperty(this, "centerX");
    private final DoubleProperty centerY = new SimpleDoubleProperty(this, "centerY");
    private final DoubleProperty radius = new SimpleDoubleProperty(this, "radius");
    private final BooleanProperty draft = new SimpleBooleanProperty(this, "draft");
    private final StringProperty areaName = new SimpleStringProperty();

    public double getCenterX() {
        return centerX.get();
    }

    public DoubleProperty centerXProperty() {
        return centerX;
    }

    public void setCenterX(double centerX) {
        this.centerX.set(centerX);
    }

    public double getCenterY() {
        return centerY.get();
    }

    public DoubleProperty centerYProperty() {
        return centerY;
    }

    public void setCenterY(double centerY) {
        this.centerY.set(centerY);
    }

    public double getRadius() {
        return radius.get();
    }

    public DoubleProperty radiusProperty() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius.set(radius);
    }

    public boolean isDraft() {
        return draft.get();
    }

    public boolean getDraft() {
        return draft.get();
    }

    public BooleanProperty draftProperty() {
        return draft;
    }

    public void setDraft(boolean draft) {
        this.draft.set(draft);
    }

    public String getAreaName() {
        return areaName.get();
    }

    public StringProperty areaNameProperty() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName.set(areaName);
    }

    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(2); // version
        out.writeDouble(getCenterX());
        out.writeDouble(getCenterY());
        out.writeDouble(getRadius());
        out.writeBoolean(isDraft());
        String areaName = getAreaName();
        areaName = areaName == null ? null : areaName.trim();
        areaName = areaName != null && areaName.isEmpty() ? null : areaName;
        out.writeObject(areaName);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        int version = in.readInt(); // version
        setCenterX(in.readDouble());
        setCenterY(in.readDouble());
        setRadius(in.readDouble());
        setDraft(in.readBoolean());
        if (version > 1) {
            setAreaName((String) in.readObject());
        }
    }
}
