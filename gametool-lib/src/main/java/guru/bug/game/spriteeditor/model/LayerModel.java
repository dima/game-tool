package guru.bug.game.spriteeditor.model;

import guru.bug.game.utils.ImageExternalizer;
import javafx.beans.property.*;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class LayerModel implements Externalizable {
    private static final long serialVersionUID = 1L;

    private final ReadOnlyObjectWrapper<LayerModel> parent = new ReadOnlyObjectWrapper<>();
    private final ReadOnlyObjectWrapper<ProjectModel> project = new ReadOnlyObjectWrapper<>();
    private final ObjectProperty<Image> image = new SimpleObjectProperty<>();
    private final StringProperty name = new SimpleStringProperty();
    private final ObservableList<BumperModel> bumpers = FXCollections.observableArrayList();
    private final ValueRouter<SimpleDoubleProperty, Number> translateXRouter = new ValueRouter<>(SimpleDoubleProperty::new);
    private final ValueRouter<SimpleDoubleProperty, Number> translateYRouter = new ValueRouter<>(SimpleDoubleProperty::new);
    private final ValueRouter<SimpleDoubleProperty, Number> rotatePivotXRouter = new ValueRouter<>(SimpleDoubleProperty::new);
    private final ValueRouter<SimpleDoubleProperty, Number> rotatePivotYRouter = new ValueRouter<>(SimpleDoubleProperty::new);
    private final ValueRouter<SimpleDoubleProperty, Number> rotateAngleRouter = new ValueRouter<>(SimpleDoubleProperty::new);
    private final ValueRouter<SimpleDoubleProperty, Number> opacityRouter = new ValueRouter<>(SimpleDoubleProperty::new, 1d);
    private ObservableList<LayerModel> children;
    private boolean group;

    public LayerModel() {
        this(false);
    }

    public LayerModel(boolean group) {
        project.bind(new ProjectRootBinding(this));
        this.group = group;
        groupUpdated();
    }

    private void groupUpdated() {
        if (this.group) {
            children = FXCollections.observableArrayList();
            children.addListener(this::childrenChanged);
        } else {
            children = FXCollections.emptyObservableList();
        }
    }

    private void childrenChanged(ListChangeListener.Change<? extends LayerModel> c) {
        while (c.next()) {
            for (LayerModel m : c.getAddedSubList()) {
                m.parent.set(this);
            }
            for (LayerModel m : c.getRemoved()) {
                m.parent.set(null);
            }
        }
    }

    public ProjectModel getProject() {
        return project.get();
    }

    public ReadOnlyObjectProperty<ProjectModel> projectProperty() {
        return project.getReadOnlyProperty();
    }

    public LayerModel getParent() {
        return parent.get();
    }

    public ReadOnlyObjectProperty<LayerModel> parentProperty() {
        return parent.getReadOnlyProperty();
    }

    public Image getImage() {
        return image.get();
    }

    public ObjectProperty<Image> imageProperty() {
        return image;
    }

    public void setImage(Image image) {
        this.image.set(image);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public boolean isGroup() {
        return group;
    }

    public ObservableList<LayerModel> children() {
        return children;
    }

    public ObservableList<BumperModel> getBumpers() {
        return bumpers;
    }

    public ValueRouter<SimpleDoubleProperty, Number> getTranslateXRouter() {
        return translateXRouter;
    }

    public ValueRouter<SimpleDoubleProperty, Number> getTranslateYRouter() {
        return translateYRouter;
    }

    public ValueRouter<SimpleDoubleProperty, Number> getRotatePivotXRouter() {
        return rotatePivotXRouter;
    }

    public ValueRouter<SimpleDoubleProperty, Number> getRotatePivotYRouter() {
        return rotatePivotYRouter;
    }

    public ValueRouter<SimpleDoubleProperty, Number> getRotateAngleRouter() {
        return rotateAngleRouter;
    }

    public ValueRouter<SimpleDoubleProperty, Number> getOpacityRouter() {
        return opacityRouter;
    }

    //<editor-fold desc="Externalization">
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(1); // version
        out.writeBoolean(group);
        out.writeObject(getName());
        ImageExternalizer.writeImage(out, getImage());
        if (group) {
            out.writeInt(children.size());
            for (LayerModel m : children) {
                out.writeObject(m);
            }
        }
        out.writeInt(bumpers.size());
        for (BumperModel bm : bumpers) {
            out.writeObject(bm);
        }
        translateXRouter.writeExternal(out);
        translateYRouter.writeExternal(out);
        rotatePivotXRouter.writeExternal(out);
        rotatePivotYRouter.writeExternal(out);
        rotateAngleRouter.writeExternal(out);
        opacityRouter.writeExternal(out);
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        in.readInt(); // version
        group = in.readBoolean();
        groupUpdated();
        setName((String) in.readObject());
        setImage(ImageExternalizer.readImage(in));
        if (group) {
            int size = in.readInt();
            for (int i = 0; i < size; i++) {
                LayerModel m = (LayerModel) in.readObject();
                children.add(m);
            }
        }
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            BumperModel bm = (BumperModel) in.readObject();
            bumpers.add(bm);
        }
        translateXRouter.readExternal(in);
        translateYRouter.readExternal(in);
        rotatePivotXRouter.readExternal(in);
        rotatePivotYRouter.readExternal(in);
        rotateAngleRouter.readExternal(in);
        opacityRouter.readExternal(in);
    }

    //</editor-fold>
}
