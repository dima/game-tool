package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.DimensionModel;
import guru.bug.game.spriteeditor.model.MeasureModel;
import guru.bug.game.spriteeditor.model.ProjectModel;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.BorderPane;

import java.io.IOException;
import java.util.List;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class DimensionManager extends BorderPane {
    private final ObjectProperty<ProjectModel> project = new SimpleObjectProperty<>();
    public DimensionTreeView dimensionTreeView;

    public DimensionManager() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("DimensionManager.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    public void addDimension() {
        DimensionModel dm = new DimensionModel();
        List<DimensionModel> list = project.get().getDimensions();
        dm.setName("Dimension#" + (list.size() + 1));
        list.add(dm);
    }

    public void addMeasure() {
        TreeItem ti = (TreeItem) dimensionTreeView.getSelectionModel().getSelectedItem();
        Object obj = ti.getValue();
        DimensionModel dm = null;
        if (obj instanceof DimensionModel) {
            dm = (DimensionModel) obj;
        } else if (obj instanceof MeasureModel) {
            dm = (DimensionModel) ti.getParent().getValue();
        }

        if (dm != null) {
            MeasureModel mm = new MeasureModel();
            List<MeasureModel> list = dm.getMeasures();
            mm.setName("Measure#" + (list.size() + 1));
            list.add(mm);
        }
    }

    public ProjectModel getProject() {
        return project.get();
    }

    public ObjectProperty<ProjectModel> projectProperty() {
        return project;
    }

    public void setProject(ProjectModel project) {
        this.project.set(project);
    }

    public DimensionModel getSelectedDimension() {
        return dimensionTreeView.getSelectedDimension();
    }

    public ReadOnlyObjectProperty<DimensionModel> selectedDimensionProperty() {
        return dimensionTreeView.selectedDimensionProperty();
    }
}
