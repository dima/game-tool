package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.BumperModel;
import guru.bug.game.spriteeditor.model.LayerModel;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.collections.ListChangeListener;
import javafx.geometry.Bounds;
import javafx.scene.Group;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Transform;
import javafx.scene.transform.Translate;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class LayerView extends Group {
    private final LayerModel layer;
    private final ProjectView root;
    private final Translate translate = Transform.translate(0, 0);
    private final Rotate rotate = Transform.rotate(0, 0, 0);
    private final DoubleBinding strokeWidth;

    public LayerView(LayerModel layer) {
        this(null, layer);
    }

    public LayerView(ProjectView root, LayerModel layer) {
        this.root = root;
        this.layer = layer;
        if (root == null) {
            strokeWidth = null;
        } else {
            strokeWidth = Bindings.divide(1d, root.scaleXProperty());
        }
        layer.children().addListener((ListChangeListener<? super LayerModel>) c -> rebuildChildren());
        layer.getBumpers().addListener((ListChangeListener<? super BumperModel>) c -> rebuildChildren());
        rebuildChildren();
        translate.xProperty().bind(layer.getTranslateXRouter().valueProperty());
        translate.yProperty().bind(layer.getTranslateYRouter().valueProperty());
        rotate.pivotXProperty().bind(layer.getRotatePivotXRouter().valueProperty());
        rotate.pivotYProperty().bind(layer.getRotatePivotYRouter().valueProperty());
        rotate.angleProperty().bind(layer.getRotateAngleRouter().valueProperty());
        opacityProperty().bind(layer.getOpacityRouter().valueProperty());
        getTransforms().setAll(rotate, translate);
    }

    private void rebuildChildren() {
        if (layer.isGroup()) {
            List<LayerView> tmp = layer.children()
                    .stream()
                    .map(m -> new LayerView(root, m))
                    .collect(Collectors.toList());
            Collections.reverse(tmp);
            getChildren().setAll(tmp);
        } else {
            ImageView iv = new ImageView();
            iv.imageProperty().bind(layer.imageProperty());
            getChildren().setAll(iv);
        }
        if (root != null) {
            addOutline();
        }
    }

    private void addOutline() {
        Group outline = new Group();
        Bounds bounds = getBoundsInLocal();
        Rectangle frame = new Rectangle(bounds.getMinX(), bounds.getMinY(), bounds.getWidth(), bounds.getHeight());
        frame.setFill(Color.TRANSPARENT);
        frame.setStroke(Color.RED);
        frame.strokeWidthProperty().bind(strokeWidth);
        Circle circle = new Circle(2, Color.GREEN);
        circle.centerXProperty().bind(rotate.pivotXProperty());
        circle.centerYProperty().bind(rotate.pivotYProperty());
        circle.strokeWidthProperty().bind(strokeWidth);

        outline.getChildren().addAll(frame, circle);
        getChildren().add(outline);
        outline.visibleProperty().bind(Bindings.equal(layer, root.selectedLayerProperty()));

        for (BumperModel bm : layer.getBumpers()) {
            Circle bmc = new Circle();
            bmc.centerXProperty().bind(bm.centerXProperty());
            bmc.centerYProperty().bind(bm.centerYProperty());
            bmc.radiusProperty().bind(bm.radiusProperty());
            bmc.setFill(Color.TRANSPARENT);
            bmc.strokeProperty().bind(Bindings.when(bm.draftProperty()).then(Color.LIGHTBLUE).otherwise(Color.BLUE));
            bmc.strokeWidthProperty().bind(strokeWidth);
            getChildren().add(bmc);
        }
    }

    public LayerModel getLayer() {
        return layer;
    }
}
