package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.LayerModel;
import javafx.scene.control.TreeCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;

import java.io.File;
import java.net.MalformedURLException;

import static guru.bug.game.spriteeditor.ui.LayerTreeView.LAYER_DATA_FORMAT;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class LayerTreeCell extends TreeCell<LayerModel> {
    private ImageView imageView = new ImageView();

    public LayerTreeCell() {
        imageView.setPreserveRatio(true);
        imageView.setFitWidth(32);
        imageView.setFitHeight(32);
        setOnDragOver(this::dragOver);
        setOnDragDropped(this::dragDropped);
        setOnDragExited(this::dragExited);
    }

    @Override
    protected void updateItem(LayerModel item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setGraphic(null);
            setText(null);
            imageView.setImage(null);
            removeAcceptingFrame();
        } else {
            imageView.setImage(item.getImage());
            setGraphic(imageView);
            setText(item.getName());
        }
    }

    private void removeAcceptingFrame() {
        //noinspection StatementWithEmptyBody
        while (getStyleClass().remove("drag-accept")) {
            //do nothing
        }
    }

    private void addAcceptingFrame() {
        if (!getStyleClass().contains("drag-accept")) {
            getStyleClass().add("drag-accept");
        }
    }

    public void dragOver(DragEvent e) {
        LayerModel lm = getItem();
        if (lm != null) {
            if (lm.isGroup()) {
                dragOverGroup(e);
            } else {
                dragOverLayer(e);
            }
        }
        e.consume();
    }

    private void dragOverLayer(DragEvent e) {
        Dragboard db = e.getDragboard();
        if (db.hasFiles() && db.getFiles().size() == 1) {
            e.acceptTransferModes(TransferMode.COPY);
            addAcceptingFrame();
        }
    }

    private void dragOverGroup(DragEvent e) {
        Dragboard db = e.getDragboard();
        if (db.hasContent(LAYER_DATA_FORMAT)) {
            e.acceptTransferModes(TransferMode.MOVE);
            addAcceptingFrame();
        } else if (e.getDragboard().hasFiles()) {
            e.acceptTransferModes(TransferMode.COPY);
            addAcceptingFrame();
        }
    }

    public void dragDropped(DragEvent event) {
        removeAcceptingFrame();
        LayerModel item = getItem();
        if (item != null) {
            if (item.isGroup()) {
                dragDroppedOnGroup(event);
            } else {
                dragDroppedOnLayer(event);
            }
        }
        event.consume();
    }

    private void dragDroppedOnLayer(DragEvent event) {
        Dragboard db = event.getDragboard();
        LayerModel layer = getItem();
        if (db.hasFiles() && db.getFiles().size() == 1) {
            try {
                File f = db.getFiles().get(0);
                Image img = new Image(f.toURI().toURL().toExternalForm());
                layer.setImage(img);
                layer.setName(f.getName());
            } catch (MalformedURLException e) {
                throw new IllegalArgumentException(e);
            }
            event.setDropCompleted(true);
        } else {
            event.setDropCompleted(false);
        }
    }

    private void dragDroppedOnGroup(DragEvent event) {
        Dragboard db = event.getDragboard();
        LayerModel parent = getItem();
        if (db.hasContent(LAYER_DATA_FORMAT)) {
            LayerModel lm = (LayerModel) db.getContent(LAYER_DATA_FORMAT);
            parent.children().add(lm);
            event.setDropCompleted(true);
        } else if (db.hasFiles()) {
            db.getFiles().forEach(f -> {
                try {
                    Image img = new Image(f.toURI().toURL().toExternalForm());
                    LayerModel pi = new LayerModel();
                    pi.setImage(img);
                    pi.setName(f.getName());
                    parent.children().add(pi);
                } catch (MalformedURLException e) {
                    throw new IllegalArgumentException(e);
                }
            });
            event.setDropCompleted(true);
        } else {
            event.setDropCompleted(false);
        }
    }

    private void dragExited(DragEvent dragEvent) {
        removeAcceptingFrame();
    }
}
