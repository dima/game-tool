package guru.bug.game.spriteeditor.ui;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class ImageButton extends Button {
    private final StringProperty url = new SimpleStringProperty();
    private final DoubleProperty size = new SimpleDoubleProperty(24);
    private final ObjectExpression<Image> image = Bindings.createObjectBinding(() -> {
        if (url.get() == null) {
            return null;
        }
        URL u = ImageButton.class.getClassLoader().getResource(url.get());
        if (u == null) {
            return null;
        }
        return new Image(u.toExternalForm());
    }, url);

    public ImageButton() {
        setText(null);
        ImageView graphic = new ImageView();
        graphic.setPreserveRatio(true);
        graphic.fitWidthProperty().bind(size);
        graphic.fitHeightProperty().bind(size);
        graphic.imageProperty().bind(image);
        setGraphic(graphic);
    }

    public ImageButton(String url, double size) {
        this();
        this.size.set(size);
        this.url.set(url);
    }

    public String getUrl() {
        return url.get();
    }

    public StringProperty urlProperty() {
        return url;
    }

    public void setUrl(String url) {
        this.url.set(url);
    }

    public double getSize() {
        return size.get();
    }

    public DoubleProperty sizeProperty() {
        return size;
    }

    public void setSize(double size) {
        this.size.set(size);
    }
}
