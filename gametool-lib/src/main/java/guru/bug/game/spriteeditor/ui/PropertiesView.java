package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.DimensionModel;
import guru.bug.game.spriteeditor.model.LayerModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.layout.GridPane;

import java.io.IOException;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class PropertiesView extends GridPane {
    private final ObjectProperty<LayerModel> layer = new SimpleObjectProperty<>();
    public Spinner<Number> lTranslateX;
    public ComboBox<DimensionModel> lTranslateXDim;
    public Spinner<Number> lTranslateY;
    public ComboBox<DimensionModel> lTranslateYDim;
    public Spinner<Number> lRotatePivotX;
    public ComboBox<DimensionModel> lRotatePivotXDim;
    public Spinner<Number> lRotatePivotY;
    public ComboBox<DimensionModel> lRotatePivotYDim;
    public Spinner<Number> lRotateAngle;
    public ComboBox<DimensionModel> lRotateAngleDim;
    public Spinner<Number> lOpacity;
    public ComboBox<DimensionModel> lOpacityDim;

    public PropertiesView() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PropertiesView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        disableProperty().bind(Bindings.isNull(layer));
        layer.addListener((o, ov, nv) -> {
            if (ov != null) {
                stopEdit(ov);
            }
            if (nv != null) {
                startEdit(nv);
            }
        });
    }

    private void startEdit(LayerModel l) {
        l.getTranslateXRouter().startEdit(lTranslateX.getValueFactory().valueProperty(), lTranslateXDim.valueProperty());
        l.getTranslateYRouter().startEdit(lTranslateY.getValueFactory().valueProperty(), lTranslateYDim.valueProperty());
        l.getRotatePivotXRouter().startEdit(lRotatePivotX.getValueFactory().valueProperty(), lRotatePivotXDim.valueProperty());
        l.getRotatePivotYRouter().startEdit(lRotatePivotY.getValueFactory().valueProperty(), lRotatePivotYDim.valueProperty());
        l.getRotateAngleRouter().startEdit(lRotateAngle.getValueFactory().valueProperty(), lRotateAngleDim.valueProperty());
        l.getOpacityRouter().startEdit(lOpacity.getValueFactory().valueProperty(), lOpacityDim.valueProperty());
    }

    private void stopEdit(LayerModel l) {
        l.getTranslateXRouter().stopEdit(lTranslateX.getValueFactory().valueProperty());
        l.getTranslateYRouter().stopEdit(lTranslateY.getValueFactory().valueProperty());
        l.getRotatePivotXRouter().stopEdit(lRotatePivotX.getValueFactory().valueProperty());
        l.getRotatePivotYRouter().stopEdit(lRotatePivotY.getValueFactory().valueProperty());
        l.getRotateAngleRouter().stopEdit(lRotateAngle.getValueFactory().valueProperty());
        l.getOpacityRouter().stopEdit(lOpacity.getValueFactory().valueProperty());
    }

    public LayerModel getLayer() {
        return layer.get();
    }

    public ObjectProperty<LayerModel> layerProperty() {
        return layer;
    }

    public void setLayer(LayerModel layer) {
        this.layer.set(layer);
    }
}
