package guru.bug.game.spriteeditor.ui;

import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.TransformationList;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class MappedList<T, S> extends TransformationList<T, S> {
    private final List<T> target = new ArrayList<>();
    private final Function<S, T> mapFunction;

    public MappedList(ObservableList<S> source, Function<S, T> mapFunction) {
        super(source);
        this.mapFunction = mapFunction;
        source.stream()
                .map(mapFunction)
                .forEach(target::add);
    }

    @Override
    protected void sourceChanged(ListChangeListener.Change<? extends S> c) {
        beginChange();
        try {
            while (c.next()) {
                if (c.wasPermutated()) {
                    int[] perm = new int[c.getTo() - c.getFrom()];
                    for (int i = c.getFrom(); i < c.getTo(); ++i) {
                        T a = target.remove(i);
                        target.add(c.getPermutation(i), a);
                        perm[i - c.getFrom()] = c.getPermutation(i);
                    }
                    nextPermutation(c.getFrom(), c.getTo(), perm);
                } else if (c.wasReplaced()) {
                    List<T> removed = new ArrayList<>(target.subList(c.getFrom(), c.getTo()));
                    for (int i = c.getFrom(); i < c.getTo(); ++i) {
                        target.set(i, mapFunction.apply(getSource().get(i)));
                    }
                    nextReplace(c.getFrom(), c.getTo(), removed);
                } else if (c.wasRemoved()) {
                    List<T> remSubList = target.subList(c.getFrom(), c.getTo());
                    List<T> removed = new ArrayList<>(remSubList);
                    remSubList.clear();
                    nextRemove(c.getFrom(), removed);
                } else if (c.wasAdded()) {
                    for (int i = c.getFrom(); i < c.getTo(); ++i) {
                        target.add(i, mapFunction.apply(getSource().get(i)));
                    }
                    nextAdd(c.getFrom(), c.getTo());
                }
            }
        } finally {
            endChange();
        }
    }

    @Override
    public int getSourceIndex(int index) {
        return index;
    }

    @Override
    public int getViewIndex(int i) {
        return 0;
    }

    @Override
    public T get(int index) {
        return target.get(index);
    }

    @Override
    public int size() {
        return getSource().size();
    }
}
