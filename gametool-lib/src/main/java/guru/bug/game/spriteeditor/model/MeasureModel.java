package guru.bug.game.spriteeditor.model;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class MeasureModel implements Externalizable {
    private static final long serialVersionUID = 1L;
    private ReadOnlyBooleanWrapper active = new ReadOnlyBooleanWrapper();
    private StringProperty name = new SimpleStringProperty();
    private ReadOnlyObjectWrapper<DimensionModel> dimension = new ReadOnlyObjectWrapper<>();

    public MeasureModel() {
        active.bind(Bindings.equal(this, Bindings.select(dimension, "active")));
    }

    public boolean isActive() {
        return active.get();
    }

    public ReadOnlyBooleanProperty activeProperty() {
        return active.getReadOnlyProperty();
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public DimensionModel getDimension() {
        return dimension.get();
    }

    public ReadOnlyObjectProperty<DimensionModel> dimensionProperty() {
        return dimension.getReadOnlyProperty();
    }

    ObjectProperty<DimensionModel> writableDimensionProperty() {
        return dimension;
    }

    @Override
    public boolean equals(Object o) {
        return this == o;
    }

    @Override
    public int hashCode() {
        return System.identityHashCode(this);
    }

    //<editor-fold desc="Externalization">
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(1); // version
        out.writeObject(getName());
    }

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        in.readInt(); // version
        setName((String) in.readObject());
    }
    //</editor-fold>
}
