package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.LayerModel;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class LayerTreeItem extends TreeItem<LayerModel> {
    ImageView graphics = new ImageView();

    public LayerTreeItem(LayerModel value) {
        super(value);
        graphics.setFitHeight(32);
        graphics.setFitWidth(32);
        graphics.setPreserveRatio(true);
        setGraphic(graphics);
        valueProperty().addListener(this::valueChanged);
        setExpanded(true);
        if (value != null) {
            bindValue(value);
        }
    }

    private void valueChanged(ObservableValue<? extends LayerModel> o, LayerModel ov, LayerModel nv) {
        if (ov != null) {
            unbindValue(ov);
        }
        if (nv != null) {
            bindValue(nv);
        }
    }

    private void bindValue(LayerModel nv) {
        nv.children().addListener(this::layerChildrenChanged);
        graphics.imageProperty().bind(nv.imageProperty());
        rebuildChildren(nv);
    }

    private void unbindValue(LayerModel ov) {
        ov.children().removeListener(this::layerChildrenChanged);
        graphics.imageProperty().unbind();
        graphics.setImage(null);
        getChildren().clear();
    }

    private void rebuildChildren(LayerModel nv) {
        List<TreeItem<LayerModel>> tmp = nv.children()
                .stream()
                .map(LayerTreeItem::new)
                .collect(Collectors.toList());
        getChildren().setAll(tmp);
    }

    private void layerChildrenChanged(ListChangeListener.Change<? extends LayerModel> c) {
        rebuildChildren(getValue());
    }
}
