package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.DimensionModel;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.util.StringConverter;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class DimensionsComboBox extends ComboBox<DimensionModel> {
    public DimensionsComboBox() {
        setMaxWidth(Double.POSITIVE_INFINITY);
        setCellFactory(p -> new ListCell<DimensionModel>() {
            @Override
            protected void updateItem(DimensionModel item, boolean empty) {
                super.updateItem(item, empty);
                if (!empty) {
                    setText(item.getName());
                }
            }
        });

        setConverter(new StringConverter<DimensionModel>() {
            @Override
            public String toString(DimensionModel object) {
                return object == null ? "none" : object.getName();
            }

            @Override
            public DimensionModel fromString(String string) {
                return null;
            }
        });

    }
}
