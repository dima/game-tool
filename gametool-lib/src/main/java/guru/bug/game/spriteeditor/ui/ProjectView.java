package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.DimensionModel;
import guru.bug.game.spriteeditor.model.LayerModel;
import guru.bug.game.spriteeditor.model.ProjectModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.StackPane;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class ProjectView extends StackPane {
    private final ObjectProperty<ProjectModel> project = new SimpleObjectProperty<>();
    private final ObjectProperty<LayerModel> selectedLayer = new SimpleObjectProperty<>();
    private final ObjectProperty<DimensionModel> selectedDimension = new SimpleObjectProperty<>();
    private final ReadOnlyObjectWrapper<LayerView> rootLayer = new ReadOnlyObjectWrapper<>();

    public ProjectView() {
        setCenterShape(true);
        setOnScroll(this::scrollProjectView);
        rootLayer.bind(Bindings.createObjectBinding(() -> {
            if (project.get() == null) {
                return null;
            } else {
                return new LayerView(this, project.get());
            }
        }, project));
        rootLayer.addListener(this::rootLayerChanged);
    }

    private void rootLayerChanged(ObservableValue<? extends LayerView> o, LayerView ov, LayerView nv) {
        if (ov != null) {
            getChildren().remove(ov);
        }
        if (nv != null) {
            getChildren().add(nv);
        }
    }

    private void scrollProjectView(ScrollEvent event) {
        double dir = Math.signum(event.getDeltaY());
        double scale = getScaleX();
        double inc = scale / 10d * dir;
        setScaleX(scale + inc);
        setScaleY(scale + inc);
        event.consume();
    }

    public ProjectModel getProject() {
        return project.get();
    }

    public ObjectProperty<ProjectModel> projectProperty() {
        return project;
    }

    public void setProject(ProjectModel project) {
        this.project.set(project);
    }

    public LayerModel getSelectedLayer() {
        return selectedLayer.get();
    }

    public ObjectProperty<LayerModel> selectedLayerProperty() {
        return selectedLayer;
    }

    public void setSelectedLayer(LayerModel selectedLayer) {
        this.selectedLayer.set(selectedLayer);
    }

    public LayerView getRootLayer() {
        return rootLayer.get();
    }

    public ReadOnlyObjectProperty<LayerView> rootLayerProperty() {
        return rootLayer.getReadOnlyProperty();
    }

    public DimensionModel getSelectedDimension() {
        return selectedDimension.get();
    }

    public ObjectProperty<DimensionModel> selectedDimensionProperty() {
        return selectedDimension;
    }

    public void setSelectedDimension(DimensionModel selectedDimension) {
        this.selectedDimension.set(selectedDimension);
    }
}
