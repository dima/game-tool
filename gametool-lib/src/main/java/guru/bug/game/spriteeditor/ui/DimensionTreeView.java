package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.DimensionModel;
import guru.bug.game.spriteeditor.model.MeasureModel;
import guru.bug.game.spriteeditor.model.ProjectModel;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ListBinding;
import javafx.beans.binding.ListExpression;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.*;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class DimensionTreeView extends TreeView {
    private final ObjectProperty<ProjectModel> project = new SimpleObjectProperty<>();
    private final ReadOnlyObjectWrapper<DimensionModel> selectedDimension = new ReadOnlyObjectWrapper<>();

    public DimensionTreeView() {
        setShowRoot(false);
        ProjectTreeItem root = new ProjectTreeItem();
        //noinspection unchecked
        root.valueProperty().bind(project);
        //noinspection unchecked
        setRoot(root);
        //noinspection unchecked
        setCellFactory(t -> new TreeCell() {
            @Override
            protected void updateItem(Object item, boolean empty) {
                //noinspection unchecked
                super.updateItem(item, empty);
                if (empty) {
                    setText(null);
                    setGraphic(null);
                    return;
                }
                if (item instanceof DimensionModel) {
                    DimensionModel dm = (DimensionModel) item;
                    setText(dm.getName());
                    setGraphic(null);
                } else if (item instanceof MeasureModel) {
                    MeasureModel mm = (MeasureModel) item;
                    setText(mm.getName());
                    ToggleButton cb = new ImageToggleButton("guru/bug/game/spriteeditor/ui/edit-document-24.png");
                    setGraphic(cb);
                    DimensionTreeItem dti = (DimensionTreeItem) getTreeItem().getParent();
                    cb.setToggleGroup(dti.getToggleGroup());
                    cb.setUserData(mm);
                }
            }
        });
        selectedDimension.bind(Bindings.createObjectBinding(() -> {
            Object item = getSelectionModel().getSelectedItem();
            if (item == null) {
                return null;
            }
            if (item instanceof MeasureTreeItem) {
                MeasureTreeItem mm = (MeasureTreeItem) item;
                item = mm.getParent();
            }
            if (item instanceof DimensionTreeItem) {
                DimensionTreeItem dd = (DimensionTreeItem) item;
                return (DimensionModel) dd.getValue();
            }
            return null;
        }, getSelectionModel().selectedItemProperty()));
    }

    public ProjectModel getProject() {
        return project.get();
    }

    public ObjectProperty<ProjectModel> projectProperty() {
        return project;
    }

    public void setProject(ProjectModel project) {
        this.project.set(project);
    }

    public DimensionModel getSelectedDimension() {
        return selectedDimension.get();
    }

    public ReadOnlyObjectProperty<DimensionModel> selectedDimensionProperty() {
        return selectedDimension.getReadOnlyProperty();
    }

    private static class ProjectTreeItem extends TreeItem {
        private ObjectExpression<ObservableList<DimensionModel>> dimensions = Bindings.select(valueProperty(), "dimensions");
        private ListExpression<DimensionTreeItem> children = new ListBinding<DimensionTreeItem>() {
            {
                bind(dimensions);
            }

            @Override
            protected ObservableList<DimensionTreeItem> computeValue() {
                if (dimensions.get() == null) {
                    return new MappedList<>(FXCollections.emptyObservableList(), DimensionTreeItem::new);
                } else {
                    return new MappedList<>(dimensions.get(), DimensionTreeItem::new);
                }
            }
        };


        ProjectTreeItem() {
            //noinspection unchecked
            Bindings.bindContent(getChildren(), children);
        }

    }

    private static class DimensionTreeItem extends TreeItem {
        private ObjectExpression<ObservableList<MeasureModel>> measures = Bindings.select(valueProperty(), "measures");
        private ToggleGroup toggleGroup = new ToggleGroup();
        private ListExpression<MeasureTreeItem> children = new ListBinding<MeasureTreeItem>() {
            {
                bind(measures);
            }

            @Override
            protected ObservableList<MeasureTreeItem> computeValue() {
                if (measures.get() == null) {
                    return new MappedList<>(FXCollections.emptyObservableList(), MeasureTreeItem::new);
                } else {
                    return new MappedList<>(measures.get(), MeasureTreeItem::new);
                }
            }
        };

        DimensionTreeItem(DimensionModel dimensionModel) {
            //noinspection unchecked
            super(dimensionModel);
            //noinspection unchecked
            Bindings.bindContent(getChildren(), children);
            toggleGroup.selectedToggleProperty().addListener((o, ov, nv) -> {
                if (nv == null) {
                    dimensionModel.setActive(null);
                } else {
                    MeasureModel mm = (MeasureModel) nv.getUserData();
                    dimensionModel.setActive(mm);
                }
            });

        }

        public ToggleGroup getToggleGroup() {
            return toggleGroup;
        }
    }

    private static class MeasureTreeItem extends TreeItem {

        MeasureTreeItem(MeasureModel measure) {
            //noinspection unchecked
            super(measure);
        }
    }
}
