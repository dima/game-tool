package guru.bug.game.spriteeditor.model;

import javafx.beans.binding.ObjectBinding;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class ProjectRootBinding extends ObjectBinding<ProjectModel> {
    private LayerModel oldParent;
    private LayerModel owner;

    public ProjectRootBinding(LayerModel owner) {
        this.owner = owner;
        if (owner instanceof ProjectModel) {
            return;
        }
        this.oldParent = owner.getParent();
        bind(owner.parentProperty());
        if (oldParent != null) {
            bind(oldParent.projectProperty());
        }
    }

    @Override
    protected ProjectModel computeValue() {
        if (owner instanceof ProjectModel) {
            return (ProjectModel) owner;
        }
        LayerModel newParent = owner.getParent();
        if (newParent != oldParent) {
            if (oldParent != null) {
                unbind(oldParent.projectProperty());
            }
            if (newParent != null) {
                bind(newParent.projectProperty());
            }
            oldParent = newParent;
        }
        if (newParent == null) {
            return null;
        }
        return newParent.getProject();
    }
}
