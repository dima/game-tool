package guru.bug.game.spriteeditor.ui;

import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class ImageToggleButton extends ToggleButton {
    public ImageToggleButton(String url) {
        ImageView graphic = new ImageView();
        graphic.setPreserveRatio(true);
        graphic.setFitWidth(16);
        graphic.setFitHeight(16);
        //noinspection ConstantConditions
        Image image = new Image(getClass().getClassLoader().getResource(url).toExternalForm());
        graphic.setImage(image);
        setGraphic(graphic);
        setText(null);
    }
}
