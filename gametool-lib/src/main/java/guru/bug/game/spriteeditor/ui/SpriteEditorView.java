package guru.bug.game.spriteeditor.ui;

import guru.bug.game.Direction;
import guru.bug.game.spriteeditor.model.DimensionModel;
import guru.bug.game.spriteeditor.model.LayerModel;
import guru.bug.game.spriteeditor.model.MeasureModel;
import guru.bug.game.spriteeditor.model.ProjectModel;
import guru.bug.game.utils.Generator;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class SpriteEditorView extends BorderPane {
    public ProjectView projectView;
    public LayerManager layerManager;
    public ObjectProperty<ProjectModel> project = new SimpleObjectProperty<>(new ProjectModel());

    public SpriteEditorView() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SpriteEditorView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        project.get().setName("Untitled Project");
    }

    public void saveProject() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Save project");
        File linked = project.get().getLinkedWith();
        if (linked != null) {
            File dir = linked.getParentFile();
            fc.setInitialDirectory(dir);
            fc.setInitialFileName(linked.getName());
        } else {
            fc.setInitialFileName(project.get().getName());
        }
        File file = fc.showSaveDialog(getScene().getWindow());
        if (file != null) {
            project.get().setName(file.getName());
            try (OutputStream os = new FileOutputStream(file);
                 ObjectOutputStream oos = new ObjectOutputStream(os)) {
                oos.writeObject(project.get());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
            project.get().setLinkedWith(file);
        }
    }

    public void loadProject() {
        FileChooser fc = new FileChooser();
        fc.setTitle("Open project");
        File linked = project.get().getLinkedWith();
        if (linked != null) {
            File dir = linked.getParentFile();
            fc.setInitialDirectory(dir);
        }
        File file = fc.showOpenDialog(getScene().getWindow());
        if (file != null) {
            try (InputStream is = new FileInputStream(file);
                 ObjectInputStream ois = new ObjectInputStream(is)) {
                ProjectModel pm = (ProjectModel) ois.readObject();
                project.set(pm);
            } catch (IOException | ClassNotFoundException e) {
                throw new IllegalStateException(e);
            }
            project.get().setLinkedWith(file);
            project.get().setName(file.getName());
        }
    }

    public void generate() throws IOException {
        saveProject();
        Generator generator = new Generator(getProject());
        generator.generate();
    }

    public void clearDim() {
        DimensionModel dim = projectView.getSelectedDimension();
        if (dim == null) {
            return;
        }
        dim.setActive(null);
        Iterator<MeasureModel> i = dim.getMeasures().iterator();
        while (i.hasNext()) {
            i.remove();
        }
    }

    public void rotate() {
        DimensionModel dim = projectView.getSelectedDimension();
        LayerModel group = projectView.getSelectedLayer();
        if (dim == null || group == null) {
            return;
        }
        dim.setActive(null);

        double startAngle = group.getRotateAngleRouter().valueProperty().get();
        layerManager.selectLayer(null);
        group.getRotateAngleRouter().setDimension(null);
        ObservableList<MeasureModel> measures = dim.getMeasures();
        measures.clear();
        group.getRotateAngleRouter().setDimension(dim);
        for (Direction dir : Direction.values()) {
            double angle = startAngle + Math.toDegrees(dir.getAngle());
            MeasureModel mm = new MeasureModel();
            mm.setName("Measure#" + (measures.size() + 1));
            measures.add(mm);
            dim.setActive(mm);
            group.getRotateAngleRouter().valueProperty().setValue(angle);
        }
    }

    public void syncLayersAndDimension() {
        DimensionModel dim = projectView.getSelectedDimension();
        LayerModel group = projectView.getSelectedLayer();
        if (dim == null || group == null) {
            return;
        }
        ObservableList<MeasureModel> measures = dim.getMeasures();
        ObservableList<LayerModel> layers = group.children();
        int toAddSize = layers.size() - measures.size();
        if (toAddSize > 0) {
            List<MeasureModel> toAdd = new ArrayList<>(toAddSize);
            while (toAdd.size() + measures.size() < layers.size()) {
                MeasureModel mm = new MeasureModel();
                mm.setName("Measure#" + (measures.size() + toAdd.size() + 1));
                toAdd.add(mm);
            }
            measures.addAll(toAdd);
        }
        for (int i = 0; i < layers.size(); i++) {
            LayerModel l = layers.get(i);
            MeasureModel m = measures.get(i);
            dim.setActive(null);
            l.getOpacityRouter().setDimension(null);
            l.getOpacityRouter().valueProperty().set(0);
            l.getOpacityRouter().setDimension(dim);
            dim.setActive(m);
            l.getOpacityRouter().valueProperty().set(1);
        }
    }

    public ProjectModel getProject() {
        return project.get();
    }

    public ObjectProperty<ProjectModel> projectProperty() {
        return project;
    }

    public void setProject(ProjectModel project) {
        this.project.set(project);
    }
}
