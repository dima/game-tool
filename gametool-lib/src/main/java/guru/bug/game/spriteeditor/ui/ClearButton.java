package guru.bug.game.spriteeditor.ui;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.ComboBox;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class ClearButton extends ImageButton {
    private ObjectProperty<ComboBox> forComboBox = new SimpleObjectProperty<>();

    public ClearButton() {
        super("guru/bug/game/spriteeditor/ui/cross-48.png", 24);
    }

    @Override
    public void fire() {
        if (!isDisabled() && forComboBox.get() != null) {
            forComboBox.get().getSelectionModel().clearSelection();
        }
    }

    public ComboBox getForComboBox() {
        return forComboBox.get();
    }

    public ObjectProperty<ComboBox> forComboBoxProperty() {
        return forComboBox;
    }

    public void setForComboBox(ComboBox forComboBox) {
        this.forComboBox.set(forComboBox);
    }
}
