package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.BumperModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyBooleanProperty;
import javafx.beans.property.ReadOnlyBooleanWrapper;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class BumpersGenerateDialog extends Dialog<List<BumperModel>> {
    private Content content = new Content();

    public BumpersGenerateDialog() {
        try (InputStream is = getClass().getResourceAsStream("magic-wand-48.png")) {
            setGraphic(new ImageView(new Image(is)));
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        setResultConverter(this::convertResult);
        getDialogPane().getButtonTypes().setAll(ButtonType.OK, ButtonType.CANCEL);
        getDialogPane().setContent(content);
        getDialogPane().lookupButton(ButtonType.OK).disableProperty().bind(Bindings.not(content.validProperty()));
    }


    private List<BumperModel> convertResult(ButtonType buttonType) {
        if (buttonType != ButtonType.OK) {
            return Collections.emptyList();
        }
        double nsx = content.stepXSpin.valueProperty().getValue().doubleValue();
        if (nsx == 0) {
            return generateByY();
        } else {
            return generateByX();
        }
    }

    private List<BumperModel> generateByX() {
        double fx = content.firstXSpin.valueProperty().getValue().doubleValue();
        double fy = content.firstYSpin.valueProperty().getValue().doubleValue();
        double lx = content.lastXSpin.valueProperty().getValue().doubleValue();
        double sx = content.stepXSpin.valueProperty().getValue().doubleValue();
        double r = content.radiusSpin.valueProperty().getValue().doubleValue();
        List<BumperModel> result = new ArrayList<>();
        for (double x = fx; x <= lx; x += sx) {
            BumperModel bm = new BumperModel();
            bm.setRadius(r);
            bm.setCenterX(x);
            bm.setCenterY(fy);
            result.add(bm);
        }
        return result;
    }

    private List<BumperModel> generateByY() {
        double fx = content.firstXSpin.valueProperty().getValue().doubleValue();
        double fy = content.firstYSpin.valueProperty().getValue().doubleValue();
        double ly = content.lastYSpin.valueProperty().getValue().doubleValue();
        double sy = content.stepYSpin.valueProperty().getValue().doubleValue();
        double r = content.radiusSpin.valueProperty().getValue().doubleValue();
        List<BumperModel> result = new ArrayList<>();
        for (double y = fy; y <= ly; y += sy) {
            BumperModel bm = new BumperModel();
            bm.setRadius(r);
            bm.setCenterX(fx);
            bm.setCenterY(y);
            result.add(bm);
        }
        return result;
    }


    public static class Content extends GridPane {
        public DoubleSpinner firstXSpin;
        public DoubleSpinner lastXSpin;
        public DoubleSpinner stepXSpin;
        public DoubleSpinner firstYSpin;
        public DoubleSpinner lastYSpin;
        public DoubleSpinner stepYSpin;
        public DoubleSpinner radiusSpin;
        public ReadOnlyBooleanWrapper valid = new ReadOnlyBooleanWrapper();

        public Content() {
            FXMLLoader fxmlLoader = new FXMLLoader(BumpersGenerateDialog.class.getResource("BumpersGenerateDialog.fxml"));
            fxmlLoader.setRoot(this);
            fxmlLoader.setController(this);

            try {
                fxmlLoader.load();
            } catch (IOException exception) {
                throw new RuntimeException(exception);
            }
            valid.bind(Bindings.createBooleanBinding(() -> {
                        boolean lx = lastXSpin.valueProperty().getValue().doubleValue() == 0;
                        boolean ly = lastYSpin.valueProperty().getValue().doubleValue() == 0;
                        boolean sx = stepXSpin.valueProperty().getValue().doubleValue() == 0;
                        boolean sy = stepYSpin.valueProperty().getValue().doubleValue() == 0;
                        boolean r = radiusSpin.valueProperty().getValue().doubleValue() == 0;
                        return (sx ^ sy) && (sx && lx || sy && ly) && !r;
                    },
                    lastXSpin.valueProperty(),
                    lastYSpin.valueProperty(),
                    stepXSpin.valueProperty(),
                    stepYSpin.valueProperty()));

        }

        public boolean isValid() {
            return valid.get();
        }

        public ReadOnlyBooleanProperty validProperty() {
            return valid.getReadOnlyProperty();
        }
    }
}
