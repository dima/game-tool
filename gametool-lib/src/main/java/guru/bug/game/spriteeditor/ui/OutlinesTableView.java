package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.BumperModel;
import guru.bug.game.spriteeditor.model.LayerModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.converter.DefaultStringConverter;
import javafx.util.converter.DoubleStringConverter;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class OutlinesTableView extends TableView<BumperModel> {
    private final ObjectProperty<LayerModel> layer = new SimpleObjectProperty<>();

    public OutlinesTableView() {
        TableColumn<BumperModel, Double> centerXColumn = new TableColumn<>("X");
        centerXColumn.setCellValueFactory(new PropertyValueFactory<>("centerX"));
        centerXColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));

        TableColumn<BumperModel, Double> centerYColumn = new TableColumn<>("Y");
        centerYColumn.setCellValueFactory(new PropertyValueFactory<>("centerY"));
        centerYColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));

        TableColumn<BumperModel, Double> radiusColumn = new TableColumn<>("Radius");
        radiusColumn.setCellValueFactory(new PropertyValueFactory<>("radius"));
        radiusColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DoubleStringConverter()));

        TableColumn<BumperModel, Boolean> draftColumn = new TableColumn<>("Draft");
        draftColumn.setCellValueFactory(new PropertyValueFactory<>("draft"));
        draftColumn.setCellFactory(CheckBoxTableCell.forTableColumn(draftColumn));

        TableColumn<BumperModel, String> areaNameColumn = new TableColumn<>("Area");
        areaNameColumn.setCellValueFactory(new PropertyValueFactory<>("areaName"));
        areaNameColumn.setCellFactory(TextFieldTableCell.forTableColumn(new DefaultStringConverter()));

        TableColumn<BumperModel, BumperModel> removeColumn = new TableColumn<>("Remove");
        removeColumn.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        removeColumn.setCellFactory(param -> new TableCell<BumperModel, BumperModel>() {
            private final ImageButton button = new ImageButton("guru/bug/game/spriteeditor/ui/cross-48.png", 16);

            @Override
            protected void updateItem(BumperModel bumperModel, boolean empty) {
                super.updateItem(bumperModel, empty);

                if (empty || bumperModel == null) {
                    setGraphic(null);
                    return;
                }

                setGraphic(button);
                button.setOnAction(
                        event -> getLayer().getBumpers().remove(bumperModel)
                );
            }
        });

        //noinspection unchecked
        getColumns().setAll(centerXColumn, centerYColumn, radiusColumn, draftColumn, areaNameColumn, removeColumn);

        itemsProperty().bind(Bindings.select(layer, "bumpers"));

        setEditable(true);
    }

    public LayerModel getLayer() {
        return layer.get();
    }

    public ObjectProperty<LayerModel> layerProperty() {
        return layer;
    }

    public void setLayer(LayerModel layer) {
        this.layer.set(layer);
    }

}
