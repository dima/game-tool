package guru.bug.game.spriteeditor.model;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.function.Supplier;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class ValueRouter<T extends Property<K>, K> {
    private final Map<MeasureModel, T> properties = new WeakHashMap<>();
    private final Supplier<T> propertyFactory;
    private final ObjectProperty<DimensionModel> dimension = new SimpleObjectProperty<>();
    private final ObjectExpression<MeasureModel> active = Bindings.select(dimension, "active");
    private final T defaultValue;
    private final T currentValue;

    public ValueRouter(Supplier<T> propertyFactory) {
        this(propertyFactory, null);
    }

    public ValueRouter(Supplier<T> propertyFactory, K initialValue) {
        this.propertyFactory = propertyFactory;

        currentValue = propertyFactory.get();
        defaultValue = propertyFactory.get();
        defaultValue.setValue(initialValue);

        activate(null);
        active.addListener((o, ov, nv) -> changeActive(ov, nv));
        dimension.addListener((o, ov, nv) -> {
            if (ov != nv || nv == null) {
                properties.clear();
            }
        });
    }

    T ensureSet(MeasureModel measure) {
        return properties.computeIfAbsent(measure, k -> forceOverride(k, defaultValue.getValue()));
    }

    void setValue(MeasureModel measure, K value) {
        forceOverride(measure, value);
    }

    public T valueProperty() {
        return currentValue;
    }

    private void changeActive(MeasureModel oldValue, MeasureModel newValue) {
        deactivate(oldValue);
        activate(newValue);
    }

    private void deactivate(MeasureModel measure) {
        unbind(defaultValue);
        if (measure != null && properties.containsKey(measure)) {
            unbind(ensureSet(measure));
        }
    }

    private void unbind(T value) {
        if (value.isBound()) {
            K v = value.getValue();
            value.unbind();
            value.setValue(v);
        }
    }

    private void activate(MeasureModel measure) {
        if (measure == null) {
            currentValue.setValue(defaultValue.getValue());
            defaultValue.bind(currentValue);
        } else {
            T prop = ensureSet(measure);
            currentValue.setValue(prop.getValue());
            prop.bind(currentValue);
        }
    }

    private T forceOverride(MeasureModel measure, K value) {
        T result = properties.compute(measure, (k, v) -> v == null ? propertyFactory.get() : v);
        if (result.isBound()) {
            currentValue.setValue(value);
        } else {
            result.setValue(value);
            if (measure == active.get()) {
                changeActive(null, measure);
            }
        }
        return result;
    }

    public DimensionModel getDimension() {
        return dimension.get();
    }

    public ObjectProperty<DimensionModel> dimensionProperty() {
        return dimension;
    }

    public void setDimension(DimensionModel dimension) {
        this.dimension.set(dimension);
    }

    public void startEdit(ObjectProperty<K> valueSource, ObjectProperty<DimensionModel> dimensionSource) {
        valueSource.bindBidirectional(currentValue);
        dimensionSource.set(dimension.get());
        dimension.bind(dimensionSource);
    }

    public void stopEdit(ObjectProperty<K> valueSource) {
        valueSource.unbindBidirectional(currentValue);
        dimension.unbind();
    }

    //<editor-fold desc="Externalization">
    void writeExternal(ObjectOutput out) throws IOException {
        out.writeInt(1); // version
        out.writeObject(defaultValue.getValue());
        out.writeObject(dimension.get());
        out.writeInt(properties.size());
        for (Map.Entry<MeasureModel, T> e : properties.entrySet()) {
            out.writeObject(e.getKey());
            out.writeObject(e.getValue().getValue());
        }
    }

    void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
        in.readInt(); // version
        @SuppressWarnings("unchecked")
        K defVal = (K) in.readObject();
        if (defaultValue.isBound()) {
            currentValue.setValue(defVal);
        } else {
            defaultValue.setValue(defVal);
        }
        setDimension((DimensionModel) in.readObject());
        int size = in.readInt();
        for (int i = 0; i < size; i++) {
            MeasureModel mm = (MeasureModel) in.readObject();
            @SuppressWarnings("unchecked")
            K val = (K) in.readObject();
            forceOverride(mm, val);
        }

    }
    //</editor-fold>
}
