package guru.bug.game.spriteeditor.ui;

import guru.bug.game.spriteeditor.model.LayerModel;
import guru.bug.game.spriteeditor.model.ProjectModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class LayerManager extends BorderPane {
    private final ObjectProperty<ProjectModel> project = new SimpleObjectProperty<>();
    public LayerTreeView layerTreeView;
    private ReadOnlyObjectWrapper<LayerModel> selectedLayer = new ReadOnlyObjectWrapper<>();

    public LayerManager() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LayerManager.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        selectedLayer.bind(Bindings.select(layerTreeView, "selectionModel", "selectedItem", "value"));
    }

    public void selectLayer(LayerModel layerModel) {
        if (layerModel == null) {
            layerTreeView.getSelectionModel().clearSelection();
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public void addGroup() {
        LayerModel group = new LayerModel(true);
        group.setName("Group");
        project.get().children().add(group);
    }

    public ProjectModel getProject() {
        return project.get();
    }

    public ObjectProperty<ProjectModel> projectProperty() {
        return project;
    }

    public void setProject(ProjectModel project) {
        this.project.set(project);
    }

    public LayerModel getSelectedLayer() {
        return selectedLayer.get();
    }

    public ReadOnlyObjectProperty<LayerModel> selectedLayerProperty() {
        return selectedLayer.getReadOnlyProperty();
    }
}
