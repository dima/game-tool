package guru.bug.game;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public enum Direction {
    E(0, 1),
    ESE(1, 8),
    SE(1, 4),
    SSE(3, 8),
    S(1, 2),
    SSW(5, 8),
    SW(3, 4),
    WSW(7, 8),
    W(1, 1),
    WNW(9, 8),
    NW(5, 4),
    NNW(11, 8),
    N(3, 2),
    NNE(13, 8),
    NE(7, 4),
    ENE(15, 8);

    private static final double DOUBLE_PI = Math.PI * 2.0d;
    private final double angle;
    private final double cos;
    private final double sin;

    Direction(double mul, double div) {
        angle = mul * Math.PI / div;
        cos = Math.cos(angle);
        sin = Math.sin(angle);
    }

    /**
     * Finds closest predefined direction to provided angle.
     *
     * @param rad angle in radians.
     * @return one of constants of {@link Direction} which is closest to provided angle.
     */
    public static Direction findClosest(double rad) {
        rad = rad % DOUBLE_PI;
        if (rad < 0) {
            rad = DOUBLE_PI + rad;
        }
        double diff = Math.abs(rad - DOUBLE_PI);
        Direction result = E;
        for (Direction dir : Direction.values()) {
            double curDiff = Math.abs(rad - dir.angle);
            if (curDiff < diff) {
                result = dir;
                diff = curDiff;
            }
        }
        return result;
    }

    public Direction subtract(Direction o) {
        int length = Direction.values().length;
        int result = (ordinal() - o.ordinal()) % length;
        if (result < 0) {
            result = length + result;
        }
        return Direction.values()[result];
    }

    /**
     * Calculates new direction after bouncing. This <code>{@link Direction}</code> is direction of the obstacle.
     * Passed <code>{@link Direction}</code> is old direction of the moving sprite.
     *
     * @param dir current moving direction of the sprite.
     * @return new direction where sprite should be bounced.
     */
    public Direction bounce(Direction dir) {
        Direction wall = this.subtract(S);
        Direction fall = dir.subtract(wall);
        return wall.subtract(fall);
    }

    public static Direction random() {
        int r = ThreadLocalRandom.current().nextInt(Direction.values().length);
        return Direction.values()[r];
    }

    public static double calcAngle(double x1, double y1, double x2, double y2) {
        double dx = x2 - x1;
        double dy = y2 - y1;
        double radius = Math.sqrt(dx * dx + dy * dy);
        double cos = dx / radius;
        double sin = dy / radius;
        double angleX = Math.acos(cos);
        double angleY = Math.asin(sin);
        if (angleY < 0) {
            return DOUBLE_PI - angleX;
        } else {
            return angleX;
        }
    }

    public double getAngle() {
        return angle;
    }

    public double getCos() {
        return cos;
    }

    public double getSin() {
        return sin;
    }

    public static double shortRotation(double startAngle, double targetAngle) {
        if (startAngle == targetAngle) {
            return startAngle;
        }
        if (targetAngle < startAngle) {
            targetAngle += DOUBLE_PI;
        }
        double dif = targetAngle - startAngle;
        if (dif > Math.PI) {
            dif = -DOUBLE_PI + dif;
        }
        return startAngle + dif;
    }
}
