package guru.bug.game.ai;

import guru.bug.game.*;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class BouncingAI extends AIBase {

    @Override
    protected void init(SpriteStateBuilder<Sprite> builder) {
        AI ai = getGame().ai;
        builder
                .onCollision(ai::bounce)
                .onLoop(ai::followDirection);
    }

    @Override
    protected void setup(Sprite sprite) {
        sprite.setSpeed(5);
        sprite.setDirection(Direction.ENE);
    }

    @Override
    protected void loop(Sprite sprite) {

    }
}
