package guru.bug.game.ai;

import guru.bug.game.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class GravityAI extends AIBase {
    public static final String JUMP = "jump";
    public static final String WALK = "walk";
    private static final String FALL = "fall";
    private static final String START_JUMP_Y = "gravityAI.startJumpY";

    private List<Character> wallSprites = new ArrayList<>(2);
    private List<Character> ladderSprites = new ArrayList<>(2);
    private double fallingSpeed = 7;
    private double jumpHeight = 4;

    public GravityAI wall(Character... ch) {
        if (ch != null && ch.length > 0) {
            List<Character> toAdd = Arrays.asList(ch);
            wallSprites.addAll(toAdd);
        }
        return this;
    }

    public GravityAI ladder(Character... ch) {
        if (ch != null && ch.length > 0) {
            List<Character> toAdd = Arrays.asList(ch);
            ladderSprites.addAll(toAdd);
        }
        return this;
    }


    @Override
    protected void init(SpriteStateBuilder<Sprite> builder) {
        AI ai = getGame().ai;
        builder
                .onInit(m -> m.activate(WALK))
                .onCollision(ai::stopX, wallSprites)
                .defineState(WALK)
                .onLoop(this::walkLoop)
                .defineState(FALL)
                .onLoop(this::walkLoop)
                .defineState(JUMP)
                .onActivation(m -> {
                    m.deactivate(WALK);
                    m.data().putDouble(START_JUMP_Y, m.getY());
                })
                .onLoop(this::moveUp)
                .onCollision((m, c) -> m.switchActive(JUMP, WALK), wallSprites);
    }

    private void walkLoop(Sprite sprite) {
        if (fallDown(sprite)) {
            sprite.switchActive(WALK, FALL);
        } else {
            sprite.switchActive(FALL, WALK);
        }
    }


    private boolean fallDown(Sprite sprite) {
        double y = sprite.getY();
        CollisionSet cs = getGame().findCollisions(sprite);
        if (ladderSprites.isEmpty() || !cs.subsetBySpriteAndSymbols(sprite, null, ladderSprites).hasCollisions()) {
            moveDown(sprite);
        }
        cs = getGame().findCollisions(sprite);
        CollisionSet wcs = cs.subsetBySpriteAndSymbols(sprite, null, wallSprites);
        boolean result = true;
        if (wcs.hasCollisions()) {
            AI.Result collisionStat = AI.collisionStat(wcs);
            sprite.setY(sprite.getY() + collisionStat.getMaxYLen());
            result = false;
        } else if (!ladderSprites.isEmpty()) {
            CollisionSet lcs = cs.subsetBySpriteAndSymbols(sprite, null, ladderSprites);
            if (lcs.hasCollisions()) {
                sprite.setY(y);
                result = false;
            }
        }
        return result;
    }

    private void moveUp(Sprite sprite) {
        double fdur = GameEngine.getFrameDuration();
        double targetY = sprite.data().getDouble(START_JUMP_Y) - jumpHeight;
        double delta = Math.max(fallingSpeed * fdur, Math.min(0.25, (sprite.getY() - targetY) / 15.0));
        double y = sprite.getY() - delta;
        sprite.setY(y);
        if (y <= targetY) {
            sprite.switchActive(JUMP, WALK);
        }
    }

    private void moveDown(Sprite sprite) {
        double fdur = GameEngine.getFrameDuration();
        double y = sprite.getY() + fallingSpeed * fdur;
        sprite.setY(y);
    }

    public GravityAI fallingSpeed(double fallingSpeed) {
        setFallingSpeed(fallingSpeed);
        return this;
    }

    public double getFallingSpeed() {
        return fallingSpeed;
    }

    public void setFallingSpeed(double fallingSpeed) {
        this.fallingSpeed = fallingSpeed;
    }

    public GravityAI jumpHeight(double jumpHeight) {
        setJumpHeight(jumpHeight);
        return this;
    }

    public double getJumpHeight() {
        return jumpHeight;
    }

    public void setJumpHeight(double jumpHeight) {
        this.jumpHeight = jumpHeight;
    }

    @Override
    protected void setup(Sprite sprite) {

    }

    @Override
    protected void loop(Sprite sprite) {

    }
}
