package guru.bug.game.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class Pool<T> {
    private final Supplier<T> factory;
    private final List<T> elements = new ArrayList<>(256);
    private int nextIndex;

    public void reset() {
        nextIndex = 0;
    }

    public T get() {
        while (nextIndex >= elements.size()) {
            elements.add(factory.get());
        }
        T result = elements.get(nextIndex);
        nextIndex++;
        return result;
    }

    public Pool(Supplier<T> factory) {
        this.factory = factory;
    }

}
