package guru.bug.game.utils;

import guru.bug.game.Game;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.jar.JarFile;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class ClassScanner {
    private static final Logger logger = Logger.getLogger(ClassScanner.class.getName());
    private static final Pattern SKIP_JAR = Pattern.compile(".*((/jre/lib/.*\\.jar!)" +
            "|(/lib/idea_rt\\.jar!)" +
            "|(/lib/packager\\.jar!)" +
            "|(/lib/tools\\.jar!)" +
            "|(/lib/dt\\.jar!)" +
            "|(/lib/sa-jdi\\.jar!)" +
            "|(/lib/jconsole\\.jar!)" +
            "|(/lib/javafx-mx\\.jar!)" +
            "|(/lib/ant-javafx\\.jar!)" +
            "|(MRJToolkit\\.jar!)).*");
    private static final Pattern PASS_CLASS = Pattern.compile("[^$]+\\.class");


    private static Set<String> scan() {
        try {
            Set<String> result = new HashSet<>(50);
            logger.finer("scanning folders");
            scanFolders(result);
            logger.finer("scanning jars");
            scanByManifest(result);
            return result;
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException(e);
        }

    }

    private static void scanFolders(Set<String> result) throws IOException, URISyntaxException {
        Enumeration<URL> urls = ClassScanner.class.getClassLoader().getResources("");
        while (urls.hasMoreElements()) {
            URL url = urls.nextElement();
            visitFolder(url, result);
        }
    }

    private static void visitFolder(URL url, Set<String> result) throws URISyntaxException, IOException {
        Path root = Paths.get(url.toURI());
        Files.walk(root)
                .filter(p -> PASS_CLASS.matcher(p.toString()).matches())
                .map(root::relativize)
                .map(Path::toString)
                .map(s -> s.substring(0, s.length() - 6))
                .map(s -> s.replace(File.separatorChar, '.'))
                .forEach(result::add);
    }

    private static void scanByManifest(Set<String> result) throws IOException {
        Enumeration<URL> urls = ClassScanner.class.getClassLoader().getResources("META-INF/MANIFEST.MF");
        while (urls.hasMoreElements()) {
            URL u = urls.nextElement();
            if (SKIP_JAR.matcher(u.getPath()).matches() || !u.getProtocol().equals("jar")) {
                logger.finer(() -> "Skipping " + u);
                continue;
            }
            System.out.println(u);
            processJarFile(u, result);
        }
    }

    private static void processJarFile(URL url, Set<String> result) throws IOException {
        String u = url.toString();
        int start = 9;
        if (u.startsWith("jar:file://")) {
            start = 10;
        }
        u = u.substring(start, u.indexOf('!'));
        u = URLDecoder.decode(u, StandardCharsets.UTF_8);
        JarFile jar = new JarFile(u);
        jar.stream()
                .filter(e -> !e.isDirectory())
                .map(ZipEntry::getName)
                .filter(s -> PASS_CLASS.matcher(s).matches())
                .map(s -> s.substring(0, s.length() - 6))
                .map(s -> s.replace(File.separatorChar, '.'))
                .forEach(result::add);
    }

    public static Game findGameImplementation() {
        Optional<Class> resultClass = scan().stream()
                .map(ClassScanner::convertToClass)
                .filter(Objects::nonNull)
                .filter(c -> c != Game.class)
                .filter(Game.class::isAssignableFrom)
                .map(c -> c.asSubclass(Game.class))
                .findFirst();
        return resultClass
                .map(ClassScanner::createInstanceOf)
                .orElse(null);
    }

    private static Game createInstanceOf(Class aClass) {
        try {
            return (Game) aClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }
    }

    private static Class convertToClass(String s) {
        if (s.startsWith("com.sun.")
                || s.startsWith("oracle.")
                || s.startsWith("sun.")
                || s.startsWith("javax.")
                || s.startsWith("com.oracle.")
                || s.startsWith("com.apple.")
                || s.startsWith("org.relaxng.")
                || s.startsWith("jdk.")) {
            return null;
        }
        try {
            return Class.forName(s);
        } catch (Throwable e) {
            return null;
        }
    }
}
