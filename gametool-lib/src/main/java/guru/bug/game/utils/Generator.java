package guru.bug.game.utils;

import guru.bug.game.Bumper;
import guru.bug.game.SpritePainter;
import guru.bug.game.spriteeditor.model.*;
import guru.bug.game.spriteeditor.ui.LayerView;
import javafx.geometry.Bounds;
import javafx.geometry.Point2D;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class Generator {
    private static final double GRID_SIZE = 32;
    private final ProjectModel project;
    private final LayerView layerView;
    private final SpritePainter.Builder builder;
    private final int[] dimSizes;

    public Generator(ProjectModel project) {
        this.project = project;
        this.layerView = new LayerView(project);
        this.dimSizes = new int[project.getDimensions().size()];
        this.builder = SpritePainter.builder();
        for (int i = 0; i < dimSizes.length; i++) {
            DimensionModel dm = project.getDimensions().get(i);
            int dimSize = dm.getMeasures().size();
            this.dimSizes[i] = dimSize;
            this.builder.dimensions(dimSize);
        }
    }

    public void generate() throws IOException {
        List<DimensionModel> dimensions = project.getDimensions();
        Deque<Iterator<MeasureModel>> stack = new LinkedList<>();
        do {
            while (stack.size() < dimensions.size()) {
                DimensionModel dm = dimensions.get(stack.size());
                List<MeasureModel> measures = dm.getMeasures();
                Iterator<MeasureModel> im = measures.iterator();
                stack.push(im);
                MeasureModel mm = im.next();
                dm.setActive(mm);
            }

            generateFrame();

            while (!stack.isEmpty() && !stack.peek().hasNext()) {
                stack.pop();
            }

            if (!stack.isEmpty()) {
                MeasureModel mm = stack.peek().next();
                DimensionModel dm = dimensions.get(stack.size() - 1);
                dm.setActive(mm);
            }
        } while (!stack.isEmpty());
        SpritePainter result = builder.build();
        File projectFile = project.getLinkedWith();
        File dir = projectFile.getParentFile();
        File genFile = new File(dir, projectFile.getName() + ".sprite");
        result.write(genFile);
    }

    private void generateFrame() {
        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);
        WritableImage image = layerView.snapshot(parameters, null);
        double x = project.getRotatePivotXRouter().valueProperty().get();
        double y = project.getRotatePivotYRouter().valueProperty().get();
        Point2D center = layerView.getLocalToParentTransform().transform(x, y);
        Bounds bounds = layerView.getBoundsInParent();
        builder.frameOffsetX((center.getX() - bounds.getMinX()) / GRID_SIZE);
        builder.frameOffsetY((center.getY() - bounds.getMinY()) / GRID_SIZE);
        builder.frameWidth(bounds.getWidth() / GRID_SIZE);
        builder.frameHeight(bounds.getHeight() / GRID_SIZE);
        builder.frameImage(image);

        List<Bumper> draftBumpers = new ArrayList<>(20);
        List<Bumper> preciseBumpers = new ArrayList<>(20);
        findAllBumpers(draftBumpers, preciseBumpers, layerView);

        builder.draftBumper(true);
        buildBumpers(center, draftBumpers);
        builder.draftBumper(false);
        buildBumpers(center, preciseBumpers);

        builder.nextFrame();
    }

    private void buildBumpers(Point2D center, List<Bumper> bumpers) {
        for (Bumper b : bumpers) {
            builder.bumperRadius(b.getRadius() / GRID_SIZE);
            builder.bumperCenterOffsetX((center.getX() - b.getCenterX()) / GRID_SIZE);
            builder.bumperCenterOffsetY((center.getY() - b.getCenterY()) / GRID_SIZE);
            builder.bumperAreaName(b.getAreaName());
            builder.nextBumper();
        }
    }

    private void findAllBumpers(List<Bumper> draftBumpers, List<Bumper> preciseBumpers, LayerView layerView) {
        LayerModel lm = layerView.getLayer();
        if (lm.isGroup()) {
            layerView.getChildren().stream()
                    .filter(n -> n instanceof LayerView)
                    .map(n -> (LayerView) n)
                    .forEach(l -> findAllBumpers(draftBumpers, preciseBumpers, l));
        }
        draftBumpers.forEach(b -> updateToParent(b, layerView));
        preciseBumpers.forEach(b -> updateToParent(b, layerView));
        for (BumperModel bm : lm.getBumpers()) {
            Bumper b = new Bumper(bm.getCenterX(), bm.getCenterY(), bm.getRadius(), bm.getAreaName());
            updateToParent(b, layerView);
            if (bm.isDraft()) {
                draftBumpers.add(b);
            } else {
                preciseBumpers.add(b);
            }
        }
    }

    private void updateToParent(Bumper bumper, LayerView layerView) {
        Point2D center = layerView.localToParent(bumper.getCenterX(), bumper.getCenterY());
        bumper.setCenterX(center.getX());
        bumper.setCenterY(center.getY());
    }

}
