package guru.bug.game.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class TimeMetrics {
    private static final int MAX_SIZE = 300;
    private final List<Long> history = new ArrayList<>(MAX_SIZE);
    private int pointer = 0;
    private double sum = 0;
    private double average;

    public void add(long value) {
        sum += value;
        if (history.size() == MAX_SIZE) {
            sum -= history.get(pointer);
            history.set(pointer, value);
            pointer++;
            if (pointer == MAX_SIZE) {
                pointer = 0;
            }
        } else {
            history.add(value);
        }
        update();
    }

    private void update() {
        average = sum / (double) history.size();
    }

    public double getAverage() {
        return average;
    }
}
