package guru.bug.game.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class ProgressInputStreamWrapper extends InputStream {
    private final InputStream wrapped;
    private final Consumer<Long> progress;
    private long readSoFar;


    public ProgressInputStreamWrapper(InputStream wrapped, Consumer<Long> progress) {
        this.wrapped = wrapped;
        this.progress = progress;
    }

    @Override
    public int read() throws IOException {
        int result = wrapped.read();
        progress(result == -1 ? -1 : 1);
        return result;
    }

    private int progress(int inc) {
        return Math.toIntExact(progress((long) inc));
    }

    private long progress(long inc) {
        if (inc > 0) {
            readSoFar += inc;
            progress.accept(readSoFar);
        }
        return inc;
    }

    @Override
    public int read(byte[] b) throws IOException {
        return progress(wrapped.read(b));
    }

    @Override
    public int read(byte[] b, int off, int len) throws IOException {
        return progress(wrapped.read(b, off, len));
    }

    @Override
    public long skip(long n) throws IOException {
        return progress(wrapped.skip(n));
    }

    @Override
    public int available() throws IOException {
        return wrapped.available();
    }

    @Override
    public void close() throws IOException {
        wrapped.close();
    }

    @Override
    public synchronized void mark(int readlimit) {
    }

    @Override
    public synchronized void reset() throws IOException {
        throw new IOException("mark is not supported");
    }

    @Override
    public boolean markSupported() {
        return false;
    }
}
