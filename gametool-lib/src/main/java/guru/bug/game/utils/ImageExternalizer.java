package guru.bug.game.utils;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public final class ImageExternalizer {
    private static final int DEFAULT_BUFFER_SIZE = 2048;
    private static final ThreadLocal<ByteArrayOutputStream> tmpOutputStream =
            ThreadLocal.withInitial(() -> new ByteArrayOutputStream(DEFAULT_BUFFER_SIZE));

    private ImageExternalizer() {
    }

    public static void writeImage(ObjectOutput out, Image image) throws IOException {
        if (image == null) {
            out.writeInt(0);
        } else {
            BufferedImage bufImg = SwingFXUtils.fromFXImage(image, null);
            ByteArrayOutputStream tmp = tmpOutputStream.get();
            tmp.reset();
            ImageIO.write(bufImg, "PNG", tmp);
            out.writeInt(tmp.size());
            out.write(tmp.toByteArray());
        }
    }

    public static Image readImage(ObjectInput in) throws IOException {
        int size = in.readInt();
        if (size == 0) {
            return null;
        }
        byte[] buf = new byte[size];
        int read = 0;
        int ofs = 0;
        while (read < size) {
            read += in.read(buf, ofs, size - ofs);
            ofs = read;
        }
        ByteArrayInputStream tmp = new ByteArrayInputStream(buf);
        BufferedImage img = ImageIO.read(tmp);
        return SwingFXUtils.toFXImage(img, null);
    }
}
