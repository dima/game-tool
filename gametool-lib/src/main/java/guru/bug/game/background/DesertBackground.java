package guru.bug.game.background;

import guru.bug.game.Background;
import guru.bug.game.BackgroundItem;
import javafx.animation.*;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.util.Duration;

import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class DesertBackground extends Background {
    private static final Image BACKGROUND_3 = new Image("guru/bug/game/background/desert/background.png");
    private static final Image BACKGROUND_2 = new Image("guru/bug/game/background/desert/layer_02.png");
    private static final Image BACKGROUND_1 = new Image("guru/bug/game/background/desert/layer_01.png");
    private static final Image SUN = new Image("guru/bug/game/background/desert/sun.png");
    private static final Image PHARAOH_0 = new Image("guru/bug/game/background/desert/ph_00.png");
    private static final Image PHARAOH_1 = new Image("guru/bug/game/background/desert/ph_01.png");
    private static final Image PHARAOH_2 = new Image("guru/bug/game/background/desert/ph_02.png");
    private static final Image PHARAOH_3 = new Image("guru/bug/game/background/desert/ph_03.png");
    private static final Image CLOUD_1 = new Image("guru/bug/game/background/desert/cloud_01.png");
    private static final Image CLOUD_2 = new Image("guru/bug/game/background/desert/cloud_02.png");
    private static final Image CLOUD_3 = new Image("guru/bug/game/background/desert/cloud_03.png");
    private static final int PHARAOH_X = 330;
    private static final int PHARAOH_Y = 268;

    private final BackgroundItem background3 = new BackgroundItem(BACKGROUND_3, 0, 0);
    private final BackgroundItem background2 = new BackgroundItem(BACKGROUND_2, 0, 170);
    private final BackgroundItem background1 = new BackgroundItem(BACKGROUND_1, 0, 322);
    private final BackgroundItem sun = new BackgroundItem(SUN, 130, 182);
    private final BackgroundItem cloud1 = new BackgroundItem(CLOUD_1, 0, 180);
    private final BackgroundItem cloud2 = new BackgroundItem(CLOUD_2, 15, 62);
    private final BackgroundItem cloud3 = new BackgroundItem(CLOUD_3, 224, 24);
    private final ObservableList<BackgroundItem> pharaohFrames = FXCollections.observableArrayList(
            new BackgroundItem(PHARAOH_0, PHARAOH_X, PHARAOH_Y),
            new BackgroundItem(PHARAOH_1, PHARAOH_X, PHARAOH_Y),
            new BackgroundItem(PHARAOH_2, PHARAOH_X, PHARAOH_Y),
            new BackgroundItem(PHARAOH_3, PHARAOH_X, PHARAOH_Y));
    private final IntegerProperty pharaohFrameIndex = new SimpleIntegerProperty();
    private final ObjectExpression<BackgroundItem> pharaoh = Bindings.valueAt(pharaohFrames, pharaohFrameIndex);

    private final Timeline sunAnimation = createSunAnimation();
    private final Timeline pharaohAnimation = createPharaohAnimation();
    private final Timeline cloud1Animation = createCloud1Animation();
    private final Timeline cloud2Animation = createCloud2Animation();
    private final Timeline cloud3Animation = createCloud3Animation();

    private Timeline createPharaohAnimation() {
        Timeline wave = new Timeline(new KeyFrame(Duration.seconds(0.3),
                e -> pharaohFrameIndex.set(1),
                new KeyValue(pharaohFrameIndex, pharaohFrames.size() - 1)));
        Timeline blink = new Timeline(new KeyFrame(Duration.seconds(0.5),
                e -> pharaohFrameIndex.set(1),
                new KeyValue(pharaohFrameIndex, 0)));
        wave.setCycleCount(4);
        wave.setAutoReverse(true);
        blink.setCycleCount(1);
        blink.setAutoReverse(true);
        Timeline result = new Timeline(
                new KeyFrame(Duration.seconds(5), e -> blink.playFromStart()),
                new KeyFrame(Duration.seconds(10), e -> wave.playFromStart()));
        result.setCycleCount(Animation.INDEFINITE);
        return result;
    }

    private Timeline createCloud1Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(12), new KeyValue(cloud1.xProperty(), 10));
        result.getKeyFrames().addAll(f1);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createCloud2Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(28), new KeyValue(cloud2.xProperty(), 30));
        KeyFrame f2 = new KeyFrame(Duration.seconds(28), new KeyValue(cloud2.yProperty(), 30));
        result.getKeyFrames().addAll(f1, f2);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createSunAnimation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(28), new KeyValue(sun.xProperty(), 175, Interpolator.EASE_BOTH));
        KeyFrame f2 = new KeyFrame(Duration.seconds(28), new KeyValue(sun.yProperty(), 10, Interpolator.EASE_BOTH));
        result.getKeyFrames().addAll(f1, f2);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createCloud3Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(70), new KeyValue(cloud3.xProperty(), 150));
        result.getKeyFrames().addAll(f1);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    public DesertBackground() {
        setWidth(BACKGROUND_3.getWidth());
        setHeight(BACKGROUND_3.getHeight());
    }

    @Override
    protected void startAnimation() {
        pharaohAnimation.playFromStart();
        cloud1Animation.playFromStart();
        cloud2Animation.playFromStart();
        cloud3Animation.playFromStart();
        sunAnimation.playFromStart();
    }

    @Override
    protected void stopAnimation() {
        pharaohAnimation.stop();
        cloud1Animation.stop();
        cloud2Animation.stop();
        cloud3Animation.stop();
        sunAnimation.stop();
    }

    @Override
    protected void drawing(Consumer<BackgroundItem> c) {
        c.accept(background3);
        c.accept(sun);
        c.accept(cloud1);
        c.accept(cloud2);
        c.accept(cloud3);
        c.accept(background2);
        c.accept(background1);
        c.accept(pharaoh.get());
    }
}
