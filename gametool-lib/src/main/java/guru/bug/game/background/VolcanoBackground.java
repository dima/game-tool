package guru.bug.game.background;

import guru.bug.game.Background;
import guru.bug.game.BackgroundItem;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.util.Duration;

import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class VolcanoBackground extends Background {
    private static final Image BACKGROUND_4 = new Image("guru/bug/game/background/volcano/b04.png");
    private static final Image BACKGROUND_3 = new Image("guru/bug/game/background/volcano/b03.png");
    private static final Image BACKGROUND_2 = new Image("guru/bug/game/background/volcano/b02.png");
    private static final Image BACKGROUND_1 = new Image("guru/bug/game/background/volcano/b01.png");
    private static final Image SMOKE_1 = new Image("guru/bug/game/background/volcano/smoke_1.png");
    private static final Image SMOKE_2 = new Image("guru/bug/game/background/volcano/smoke_2.png");
    private static final Image SMOKE_3 = new Image("guru/bug/game/background/volcano/smoke_3.png");
    private static final Image SNOWMAN_0 = new Image("guru/bug/game/background/volcano/s00.png");
    private static final Image SNOWMAN_1 = new Image("guru/bug/game/background/volcano/s01.png");
    private static final Image SNOWMAN_2 = new Image("guru/bug/game/background/volcano/s02.png");
    private static final Image SNOWMAN_3 = new Image("guru/bug/game/background/volcano/s03.png");
    private static final Image CLOUD_1 = new Image("guru/bug/game/background/volcano/b05-1.png");
    private static final Image CLOUD_2 = new Image("guru/bug/game/background/volcano/b05-2.png");
    private static final Image CLOUD_3 = new Image("guru/bug/game/background/volcano/b05-3.png");
    private static final Image CLOUD_4 = new Image("guru/bug/game/background/volcano/b05-4.png");
    private static final Image CLOUD_5 = new Image("guru/bug/game/background/volcano/b05-5.png");
    private static final int SMOKE_X = 185;
    private static final int SMOKE_Y = 145;
    private static final int SNOWMAN_X = 357;
    private static final int SNOWMAN_Y = 383;

    private final BackgroundItem background4 = new BackgroundItem(BACKGROUND_4, 0, 0);
    private final BackgroundItem background3 = new BackgroundItem(BACKGROUND_3, 0, BACKGROUND_4.getHeight() - BACKGROUND_3.getHeight());
    private final BackgroundItem background2 = new BackgroundItem(BACKGROUND_2, 0, BACKGROUND_4.getHeight() - BACKGROUND_2.getHeight());
    private final BackgroundItem background1 = new BackgroundItem(BACKGROUND_1, 0, BACKGROUND_4.getHeight() - BACKGROUND_1.getHeight());

    private final ObservableList<BackgroundItem> smokeFrames = FXCollections.observableArrayList(
            new BackgroundItem(SMOKE_1, SMOKE_X, SMOKE_Y),
            new BackgroundItem(SMOKE_2, SMOKE_X, SMOKE_Y),
            new BackgroundItem(SMOKE_3, SMOKE_X, SMOKE_Y));
    private final ObservableList<BackgroundItem> snowmanFrames = FXCollections.observableArrayList(
            new BackgroundItem(SNOWMAN_0, SNOWMAN_X, SNOWMAN_Y),
            new BackgroundItem(SNOWMAN_1, SNOWMAN_X, SNOWMAN_Y),
            new BackgroundItem(SNOWMAN_2, SNOWMAN_X, SNOWMAN_Y),
            new BackgroundItem(SNOWMAN_3, SNOWMAN_X, SNOWMAN_Y));
    private final IntegerProperty smokeFrameIndex = new SimpleIntegerProperty();
    private final ObjectExpression<BackgroundItem> smoke = Bindings.valueAt(smokeFrames, smokeFrameIndex);
    private final IntegerProperty snowmanFrameIndex = new SimpleIntegerProperty();
    private final ObjectExpression<BackgroundItem> snowman = Bindings.valueAt(snowmanFrames, snowmanFrameIndex);
    private final BackgroundItem cloud1 = new BackgroundItem(CLOUD_1, 0, 275);
    private final BackgroundItem cloud2 = new BackgroundItem(CLOUD_2, 85, 180);
    private final BackgroundItem cloud3 = new BackgroundItem(CLOUD_3, 640, 200);
    private final BackgroundItem cloud4 = new BackgroundItem(CLOUD_4, 500, 80);
    private final BackgroundItem cloud5 = new BackgroundItem(CLOUD_5, 120, 120);
    private final Timeline smokeAnimation = createSmokeAnimation();
    private final Timeline snowmanAnimation = createSnowmanAnimation();
    private final Timeline cloud1Animation = createCloud1Animation();
    private final Timeline cloud2Animation = createCloud2Animation();
    private final Timeline cloud3Animation = createCloud3Animation();
    private final Timeline cloud4Animation = createCloud4Animation();
    private final Timeline cloud5Animation = createCloud5Animation();

    private Timeline createSmokeAnimation() {
        Timeline result = new Timeline(new KeyFrame(Duration.seconds(2),
                new KeyValue(smokeFrameIndex, smokeFrames.size() - 1)));
        result.setCycleCount(Animation.INDEFINITE);
        return result;
    }

    private Timeline createSnowmanAnimation() {
        Timeline wave = new Timeline(new KeyFrame(Duration.seconds(0.3),
                e -> snowmanFrameIndex.set(1),
                new KeyValue(snowmanFrameIndex, snowmanFrames.size() - 1)));
        Timeline blink = new Timeline(new KeyFrame(Duration.seconds(0.5),
                e -> snowmanFrameIndex.set(1),
                new KeyValue(snowmanFrameIndex, 0)));
        wave.setCycleCount(3);
        wave.setAutoReverse(true);
        blink.setCycleCount(1);
        blink.setAutoReverse(true);
        Timeline result = new Timeline(
                new KeyFrame(Duration.seconds(5), e -> blink.playFromStart()),
                new KeyFrame(Duration.seconds(10), e -> wave.playFromStart()));
        result.setCycleCount(Animation.INDEFINITE);
        return result;
    }

    private Timeline createCloud1Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(12), new KeyValue(cloud1.xProperty(), 10));
        KeyFrame f2 = new KeyFrame(Duration.seconds(18), new KeyValue(cloud1.yProperty(), 285));
        result.getKeyFrames().addAll(f1, f2);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createCloud2Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(28), new KeyValue(cloud2.xProperty(), 150));
        result.getKeyFrames().addAll(f1);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createCloud3Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(70), new KeyValue(cloud3.xProperty(), 200));
        result.getKeyFrames().addAll(f1);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createCloud4Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(25), new KeyValue(cloud4.xProperty(), 450));
        KeyFrame f2 = new KeyFrame(Duration.seconds(35), new KeyValue(cloud4.yProperty(), 100));
        result.getKeyFrames().addAll(f1, f2);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createCloud5Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(30), new KeyValue(cloud5.xProperty(), 180));
        result.getKeyFrames().addAll(f1);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    public VolcanoBackground() {
        setWidth(BACKGROUND_4.getWidth());
        setHeight(BACKGROUND_4.getHeight());
    }

    @Override
    protected void startAnimation() {
        smokeAnimation.playFromStart();
        snowmanAnimation.playFromStart();
        cloud1Animation.playFromStart();
        cloud2Animation.playFromStart();
        cloud3Animation.playFromStart();
        cloud4Animation.playFromStart();
        cloud5Animation.playFromStart();
    }

    @Override
    protected void stopAnimation() {
        smokeAnimation.stop();
        snowmanAnimation.stop();
        cloud1Animation.stop();
        cloud2Animation.stop();
        cloud3Animation.stop();
        cloud4Animation.stop();
        cloud5Animation.stop();
    }

    @Override
    protected void drawing(Consumer<BackgroundItem> c) {
        c.accept(background4);
        c.accept(cloud1);
        c.accept(background3);
        c.accept(cloud4);
        c.accept(cloud2);
        c.accept(cloud3);
        c.accept(cloud5);
        c.accept(background2);
        c.accept(background1);
        c.accept(smoke.get());
        c.accept(snowman.get());
    }
}
