package guru.bug.game.background;

import guru.bug.game.Background;
import guru.bug.game.BackgroundItem;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.function.Consumer;

/**
 * Created by dima on 16.10.10.
 */
public class FillBackground extends Background {

    @Override
    protected void startAnimation() {

    }

    @Override
    protected void stopAnimation() {

    }

    @Override
    protected void drawing(Consumer<BackgroundItem> consumer) {

    }

    @Override
    protected void draw(GraphicsContext gc) {
        double w = getFitWidth();
        double h = getFitHeight();
        gc.save();
        gc.setFill(Color.BLACK);
        gc.fillRect(0, 0, w, h);
        gc.restore();
    }
}
