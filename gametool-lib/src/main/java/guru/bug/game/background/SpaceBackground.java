package guru.bug.game.background;

import guru.bug.game.Background;
import guru.bug.game.BackgroundItem;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import javafx.util.Duration;

import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class SpaceBackground extends Background {
    private static final Image BACKGROUND_4 = new Image("guru/bug/game/background/space/background.png");
    private static final Image BACKGROUND_3 = new Image("guru/bug/game/background/space/layer_03.png");
    private static final Image BACKGROUND_2A = new Image("guru/bug/game/background/space/layer_02a.png");
    private static final Image BACKGROUND_2B = new Image("guru/bug/game/background/space/layer_02b.png");
    private static final Image BACKGROUND_1 = new Image("guru/bug/game/background/space/layer_01.png");
    private static final Image PLANET = new Image("guru/bug/game/background/space/planet.png");
    private static final Image CLOUD_1 = new Image("guru/bug/game/background/space/cloud_01.png");
    private static final Image CLOUD_2 = new Image("guru/bug/game/background/space/cloud_02.png");
    private static final Image CLOUD_3 = new Image("guru/bug/game/background/space/cloud_03.png");
    private static final Image ALIEN_0 = new Image("guru/bug/game/background/space/g00.png");
    private static final Image ALIEN_1 = new Image("guru/bug/game/background/space/g01.png");
    private static final Image ALIEN_2 = new Image("guru/bug/game/background/space/g02.png");
    private static final Image ALIEN_3 = new Image("guru/bug/game/background/space/g03.png");
    private static final double ALIEN_X = 35;
    private static final double ALIEN_Y = 390;

    private final BackgroundItem background4 = new BackgroundItem(BACKGROUND_4, 0, 0);
    private final BackgroundItem background3 = new BackgroundItem(BACKGROUND_3, 0, 197);
    private final BackgroundItem background2a = new BackgroundItem(BACKGROUND_2A, 75, 361);
    private final BackgroundItem background2b = new BackgroundItem(BACKGROUND_2B, 363, 169);
    private final BackgroundItem background1 = new BackgroundItem(BACKGROUND_1, 0, 396);
    private final BackgroundItem planet = new BackgroundItem(PLANET, 42, 95);
    private final BackgroundItem cloud1 = new BackgroundItem(CLOUD_1, 202, 24);
    private final BackgroundItem cloud2 = new BackgroundItem(CLOUD_2, 402, 121);
    private final BackgroundItem cloud3 = new BackgroundItem(CLOUD_3, 37, 223);
    private final ObservableList<BackgroundItem> alienFrames = FXCollections.observableArrayList(
            new BackgroundItem(ALIEN_0, ALIEN_X, ALIEN_Y),
            new BackgroundItem(ALIEN_1, ALIEN_X, ALIEN_Y),
            new BackgroundItem(ALIEN_2, ALIEN_X, ALIEN_Y),
            new BackgroundItem(ALIEN_3, ALIEN_X, ALIEN_Y));
    private final IntegerProperty alienFrameIndex = new SimpleIntegerProperty();
    private final ObjectExpression<BackgroundItem> alien = Bindings.valueAt(alienFrames, alienFrameIndex);

    private final Timeline cloud1Animation = createCloud1Animation();
    private final Timeline cloud2Animation = createCloud2Animation();
    private final Timeline cloud3Animation = createCloud3Animation();
    private final Timeline planetAnimation = createPlanetAnimation();
    private final Timeline alienAnimation = createAlienAnimation();

    private Timeline createCloud1Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(25), new KeyValue(cloud1.xProperty(), 150));
        KeyFrame f2 = new KeyFrame(Duration.seconds(25), new KeyValue(cloud1.yProperty(), 50));
        result.getKeyFrames().addAll(f1, f2);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createCloud2Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(35), new KeyValue(cloud2.xProperty(), 320));
        result.getKeyFrames().addAll(f1);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createCloud3Animation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(70), new KeyValue(cloud3.xProperty(), 185));
        result.getKeyFrames().addAll(f1);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createPlanetAnimation() {
        Timeline result = new Timeline();
        KeyFrame f1 = new KeyFrame(Duration.seconds(40), new KeyValue(planet.xProperty(), 15));
        KeyFrame f2 = new KeyFrame(Duration.seconds(45), new KeyValue(planet.yProperty(), 70));
        result.getKeyFrames().addAll(f1, f2);
        result.setCycleCount(Animation.INDEFINITE);
        result.setAutoReverse(true);
        return result;
    }

    private Timeline createAlienAnimation() {
        Timeline wave = new Timeline(new KeyFrame(Duration.seconds(0.5),
                e -> alienFrameIndex.set(1),
                new KeyValue(alienFrameIndex, alienFrames.size() - 1)));
        Timeline blink = new Timeline(new KeyFrame(Duration.seconds(0.5),
                e -> alienFrameIndex.set(1),
                new KeyValue(alienFrameIndex, 0)));
        wave.setCycleCount(3);
        wave.setAutoReverse(true);
        blink.setCycleCount(1);
        blink.setAutoReverse(true);
        Timeline result = new Timeline(
                new KeyFrame(Duration.seconds(5), e -> blink.playFromStart()),
                new KeyFrame(Duration.seconds(10), e -> wave.playFromStart()));
        result.setCycleCount(Animation.INDEFINITE);
        return result;
    }

    public SpaceBackground() {
        setWidth(BACKGROUND_4.getWidth());
        setHeight(BACKGROUND_4.getHeight());
    }

    @Override
    protected void startAnimation() {
        cloud1Animation.playFromStart();
        cloud2Animation.playFromStart();
        cloud3Animation.playFromStart();
        planetAnimation.playFromStart();
        alienAnimation.playFromStart();
    }

    @Override
    protected void stopAnimation() {
        cloud1Animation.stop();
        cloud2Animation.stop();
        cloud3Animation.stop();
        planetAnimation.stop();
        alienAnimation.stop();
    }

    @Override
    protected void drawing(Consumer<BackgroundItem> consumer) {
        consumer.accept(background4);
        consumer.accept(planet);
        consumer.accept(background3);
        consumer.accept(background2a);
        consumer.accept(background2b);
        consumer.accept(cloud1);
        consumer.accept(cloud2);
        consumer.accept(cloud3);
        consumer.accept(background1);
        consumer.accept(alien.get());
    }
}
