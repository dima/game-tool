package guru.bug.game;

import java.util.Collection;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.stream.Stream;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public abstract class CollisionSet {

    abstract Stream<Collision> collisionStream();


    public void findBumpers(BiConsumer<Bumper, Bumper> consumer) {
        collisionStream()
                .forEach(c -> consumer.accept(c.party1.bumper, c.party2.bumper));
    }

    public boolean isEmpty() {
        return !hasCollisions();
    }

    public boolean hasCollisions() {
        return collisionStream().findAny().isPresent();
    }

    public CollisionSet subsetBySpriteAndClasses(Sprite party, String area, Collection<Class<? extends Sprite>> counterparty) {
        CollisionSet result = subsetBySprite(party, area);
        if (counterparty != null && !counterparty.isEmpty()) {
            result = new CollisionSetByCounterPartyClasses(result, counterparty);
        }
        return result;
    }

    public CollisionSet subsetBySpriteAndSymbols(Sprite party, String area, Collection<Character> counterparty) {
        CollisionSet result = subsetBySprite(party, area);
        if (counterparty != null && !counterparty.isEmpty()) {
            result = new CollisionSetByCounterPartySymbols(result, counterparty);
        }
        return result;
    }

    public CollisionSet subsetBySprite(Sprite party, String area) {
        return new CollisionSetBySprite(this, party, area);
    }

    static final class Collision {
        private Party party1;
        private Party party2;

        Collision(Party party1, Party party2) {
            this.party1 = party1;
            this.party2 = party2;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Collision other = (Collision) o;
            return Objects.equals(this.party1, other.party1) && Objects.equals(this.party2, other.party2) ||
                    Objects.equals(this.party1, other.party2) && Objects.equals(this.party2, other.party1);
        }

        @Override
        public int hashCode() {
            return party1.hashCode() | party2.hashCode();
        }

        private void swap() {
            Party p = party1;
            party1 = party2;
            party2 = p;
        }

        boolean party(Sprite party) {
            if (party1.sprite == party) {
                return true;
            }
            if (party2.sprite == party) {
                swap();
                return true;
            }
            return false;
        }

        Party getParty() {
            return party1;
        }

        Party getCounterparty() {
            return party2;
        }

        @Override
        public String toString() {
            return String.format("Collision(%s, %s)", party1, party2);
        }
    }

    static final class Party {
        private final Sprite sprite;
        private final Bumper bumper;

        Party(Sprite sprite, Bumper bumper) {
            this.sprite = sprite;
            this.bumper = bumper;
        }

        Sprite getSprite() {
            return sprite;
        }

        Bumper getBumper() {
            return bumper;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Party party = (Party) o;
            return sprite == party.sprite && bumper == party.bumper;
        }

        @Override
        public int hashCode() {
            return Objects.hash(
                    System.identityHashCode(sprite),
                    System.identityHashCode(bumper));
        }

        <T extends Sprite> boolean isSpriteOfClass(Class<T> clz) {
            return sprite.getClass() == clz;
        }

        @Override
        public String toString() {
            return String.format("Party(%s, %s)", sprite, bumper);
        }
    }

}
