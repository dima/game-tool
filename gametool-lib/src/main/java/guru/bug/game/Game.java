package guru.bug.game;

import guru.bug.game.background.FillBackground;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public abstract class Game {
    private final Map<Character, SpriteFactory<? extends Sprite>> spriteFactories = new HashMap<>();
    private final SpriteRegistry registry = new SpriteRegistry();
    private final DoubleProperty width = new SimpleDoubleProperty();
    private final DoubleProperty height = new SimpleDoubleProperty();
    private final ReadOnlyDoubleWrapper mouseX = new ReadOnlyDoubleWrapper();
    private final ReadOnlyDoubleWrapper mouseY = new ReadOnlyDoubleWrapper();
    private final ObjectProperty<Background> background = new SimpleObjectProperty<>(new FillBackground());
    private final Set<KeyCode> keysHold = new HashSet<>();
    private final Set<KeyCode> keysPressed = new HashSet<>();
    private final Set<KeyCode> keysReleased = new HashSet<>();
    private final Set<MouseButton> buttonsHold = new HashSet<>();
    private final Set<MouseButton> buttonsPressed = new HashSet<>();
    private final Set<MouseButton> buttonsReleased = new HashSet<>();
    private final Executor executor = Executors.newWorkStealingPool();
    public final AI ai = new AI() {

        @Override
        protected double getMouseX() {
            return mouseX.get();
        }

        @Override
        protected double getMouseY() {
            return mouseY.get();
        }
    };

    public abstract void setup();

    public abstract void loop();

    void keyPressed(KeyCode key) {
        keysHold.add(key);
        keysPressed.add(key);
    }

    void keyReleased(KeyCode key) {
        keysHold.remove(key);
        keysReleased.add(key);
    }

    void mousePressed(MouseButton button) {
        buttonsHold.add(button);
        buttonsPressed.add(button);
    }

    void mouseReleased(MouseButton button) {
        buttonsHold.remove(button);
        buttonsReleased.add(button);
    }

    @SafeVarargs
    protected final void use(Class<? extends Sprite>... sprites) {
        GamePreloader gp = new GamePreloader(Arrays.asList(sprites));
        executor.execute(gp);
    }

    protected void load(String fileName) {
        load(fileName, null);
    }

    protected void load(String fileName, Runnable setup) {
        registry.scheduleOperation(() -> {
            LevelBuilder builder = new LevelBuilder();
            builder.load(fileName, spriteFactories);
            registry.clear();
            registry.add(builder.getActors());
            width.set(builder.getWidth());
            height.set(builder.getHeight());
            if (setup != null) {
                setup.run();
            }
        });
    }

    protected CollisionSet getCollisions() {
        return registry.getCollisions();
    }

    protected <T extends Sprite> SpriteStateBuilder<T> register(char ch, Supplier<T> spriteSupplier) {
        return register(ch, spriteSupplier, AIEmpty.INSTANCE);
    }

    protected <T extends Sprite> SpriteStateBuilder<T> register(char ch, Supplier<T> spriteSupplier, AIBase ai) {
        ai.init(this);
        SpriteFactory<T> factory = new SpriteFactory<>(ch);
        factory.spriteSupplier(spriteSupplier);
        factory.setLayer(spriteFactories.size());
        factory.setAI(ai);
        spriteFactories.put(ch, factory);
        return factory.getSpriteStateBuilder();
    }

    protected <T extends Sprite> SpriteStateBuilder<T> sprite(Supplier<T> spriteSupplier, double col, double row) {
        return sprite(spriteSupplier, AIEmpty.INSTANCE, col, row);
    }

    protected <T extends Sprite> SpriteStateBuilder<T> sprite(Supplier<T> spriteSupplier, AIBase ai, double col, double row) {
        ai.init(this);
        SpriteFactory<T> factory = new SpriteFactory<>(col, row);
        factory.spriteSupplier(spriteSupplier);
        factory.setLayer(spriteFactories.size());
        factory.setAI(ai);
        //noinspection unchecked
        registry.scheduleAddSprite((SpriteFactory<Sprite>) factory);
        return factory.getSpriteStateBuilder();
    }

    public final void message(String message) {
        registry.doMessage(message);
    }

    protected void updateCollisions() {
        registry.updateCollisions();
    }

    void loop(GraphicsContext gc) {
        loop();
        registry.doLoop();
        registry.doCollisions();
        registry.doTimer();
        registry.doInteractive();
        registry.doKeys(keysHold, keysPressed, keysReleased);
        registry.doMouseButtons(buttonsHold, buttonsPressed, buttonsReleased);
        registry.removeHalted();
        registry.draw(gc);
        keysPressed.clear();
        keysReleased.clear();
        buttonsPressed.clear();
        buttonsReleased.clear();
    }

    public final CollisionSet findCollisions(Sprite sprite) {
        return registry.findCollisions(sprite);
    }

    public final boolean isKeyHold(KeyCode key) {
        return keysHold.contains(key);
    }

    public boolean hasCollision(Sprite sprite, String area) {
        return registry.hasCollision(sprite, area);
    }

    public double getMouseX() {
        return mouseX.get();
    }

    public ReadOnlyDoubleProperty mouseXProperty() {
        return mouseX.getReadOnlyProperty();
    }

    public double getMouseY() {
        return mouseY.get();
    }

    public ReadOnlyDoubleProperty mouseYProperty() {
        return mouseY.getReadOnlyProperty();
    }

    public double getWidth() {
        return width.get();
    }

    public DoubleProperty widthProperty() {
        return width;
    }

    public void setWidth(double width) {
        this.width.set(width);
    }

    public double getHeight() {
        return height.get();
    }

    public DoubleProperty heightProperty() {
        return height;
    }

    public void setHeight(double height) {
        this.height.set(height);
    }

    public Background getBackground() {
        return background.get();
    }

    public ObjectProperty<Background> backgroundProperty() {
        return background;
    }

    public void setBackground(Background background) {
        this.background.set(background);
    }

    void bindMouse(DoubleExpression mouseX, DoubleExpression mouseY) {
        this.mouseX.bind(mouseX);
        this.mouseY.bind(mouseY);
    }
}
