package guru.bug.game;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.geometry.BoundingBox;
import javafx.geometry.Bounds;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public abstract class Background {
    private static final Bounds EMPTY_BOUNDS = new BoundingBox(0, 0, 0, 0);
    private final DoubleProperty gameRatio = new SimpleDoubleProperty();
    private final DoubleProperty width = new SimpleDoubleProperty();
    private final DoubleProperty height = new SimpleDoubleProperty();
    private final DoubleProperty fitWidth = new SimpleDoubleProperty();
    private final DoubleProperty fitHeight = new SimpleDoubleProperty();
    private boolean running;

    private final ObjectExpression<Bounds> clippingBounds = Bindings.createObjectBinding(() -> {
        double iw = getWidth();
        double ih = getHeight();
        double gr = getGameRatio();
        if (iw <= 0 || ih <= 0 || gr <= 0) {
            return EMPTY_BOUNDS;
        }
        double ir = iw / ih;

        double sx = 0;
        double sy = 0;
        double sw = iw;
        double sh = ih;

        if (ir > gr) {
            sw = ih * gr;
            sx = (iw - sw) / 2d;
        } else if (ir < gr) {
            sh = iw / gr;
            sy = (ih - sh) / 2d;
        }

        return new BoundingBox(sx, sy, sw, sh);
    }, width, height, gameRatio);

    private final ObjectExpression<Bounds> drawingBounds = Bindings.createObjectBinding(() -> {
        double fw = getFitWidth();
        double fh = getFitHeight();
        double gr = getGameRatio();
        if (fw <= 0 || fh <= 0 || gr <= 0) {
            return EMPTY_BOUNDS;
        }
        double fr = fw / fh;

        double sx = 0;
        double sy = 0;
        double sw = fw;
        double sh = fh;

        if (fr > gr) {
            sw = fh * gr;
            sx = (fw - sw) / 2d;
        } else if (fr < gr) {
            sh = fw / gr;
            sy = (fh - sh) / 2d;
        }

        return new BoundingBox(sx, sy, sw, sh);
    }, fitWidth, fitHeight, gameRatio);

    private final DoubleExpression scale = Bindings.createDoubleBinding(() -> {
        Bounds cb = clippingBounds.get();
        Bounds db = drawingBounds.get();
        if (cb == EMPTY_BOUNDS || db == EMPTY_BOUNDS) {
            return 1d;
        }
        return db.getWidth() / cb.getWidth();
    }, clippingBounds, drawingBounds);

    protected abstract void startAnimation();

    protected abstract void stopAnimation();

    void start() {
        running = true;
        startAnimation();
    }

    void stop() {
        running = false;
        stopAnimation();
    }

    protected abstract void drawing(Consumer<BackgroundItem> consumer);

    protected void draw(GraphicsContext gc) {
        if (!running) {
            return;
        }
        gc.save();
        try {
            Bounds db = drawingBounds.get();
            double ss = scale.get();
            gc.translate(db.getMinX(), db.getMinY());
            gc.scale(ss, ss);
            drawing(i -> drawItem(gc, i));
        } finally {
            gc.restore();
        }
    }

    private void drawItem(GraphicsContext gc, BackgroundItem item) {
        Image img = item.getImage();
        Bounds cb = clippingBounds.get();
        double iX = item.getX();
        double iY = item.getY();
        double cMinX = cb.getMinX();
        double cMinY = cb.getMinY();
        double iMinX = Math.max(iX, cMinX);
        double iMinY = Math.max(iY, cMinY);
        double iMaxX = Math.min(iX + item.getWidth(), cb.getMaxX());
        double iMaxY = Math.min(iY + item.getHeight(), cb.getMaxY());
        double iWidth = iMaxX - iMinX;
        double iHeight = iMaxY - iMinY;
        if (iWidth <= 0 || iHeight <= 0) {
            return;
        }
        gc.drawImage(img, iMinX - iX, iMinY - iY, iWidth, iHeight, iMinX - cMinX, iMinY - cMinY, iWidth, iHeight);
    }

    double getGameRatio() {
        return gameRatio.get();
    }

    DoubleProperty gameRatioProperty() {
        return gameRatio;
    }

    void setGameRatio(double gameRatio) {
        this.gameRatio.set(gameRatio);
    }

    public double getWidth() {
        return width.get();
    }

    public DoubleProperty widthProperty() {
        return width;
    }

    public void setWidth(double width) {
        this.width.set(width);
    }

    public double getHeight() {
        return height.get();
    }

    public DoubleProperty heightProperty() {
        return height;
    }

    public void setHeight(double height) {
        this.height.set(height);
    }

    protected double getFitWidth() {
        return fitWidth.get();
    }

    protected DoubleProperty fitWidthProperty() {
        return fitWidth;
    }

    protected void setFitWidth(double fitWidth) {
        this.fitWidth.set(fitWidth);
    }

    protected double getFitHeight() {
        return fitHeight.get();
    }

    protected DoubleProperty fitHeightProperty() {
        return fitHeight;
    }

    protected void setFitHeight(double fitHeight) {
        this.fitHeight.set(fitHeight);
    }
}
