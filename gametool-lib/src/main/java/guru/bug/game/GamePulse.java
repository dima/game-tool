package guru.bug.game;

import guru.bug.game.utils.TimeMetrics;
import javafx.animation.AnimationTimer;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class GamePulse extends AnimationTimer {
    private static final double NANOS_IN_SEC = 1_000_000_000d;
    private static final double TARGET_FRAME_RATE = 60d;
    private static final double MAX_FRAME_DURATION = NANOS_IN_SEC / TARGET_FRAME_RATE;
    private final TimeMetrics frameRateMetrics = new TimeMetrics();
    private final TimeMetrics performanceMetrics = new TimeMetrics();
    private boolean justStarted = true;
    private BiConsumer<Long, Long> pulseHandler;
    private Consumer<Long> startHandler;

    private long prev;

    @Override
    public void handle(long now) {
        long t1 = System.nanoTime();
        if (justStarted) {
            justStarted = false;
            if (startHandler != null) {
                startHandler.accept(now);
            }
        } else {
            frameRateMetrics.add(now - prev);
            if (pulseHandler != null) {
                pulseHandler.accept(now, now - prev);
            }
        }
        prev = now;
        performanceMetrics.add(System.nanoTime() - t1);
    }

    void setPulseHandler(BiConsumer<Long, Long> pulseHandler) {
        this.pulseHandler = pulseHandler;
    }

    void setStartHandler(Consumer<Long> startHandler) {
        this.startHandler = startHandler;
    }

    double getActualFrameRate() {
        return NANOS_IN_SEC / frameRateMetrics.getAverage();
    }

    double getActualPerformance() {
        return performanceMetrics.getAverage() / MAX_FRAME_DURATION;
    }
}
