package guru.bug.game;

import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

import java.util.*;
import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class SpriteState {
    static final String ROOT = "guru.bug.game.sprite.state.ROOT";
    private final Map<String, StateHandlers> allHandlers;
    private final Map<String, StateHandlers> activeHandlers = new LinkedHashMap<>();
    private final Queue<Consumer<Sprite>> activationQueue = new LinkedList<>();


    SpriteState(Map<String, StateHandlers> handlers) {
        this.allHandlers = new HashMap<>();
        handlers.entrySet().forEach(e -> this.allHandlers.put(e.getKey(), e.getValue().clone()));
    }

    void merge(SpriteState states) {
        states.allHandlers.entrySet().forEach(e -> this.allHandlers.merge(e.getKey(), e.getValue().clone(), (o, n) -> {
            if (o == null) {
                return n;
            }
            o.merge(n);
            return o;
        }));
    }

    private void doActivate(Sprite sprite, String stateName) {
        activeHandlers.computeIfAbsent(stateName, k -> {
            StateHandlers handlers = allHandlers.get(stateName);
            if (handlers != null) {
                handlers.doActivate(sprite);
            }
            return handlers;
        });
    }

    private void doDeactivate(Sprite sprite, String stateName) {
        activeHandlers.computeIfPresent(stateName, (k, v) -> {
            v.doDeactivate(sprite);
            return null;
        });
    }

    void scheduleActivate(String stateName) {
        activationQueue.add(s -> doActivate(s, stateName));
    }

    void scheduleDeactivate(String stateName) {
        activationQueue.add(s -> doDeactivate(s, stateName));
    }

    public boolean isActivated(String stateName) {
        return activeHandlers.containsKey(stateName);
    }

    void updateActive(Sprite sprite) {
        while (!activationQueue.isEmpty()) {
            Consumer<Sprite> action = activationQueue.remove();
            action.accept(sprite);
        }
    }

    void doCollisions(Sprite sprite, CollisionSetRoot collisionSet) {
        activeHandlers.values().forEach(h -> h.doCollisions(sprite, collisionSet));
    }

    void doLoop(Sprite sprite) {
        activeHandlers.values().forEach(h -> h.doLoop(sprite));
    }

    void doInteraction(Sprite sprite) {
        activeHandlers.values().forEach(h -> h.doInteraction(sprite));
    }

    void doTimer(Sprite sprite) {
        activeHandlers.values().forEach(h -> h.doTimer(sprite));
    }

    void doKeys(Sprite sprite, Set<KeyCode> keysHold, Set<KeyCode> keysPressed, Set<KeyCode> keysReleased) {
        activeHandlers.values().forEach(h -> h.doKeys(sprite, keysHold, keysPressed, keysReleased));
    }

    void doMouseButtons(Sprite sprite, Set<MouseButton> buttonsHold, Set<MouseButton> buttonsPressed, Set<MouseButton> buttonsReleased) {
        activeHandlers.values().forEach(h -> h.doMouseButtons(sprite, buttonsHold, buttonsPressed, buttonsReleased));
    }

    void doHalt(Sprite sprite) {
        activeHandlers.values().forEach(h -> h.doHalt(sprite));
    }

    void doMessage(Sprite sprite, String message) {
        activeHandlers.values().forEach(h -> h.doMessage(sprite, message));
    }

    void doEvent(Sprite sprite, CustomEvent event) {
        activeHandlers.values().forEach(h -> h.doEvent(sprite, event));
    }
}
