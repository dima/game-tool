package guru.bug.game;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class CollisionHandlerInfo {
    private final Set<Class<? extends Sprite>> expectedClasses;
    private final Set<Character> expectedSymbols;
    private final BiConsumer<Sprite, CollisionSet> handler;

    CollisionHandlerInfo(BiConsumer<Sprite, CollisionSet> handler, Class<? extends Sprite>[] collidedClasses) {
        this.handler = handler;
        Set<Class<? extends Sprite>> tmp = new HashSet<>(Arrays.asList(collidedClasses));
        expectedClasses = Collections.unmodifiableSet(tmp);
        expectedSymbols = null;
    }

    CollisionHandlerInfo(BiConsumer<Sprite, CollisionSet> handler, char[] symbols) {
        this.handler = handler;
        Set<Character> tmp;
        expectedClasses = null;
        if (symbols == null || symbols.length == 0) {
            tmp = Collections.emptySet();
        } else {
            tmp = new HashSet<>();
            for (char c : symbols) {
                tmp.add(c);
            }
            tmp = Collections.unmodifiableSet(tmp);
        }
        expectedSymbols = tmp;
    }

    CollisionHandlerInfo(BiConsumer<Sprite, CollisionSet> handler, Collection<Character> symbols) {
        this.handler = handler;
        Set<Character> tmp;
        expectedClasses = null;
        if (symbols == null || symbols.size() == 0) {
            tmp = Collections.emptySet();
        } else {
            tmp = new HashSet<>();
            for (char c : symbols) {
                tmp.add(c);
            }
            tmp = Collections.unmodifiableSet(tmp);
        }
        expectedSymbols = tmp;
    }

    CollisionHandlerInfo(Consumer<Sprite> handler, Class<? extends Sprite>[] collidedClasses) {
        this((s, c) -> handler.accept(s), collidedClasses);
    }

    CollisionHandlerInfo(Consumer<Sprite> handler, char[] symbols) {
        this((s, c) -> handler.accept(s), symbols);
    }

    Set<Class<? extends Sprite>> getExpectedClasses() {
        return expectedClasses;
    }

    Set<Character> getExpectedSymbols() {
        return expectedSymbols;
    }

    boolean isSymbolBased() {
        return expectedSymbols != null;
    }

    BiConsumer<Sprite, CollisionSet> getHandler() {
        return handler;
    }
}
