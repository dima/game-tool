package guru.bug.game;

import java.util.function.Supplier;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class SpriteFactory<T extends Sprite> {
    private Supplier<T> spriteSupplier;
    private final SpriteStateBuilder<T> spriteStateBuilder = new SpriteStateBuilder<>();
    private int layer;
    private double x;
    private double y;
    private boolean isCellPosition;
    private Character symbol;
    private AIBase ai;

    SpriteFactory(Character symbol) {
        this.isCellPosition = true;
        this.symbol = symbol;
    }

    SpriteFactory(double x, double y) {
        this.x = x;
        this.y = y;
        this.isCellPosition = false;
    }

    void spriteSupplier(Supplier<T> spriteSupplier) {
        this.spriteSupplier = spriteSupplier;
    }

    int getLayer() {
        return layer;
    }

    void setLayer(int layer) {
        this.layer = layer;
    }

    T create() {
        return create(x, y);
    }

    T create(double col, double row) {
        T result = spriteSupplier.get();
        result.setLayer(layer);
        if (isCellPosition) {
            result.setPosCell(col, row);
        } else {
            result.setX(col);
            result.setY(row);
        }
        result.init(symbol);
        result.init(ai);
        SpriteState ss = ai.buildState();
        ss.merge(spriteStateBuilder.build());
        result.init(ss);
        result.activate(SpriteState.ROOT);
        ai.setup(result);
        result.setup();
        return result;
    }

    SpriteStateBuilder<T> getSpriteStateBuilder() {
        return spriteStateBuilder;
    }

    public AIBase getAI() {
        return ai;
    }

    void setAI(AIBase ai) {
        this.ai = ai;
    }
}
