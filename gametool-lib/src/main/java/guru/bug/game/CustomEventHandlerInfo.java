package guru.bug.game;

import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class CustomEventHandlerInfo {
    private final CustomEvent event;
    private final Consumer<Sprite> handler;

    CustomEventHandlerInfo(Consumer<Sprite> handler, CustomEvent event) {
        this.handler = handler;
        this.event = event;
    }

    Consumer<Sprite> getHandler() {
        return handler;
    }

    public CustomEvent getEvent() {
        return event;
    }
}
