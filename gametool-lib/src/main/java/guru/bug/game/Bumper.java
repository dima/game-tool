package guru.bug.game;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class Bumper {
    private double centerX;
    private double centerY;
    private double radius;
    private String areaName;

    public Bumper() {
    }

    public Bumper(double centerX, double centerY, double radius, String areaName) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
        this.areaName = areaName;
    }

    public double getCenterX() {
        return centerX;
    }

    public void setCenterX(double centerX) {
        this.centerX = centerX;
    }

    public double getCenterY() {
        return centerY;
    }

    public void setCenterY(double centerY) {
        this.centerY = centerY;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public boolean intersects(Bumper other) {
        double dx = this.centerX - other.centerX;
        double dy = this.centerY - other.centerY;
        double sr = this.radius + other.radius;
        double distance = dx * dx + dy * dy;
        return distance < sr * sr;
    }

    @Override
    public String toString() {
        return String.format("Bumper(%.3f, %.3f, %.3f, %s)", centerX, centerY, radius, areaName);
    }
}
