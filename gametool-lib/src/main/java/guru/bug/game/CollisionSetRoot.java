package guru.bug.game;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class CollisionSetRoot extends CollisionSet {
    private final Set<Collision> collisions = new HashSet<>(255);

    public void add(Sprite sprite1, Bumper bumper1, Sprite sprite2, Bumper bumper2) {
        Party party1 = new Party(sprite1, bumper1);
        Party party2 = new Party(sprite2, bumper2);
        Collision collision = new Collision(party1, party2);
        collisions.add(collision);
    }

    void clear() {
        collisions.clear();
    }

    @Override
    protected Stream<Collision> collisionStream() {
        return collisions.stream()
                .filter(c -> !c.getParty().getSprite().isHalted() && !c.getCounterparty().getSprite().isHalted());
    }

    @Override
    public boolean isEmpty() {
        return collisions.isEmpty();
    }

    @Override
    public boolean hasCollisions() {
        return !isEmpty();
    }
}
