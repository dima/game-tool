package guru.bug.game;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class AIEmpty extends AIBase {

    static AIEmpty INSTANCE = new AIEmpty();

    @Override
    protected void init(SpriteStateBuilder<Sprite> builder) {

    }

    @Override
    protected void setup(Sprite sprite) {

    }

    @Override
    protected void loop(Sprite sprite) {

    }
}
