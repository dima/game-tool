package guru.bug.game;

import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.*;
import javafx.scene.CacheHint;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class GameEngine extends Group {
    private static final BooleanProperty debug = new SimpleBooleanProperty();
    private static final ReadOnlyLongWrapper now = new ReadOnlyLongWrapper();
    private static final ReadOnlyDoubleWrapper frameDuration = new ReadOnlyDoubleWrapper();
    private final GamePulse pulse = new GamePulse();
    private final Canvas canvas = new Canvas();
    private final ObjectProperty<Game> game = new SimpleObjectProperty<>();
    private final ReadOnlyDoubleWrapper mouseX = new ReadOnlyDoubleWrapper();
    private final ReadOnlyDoubleWrapper mouseY = new ReadOnlyDoubleWrapper();
    private final DoubleExpression gameWidth = Bindings.selectDouble(game, "width");
    private final DoubleExpression gameHeight = Bindings.selectDouble(game, "height");
    private final ObjectExpression<Background> background = Bindings.select(game, "background");
    private final DoubleExpression scale = Bindings.createDoubleBinding(() -> {
        double cw = canvas.getWidth();
        double ch = canvas.getHeight();
        double lw = gameWidth.doubleValue();
        double lh = gameHeight.doubleValue();
        return Math.min(cw / lw, ch / lh);
    }, canvas.widthProperty(), canvas.heightProperty(), gameWidth, gameHeight);
    private final DoubleExpression translateX = Bindings.createDoubleBinding(() -> {
        double cw = canvas.getWidth();
        double lw = gameWidth.doubleValue();
        double ss = scale.get();
        return (cw - lw * ss) / 2.0d;
    }, scale, canvas.widthProperty(), gameWidth);
    private final DoubleExpression translateY = Bindings.createDoubleBinding(() -> {
        double ch = canvas.getHeight();
        double lh = gameHeight.doubleValue();
        double ss = scale.get();
        return (ch - lh * ss) / 2.0d;
    }, scale, canvas.heightProperty(), gameHeight);
    private final DoubleExpression translatedMouseX = Bindings.createDoubleBinding(() -> {
        double tx = translateX.get();
        double mx = mouseX.get();
        double ss = scale.get();
        return (mx - tx) / ss;
    }, mouseX, scale, translateX);
    private final DoubleExpression translatedMouseY = Bindings.createDoubleBinding(() -> {
        double ty = translateY.get();
        double my = mouseY.get();
        double ss = scale.get();
        return (my - ty) / ss;
    }, mouseY, scale, translateY);
    private final DoubleExpression ratio = Bindings.createDoubleBinding(() -> {
        double gw = gameWidth.get();
        double gh = gameHeight.get();
        return gw / gh;
    }, gameWidth, gameHeight);

    GameEngine(Game game) {
        canvas.setCacheHint(CacheHint.SPEED);
        canvas.setCache(false);
        frameDuration.set(1.0d / 60.0d);
        setAutoSizeChildren(false);
        getChildren().add(canvas);
        setGame(game);
        pulse.setPulseHandler(this::handlePulse);
        pulse.setStartHandler(this::handleStart);
        canvas.setOnMouseMoved(this::mouseMoved);
        canvas.setOnMouseDragged(this::mouseMoved);
        game.bindMouse(translatedMouseX, translatedMouseY);
        background.addListener((o, ov, nv) -> updateBackground(ov, nv));
        updateBackground(background.get(), background.get());
    }

    private void updateBackground(Background ov, Background nv) {
        if (ov != null) {
            ov.stop();
            ov.gameRatioProperty().unbind();
            ov.fitWidthProperty().unbind();
            ov.fitHeightProperty().unbind();
        }
        if (nv != null) {
            nv.gameRatioProperty().bind(ratio);
            nv.fitWidthProperty().bind(canvas.widthProperty());
            nv.fitHeightProperty().bind(canvas.heightProperty());
            nv.start();
        }
    }

    private void mouseMoved(MouseEvent e) {
        mouseX.set(e.getSceneX());
        mouseY.set(e.getSceneY());
    }

    private void handleStart(long now) {
        GameEngine.now.set(now);
        game.get().setup();
    }

    void bindSize(DoubleExpression width, DoubleExpression height) {
        canvas.setWidth(width.get());
        canvas.setHeight(height.get());
        width.addListener((o, ov, nv) -> canvas.setWidth(nv.doubleValue()));
        height.addListener((o, ov, nv) -> canvas.setHeight(nv.doubleValue()));
    }

    private void handlePulse(long now, long delta) {
        GameEngine.now.set(now);
        GraphicsContext gc = canvas.getGraphicsContext2D();
        double cw = canvas.getWidth();
        double ch = canvas.getHeight();
        gc.clearRect(0, 0, cw, ch);
        gc.setFont(Font.font(10));
        Background bkgnd = background.get();
        if (bkgnd != null) {
            bkgnd.draw(gc);
        }
        gc.save();
        double scale = this.scale.get();
        double tx = this.translateX.get();
        double ty = this.translateY.get();
        gc.translate(tx, ty);
        gc.scale(scale, scale);
        game.get().loop(gc);
        gc.restore();
        drawPerformance(gc);
    }

    private void drawPerformance(GraphicsContext gc) {
        double ch = canvas.getHeight();
        double performance = pulse.getActualPerformance();
        double greenPerf = Math.min(performance, 0.5d);
        double yellowPerf = Math.min(performance, 0.75d);
        double redPerf = Math.min(performance, 1d);
        if (redPerf > 0.75d) {
            gc.setFill(Color.LIGHTPINK);
            gc.fillRect(0.5, ch - 12.5, 32 * redPerf, 12);
        }
        if (yellowPerf > 0.5d) {
            gc.setFill(Color.LIGHTYELLOW);
            gc.fillRect(0.5, ch - 12.5, 32 * yellowPerf, 12);
        }
        gc.setFill(Color.LIGHTGREEN);
        gc.fillRect(0.5, ch - 12.5, 32 * greenPerf, 12);
        gc.setStroke(Color.GREEN);
        gc.strokeText(String.format("%.1f", pulse.getActualFrameRate()), 5, ch - 3);
        gc.strokeRect(0.5, ch - 12.5, 32, 12);
    }

    void start() {
        pulse.start();
    }

    void stop() {
        pulse.stop();
    }

    static long getNow() {
        return now.get();
    }

    static ReadOnlyLongProperty nowProperty() {
        return now.getReadOnlyProperty();
    }

    public Game getGame() {
        return game.get();
    }

    public ObjectProperty<Game> gameProperty() {
        return game;
    }

    public void setGame(Game game) {
        this.game.set(game);
    }

    public static double getFrameDuration() {
        return frameDuration.get();
    }

    public static ReadOnlyDoubleProperty frameDurationProperty() {
        return frameDuration.getReadOnlyProperty();
    }

    @SuppressWarnings("WeakerAccess")
    public static boolean isDebug() {
        return debug.get();
    }

    public static BooleanProperty debugProperty() {
        return debug;
    }

    @SuppressWarnings("WeakerAccess")
    public static void setDebug(boolean debug) {
        GameEngine.debug.set(debug);
    }

    void keyPressed(KeyCode code) {
        Game g = game.get();
        if (g != null) {
            g.keyPressed(code);
        }
    }

    void keyReleased(KeyCode code) {
        Game g = game.get();
        if (g != null) {
            g.keyReleased(code);
        }
    }

    public void mousePressed(MouseButton button) {
        Game g = game.get();
        if (g != null) {
            g.mousePressed(button);
        }
    }

    public void mouseReleased(MouseButton button) {
        Game g = game.get();
        if (g != null) {
            g.mouseReleased(button);
        }
    }
}
