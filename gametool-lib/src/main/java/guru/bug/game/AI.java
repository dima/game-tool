package guru.bug.game;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.ObjectExpression;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.util.Duration;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public abstract class AI {

    public <T extends Sprite> void halt(T sprite) {
        sprite.halt();
    }

    public <T extends Sprite> void followTargetX(T sprite) {
        double fdur = GameEngine.getFrameDuration();
        double x = sprite.getX();
        double dir = sprite.getTargetX() - x;
        if (Math.abs(dir) < 0.25) {
            dir = 0;
        } else {
            dir = dir < 0 ? -1 : 1;
        }
        sprite.setX(x + sprite.getSpeed() * fdur * dir);
    }

    public <T extends Sprite> void followTargetY(T sprite) {
        double fdur = GameEngine.getFrameDuration();
        double y = sprite.getY();
        double dir = sprite.getTargetY() - y;
        if (Math.abs(dir) < 0.25) {
            dir = 0;
        } else {
            dir = dir < 0 ? -1 : 1;
        }
        sprite.setY(y + sprite.getSpeed() * fdur * dir);
    }

    public <T extends Sprite> void followMouseX(T sprite) {
        double fdur = GameEngine.getFrameDuration();
        double x = sprite.getX();
        double dir = getMouseX() - x;
        if (Math.abs(dir) < 0.25) {
            dir = 0;
        } else {
            dir = dir < 0 ? -1 : 1;
        }
        sprite.setX(x + sprite.getSpeed() * fdur * dir);
    }

    public <T extends Sprite> void followMouseY(T sprite) {
        double fdur = GameEngine.getFrameDuration();
        double y = sprite.getY();
        double dir = getMouseY() - y;
        if (Math.abs(dir) < 0.25) {
            dir = 0;
        } else {
            dir = dir < 0 ? -1 : 1;
        }
        sprite.setY(y + sprite.getSpeed() * fdur * dir);
    }

    public <T extends Sprite> void followDirection(T sprite) {
        double fdur = GameEngine.getFrameDuration();
        Direction dir = sprite.getDirection();
        double spd = sprite.getSpeed();
        double x = sprite.getX() + dir.getCos() * spd * fdur;
        double y = sprite.getY() + dir.getSin() * spd * fdur;
        sprite.setX(x);
        sprite.setY(y);
    }

    public <T extends Sprite> void stopXY(T sprite, CollisionSet collisionSet) {
        Result result = collisionStat(collisionSet);
        sprite.setX(sprite.getX() + result.maxXLen);
        sprite.setY(sprite.getY() + result.maxYLen);
    }

    public <T extends Sprite> void stopX(T sprite, CollisionSet collisionSet) {
        Result result = collisionStat(collisionSet);
        sprite.setX(sprite.getX() + result.maxXLen);
    }

    public <T extends Sprite> void stopY(T sprite, CollisionSet collisionSet) {
        Result result = collisionStat(collisionSet);
        sprite.setY(sprite.getY() + result.maxYLen);
    }

    public <T extends Sprite> void bounce(T sprite, CollisionSet collisionSet) {
        Result result = collisionStat(collisionSet);
        sprite.setX(sprite.getX() + result.maxXLen);
        sprite.setY(sprite.getY() + result.maxYLen);
        double midAngle = (result.maxAngle - result.minAngle) / 2d + result.minAngle;
        Direction fallDir = Direction.findClosest(midAngle);
        Direction bounceDir = fallDir.bounce(sprite.getDirection());
        // This is a workaround to prevent ball to move horizontally or vertically.
        if (bounceDir == Direction.W
                || bounceDir == Direction.E
                || bounceDir == Direction.N
                || bounceDir == Direction.S) {
            bounceDir = bounceDir.subtract(Direction.ESE);
        }
        sprite.setDirection(bounceDir);
    }

    protected abstract double getMouseX();

    protected abstract double getMouseY();

    public static Result collisionStat(CollisionSet collisionSet) {
        Result result = new Result();
        collisionSet.findBumpers((b1, b2) -> {
            double x1 = b1.getCenterX();
            double y1 = b1.getCenterY();
            double r1 = b1.getRadius();
            double x2 = b2.getCenterX();
            double y2 = b2.getCenterY();
            double r2 = b2.getRadius();
            double rad = Direction.calcAngle(x1, y1, x2, y2);
            if (rad > result.maxAngle) {
                result.maxAngle = rad;
            }
            if (rad < result.minAngle) {
                result.minAngle = rad;
            }
            double ox1 = x1 + Math.cos(rad) * r1;
            double oy1 = y1 + Math.sin(rad) * r1;
            double rad180 = rad + Math.PI;
            double ox2 = x2 + Math.cos(rad180) * r2;
            double oy2 = y2 + Math.sin(rad180) * r2;
            double xLen = ox2 - ox1;
            double yLen = oy2 - oy1;
            if (Math.abs(result.maxXLen) < Math.abs(xLen)) {
                result.maxXLen = xLen;
            }
            if (Math.abs(result.maxYLen) < Math.abs(yLen)) {
                result.maxYLen = yLen;
            }
        });
        return result;
    }

    public void followMouseXY(Sprite sprite) {
        followMouseX(sprite);
        followMouseY(sprite);
    }

    public void turnToMouse(Sprite sprite) {
        double rad = Direction.calcAngle(sprite.getX(), sprite.getY(), getMouseX(), getMouseY());
        Direction dir = Direction.findClosest(rad);
        sprite.setRotation(dir);
    }

    public void turnToDirection(Sprite sprite) {
        Direction target = sprite.getDirection();
        Timeline timeline = new Timeline();
        double startAngl = sprite.getRotation().getAngle();
        double targetAngl = Direction.shortRotation(startAngl, target.getAngle());
        DoubleProperty prop = new SimpleDoubleProperty(startAngl);
        ObjectExpression<Direction> dir = Bindings.createObjectBinding(() -> Direction.findClosest(prop.get()), prop);
        sprite.rotationProperty().unbind();
        sprite.rotationProperty().bind(dir);
        KeyFrame f = new KeyFrame(Duration.millis(100), new KeyValue(prop, targetAngl));
        timeline.getKeyFrames().add(f);
        timeline.setAutoReverse(false);
        timeline.setCycleCount(1);
        timeline.play();
    }

    public static class Result {
        private double maxAngle = Double.MIN_VALUE;
        private double minAngle = Double.MAX_VALUE;
        private double maxXLen = Double.MIN_VALUE;
        private double maxYLen = Double.MIN_VALUE;

        public double getMaxAngle() {
            return maxAngle;
        }

        public double getMinAngle() {
            return minAngle;
        }

        public double getMaxXLen() {
            return maxXLen;
        }

        public double getMaxYLen() {
            return maxYLen;
        }
    }

}
