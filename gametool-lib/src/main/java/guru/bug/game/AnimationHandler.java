package guru.bug.game;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class AnimationHandler {
    private List<Runnable> finishHandlers;

    public AnimationHandler onFinish(Runnable handler) {
        if (finishHandlers == null) {
            finishHandlers = new ArrayList<>(2);
        }
        finishHandlers.add(handler);
        return this;
    }

    public void fire() {
        if (finishHandlers == null || finishHandlers.isEmpty()) {
            return;
        }
        finishHandlers.forEach(Runnable::run);
    }
}
