package guru.bug.game;

import javafx.scene.input.MouseButton;

import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class MouseButtonHandlerInfo {
    private final Consumer<Sprite> handler;
    private final MouseButton button;

    MouseButtonHandlerInfo(Consumer<Sprite> handler, MouseButton button) {
        this.handler = handler;
        this.button = button;
    }

    MouseButton getButton() {
        return button;
    }

    Consumer<Sprite> getHandler() {
        return handler;
    }

}
