package guru.bug.game;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class SpritePainterCache {
    private static final Map<String, SpritePainter> cache = new ConcurrentHashMap<>(10);

    public static SpritePainter get(String path) {
        return cache.computeIfAbsent(path, SpritePainterCache::load);
    }

    private static SpritePainter load(String path) {
        SpritePainter result = new SpritePainter(path);
        result.load();
        return result;
    }
}
