package guru.bug.game;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class UserData {
    private final Map<String, Object> data = new HashMap<>(2);

    public void putInt(String name, int value) {
        data.put(name, value);
    }

    public int getInt(String name) {
        return (int) data.getOrDefault(name, 0);
    }

    public void putDouble(String name, double value) {
        data.put(name, value);
    }

    public double getDouble(String name) {
        return (double) data.getOrDefault(name, 0d);
    }
}
