package guru.bug.game;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class SimpleTimer {
    private static final double NANOS_IN_SEC = 1e+9;
    private long alarmAt;
    private boolean enabled;

    public void stop() {
        enabled = false;
    }

    public void set(double seconds) {
        long nanos = (long) (seconds * NANOS_IN_SEC);
        alarmAt = GameEngine.getNow() + nanos;
        enabled = true;
    }

    public boolean isAlarmed() {
        return enabled && GameEngine.getNow() >= alarmAt;
    }
}
