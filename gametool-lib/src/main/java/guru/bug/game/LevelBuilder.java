package guru.bug.game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.*;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class LevelBuilder {
    private List<Sprite> sprites;
    private double width;
    private double height;

    void load(String fileName, Map<Character, SpriteFactory<? extends Sprite>> suppliers) {
        char[][] map = readMap(fileName);
        int colCount = map.length;
        int rowCount = map[0].length;
        sprites = new ArrayList<>(colCount * rowCount);
        for (int row = 0; row < rowCount; row++) {
            for (int col = 0; col < colCount; col++) {
                char ch = map[col][row];
                if (ch == ' ') {
                    continue;
                }
                SpriteFactory<? extends Sprite> factory = suppliers.get(ch);
                if (factory == null) {
                    throw new NoSuchElementException("[" + ch + "]");
                }
                Sprite sprite = factory.create(col, row);
                sprites.add(sprite);
            }
        }
        this.width = colCount;
        this.height = rowCount;
    }

    private static int findWidth(char[][] map, int startCol, int startRow) {
        int width = 1;
        while (startCol + width < map.length && map[startCol + width][startRow] == '-') {
            map[startCol + width][startRow] = ' ';
            width++;
        }
        return width;
    }

    private static int findHeight(char[][] map, int startCol, int startRow) {
        int height = 1;
        while (startRow + height < map[startCol].length && map[startCol][startRow + height] == '|') {
            map[startCol][startRow + height] = ' ';
            height++;
        }
        return height;
    }

    private static char[][] readMap(String fileName) {
        try {
            List<String> lines = readAllLines(fileName);
            int height = lines.size();
            int width = lines.stream().mapToInt(String::length).max().orElse(0);
            char[][] map = new char[width][height];
            Iterator<String> lineIter = lines.iterator();
            for (int row = 0; row < height; row++) {
                String line = lineIter.next();
                CharacterIterator charIter = new StringCharacterIterator(line);
                for (int col = 0; col < width; col++) {
                    char ch = charIter.current();
                    if (ch == CharacterIterator.DONE || Character.isWhitespace(ch)) {
                        ch = ' ';
                    }
                    map[col][row] = ch;
                    charIter.next();
                }
            }
            return map;
        } catch (IOException e) {
            throw new GameException(e);
        }
    }

    private static List<String> readAllLines(String fileName) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(GameApplication.game.getClass().getResourceAsStream(fileName)))) {
            List<String> lines = new ArrayList<>(100);
            String line;
            while ((line = reader.readLine()) != null) {
                lines.add(line);
            }
            return lines;
        }
    }

    List<Sprite> getActors() {
        return sprites;
    }

    double getWidth() {
        return width;
    }

    double getHeight() {
        return height;
    }
}
