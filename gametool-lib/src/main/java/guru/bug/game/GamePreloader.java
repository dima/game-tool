package guru.bug.game;

import javafx.concurrent.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class GamePreloader extends Task<Void> {
    private final List<Class<? extends Sprite>> toLoad;

    GamePreloader(Collection<Class<? extends Sprite>> toLoad) {
        this.toLoad = new ArrayList<>(toLoad);
    }

    @Override
    protected Void call() throws Exception {
        updateProgress(0, toLoad.size());
        for (int i = 0; i < toLoad.size(); i++) {
            Class<? extends Sprite> clz = toLoad.get(i);
            Painter p = clz.getAnnotation(Painter.class);
            if (p != null) {
                String path = p.value();
                SpritePainterCache.get(path);
            }
            updateProgress(i, toLoad.size());
        }
        return null;
    }
}
