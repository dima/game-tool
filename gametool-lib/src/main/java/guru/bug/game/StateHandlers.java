package guru.bug.game;

import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
class StateHandlers implements Cloneable {
    private boolean initialized;
    private List<Consumer<Sprite>> initHandlerList;
    private List<Consumer<Sprite>> activationHandlerList;
    private List<Consumer<Sprite>> deactivationHandlerList;
    private List<Consumer<Sprite>> loopHandlerList;
    private List<Consumer<Sprite>> haltHandlerList;
    private List<Consumer<Sprite>> interactionHandlerList;
    private List<Consumer<Sprite>> timerHandlerList;
    private List<CollisionHandlerInfo> collisionHandlerList;
    private List<KeyboardHandlerInfo> keyHoldHandlerList;
    private List<KeyboardHandlerInfo> keyPressedHandlerList;
    private List<KeyboardHandlerInfo> keyReleasedHandlerList;
    private List<MouseButtonHandlerInfo> mouseButtonHoldHandlerList;
    private List<MouseButtonHandlerInfo> mouseButtonPressedHandlerList;
    private List<MouseButtonHandlerInfo> mouseButtonReleasedHandlerList;
    private List<MessageHandlerInfo> messageHandlerList;
    private List<CustomEventHandlerInfo> eventHandlerList;

    @Override
    public StateHandlers clone() {
        try {
            StateHandlers result = (StateHandlers) super.clone();
            if (initHandlerList != null) {
                result.initHandlerList = new ArrayList<>(initHandlerList);
            }
            if (activationHandlerList != null) {
                result.activationHandlerList = new ArrayList<>(activationHandlerList);
            }
            if (deactivationHandlerList != null) {
                result.deactivationHandlerList = new ArrayList<>(deactivationHandlerList);
            }
            if (loopHandlerList != null) {
                result.loopHandlerList = new ArrayList<>(loopHandlerList);
            }
            if (haltHandlerList != null) {
                result.haltHandlerList = new ArrayList<>(haltHandlerList);
            }
            if (interactionHandlerList != null) {
                result.interactionHandlerList = new ArrayList<>(interactionHandlerList);
            }
            if (timerHandlerList != null) {
                result.timerHandlerList = new ArrayList<>(timerHandlerList);
            }
            if (collisionHandlerList != null) {
                result.collisionHandlerList = new ArrayList<>(collisionHandlerList);
            }
            if (keyHoldHandlerList != null) {
                result.keyHoldHandlerList = new ArrayList<>(keyHoldHandlerList);
            }
            if (keyPressedHandlerList != null) {
                result.keyPressedHandlerList = new ArrayList<>(keyPressedHandlerList);
            }
            if (keyReleasedHandlerList != null) {
                result.keyReleasedHandlerList = new ArrayList<>(keyReleasedHandlerList);
            }
            if (mouseButtonHoldHandlerList != null) {
                result.mouseButtonHoldHandlerList = new ArrayList<>(mouseButtonHoldHandlerList);
            }
            if (mouseButtonPressedHandlerList != null) {
                result.mouseButtonPressedHandlerList = new ArrayList<>(mouseButtonPressedHandlerList);
            }
            if (mouseButtonReleasedHandlerList != null) {
                result.mouseButtonReleasedHandlerList = new ArrayList<>(mouseButtonReleasedHandlerList);
            }
            if (messageHandlerList != null) {
                result.messageHandlerList = new ArrayList<>(messageHandlerList);
            }
            if (eventHandlerList != null) {
                result.eventHandlerList = new ArrayList<>(eventHandlerList);
            }
            return result;
        } catch (CloneNotSupportedException e) {
            throw new AssertionError("Should never happen");
        }
    }

    void onInit(Consumer<Sprite> handler) {
        if (initHandlerList == null) {
            initHandlerList = new ArrayList<>(4);
        }
        initHandlerList.add(handler);
    }

    void onActivation(Consumer<Sprite> handler) {
        if (activationHandlerList == null) {
            activationHandlerList = new ArrayList<>(4);
        }
        activationHandlerList.add(handler);
    }

    void onDeactivation(Consumer<Sprite> handler) {
        if (deactivationHandlerList == null) {
            deactivationHandlerList = new ArrayList<>(4);
        }
        deactivationHandlerList.add(handler);
    }

    void onLoop(Consumer<Sprite> handler) {
        if (loopHandlerList == null) {
            loopHandlerList = new ArrayList<>(4);
        }
        loopHandlerList.add(handler);
    }

    void onInteraction(Consumer<Sprite> handler) {
        if (interactionHandlerList == null) {
            interactionHandlerList = new ArrayList<>(4);
        }
        interactionHandlerList.add(handler);
    }

    void onTimer(Consumer<Sprite> handler) {
        if (timerHandlerList == null) {
            timerHandlerList = new ArrayList<>(4);
        }
        timerHandlerList.add(handler);
    }

    void onHalt(Consumer<Sprite> handler) {
        if (haltHandlerList == null) {
            haltHandlerList = new ArrayList<>(4);
        }
        haltHandlerList.add(handler);
    }

    @SafeVarargs
    final void onCollision(BiConsumer<Sprite, CollisionSet> handler, Class<? extends Sprite>... collidedClasses) {
        CollisionHandlerInfo info = new CollisionHandlerInfo(handler, collidedClasses);
        addCollisionHandler(info);
    }

    @SafeVarargs
    final void onCollision(Consumer<Sprite> handler, Class<? extends Sprite>... collidedClasses) {
        CollisionHandlerInfo info = new CollisionHandlerInfo(handler, collidedClasses);
        addCollisionHandler(info);
    }

    final void onCollision(BiConsumer<Sprite, CollisionSet> handler, char... symbols) {
        CollisionHandlerInfo info = new CollisionHandlerInfo(handler, symbols);
        addCollisionHandler(info);
    }

    final void onCollision(BiConsumer<Sprite, CollisionSet> handler, Collection<Character> symbols) {
        CollisionHandlerInfo info = new CollisionHandlerInfo(handler, symbols);
        addCollisionHandler(info);
    }

    final void onCollision(Consumer<Sprite> handler, char... symbols) {
        CollisionHandlerInfo info = new CollisionHandlerInfo(handler, symbols);
        addCollisionHandler(info);
    }

    private void addCollisionHandler(CollisionHandlerInfo info) {
        if (collisionHandlerList == null) {
            collisionHandlerList = new ArrayList<>(4);
        }
        collisionHandlerList.add(info);
    }

    void onKeyHold(Consumer<Sprite> handler, KeyCode keyCode) {
        if (keyHoldHandlerList == null) {
            keyHoldHandlerList = new ArrayList<>(4);
        }
        KeyboardHandlerInfo info = new KeyboardHandlerInfo(handler, keyCode);
        keyHoldHandlerList.add(info);
    }

    void onKeyPressed(Consumer<Sprite> handler, KeyCode keyCode) {
        if (keyPressedHandlerList == null) {
            keyPressedHandlerList = new ArrayList<>(4);
        }
        KeyboardHandlerInfo info = new KeyboardHandlerInfo(handler, keyCode);
        keyPressedHandlerList.add(info);
    }

    void onKeyReleased(Consumer<Sprite> handler, KeyCode keyCode) {
        if (keyReleasedHandlerList == null) {
            keyReleasedHandlerList = new ArrayList<>(4);
        }
        KeyboardHandlerInfo info = new KeyboardHandlerInfo(handler, keyCode);
        keyReleasedHandlerList.add(info);
    }

    void onMouseButtonHold(Consumer<Sprite> handler, MouseButton mouseButton) {
        if (mouseButtonHoldHandlerList == null) {
            mouseButtonHoldHandlerList = new ArrayList<>(4);
        }
        MouseButtonHandlerInfo info = new MouseButtonHandlerInfo(handler, mouseButton);
        mouseButtonHoldHandlerList.add(info);
    }

    void onMouseButtonPressed(Consumer<Sprite> handler, MouseButton mouseButton) {
        if (mouseButtonPressedHandlerList == null) {
            mouseButtonPressedHandlerList = new ArrayList<>(4);
        }
        MouseButtonHandlerInfo info = new MouseButtonHandlerInfo(handler, mouseButton);
        mouseButtonPressedHandlerList.add(info);
    }

    void onMouseButtonReleased(Consumer<Sprite> handler, MouseButton mouseButton) {
        if (mouseButtonReleasedHandlerList == null) {
            mouseButtonReleasedHandlerList = new ArrayList<>(4);
        }
        MouseButtonHandlerInfo info = new MouseButtonHandlerInfo(handler, mouseButton);
        mouseButtonReleasedHandlerList.add(info);
    }

    void onMessage(Consumer<Sprite> handler, String message) {
        if (messageHandlerList == null) {
            messageHandlerList = new ArrayList<>(4);
        }
        MessageHandlerInfo info = new MessageHandlerInfo(handler, message);
        messageHandlerList.add(info);
    }

    public void onEvent(Consumer<Sprite> handler, CustomEvent event) {
        if (eventHandlerList == null) {
            eventHandlerList = new ArrayList<>(4);
        }
        CustomEventHandlerInfo info = new CustomEventHandlerInfo(handler, event);
        eventHandlerList.add(info);
    }

    void doCollisions(Sprite sprite, CollisionSetRoot collisionSet) {
        if (collisionHandlerList == null || collisionHandlerList.isEmpty()) {
            return;
        }
        for (CollisionHandlerInfo i : collisionHandlerList) {
            CollisionSet cs;
            if (i.isSymbolBased()) {
                cs = collisionSet.subsetBySpriteAndSymbols(sprite, null, i.getExpectedSymbols());
            } else {
                cs = collisionSet.subsetBySpriteAndClasses(sprite, null, i.getExpectedClasses());
            }
            if (!cs.isEmpty()) {
                i.getHandler().accept(sprite, cs);
            }
        }
    }

    private void doInit(Sprite sprite) {
        if (initialized) {
            return;
        }
        initialized = true;
        fire(sprite, initHandlerList);
    }

    void doActivate(Sprite sprite) {
        doInit(sprite);
        fire(sprite, activationHandlerList);
    }

    void doDeactivate(Sprite sprite) {
        fire(sprite, deactivationHandlerList);
    }

    void doLoop(Sprite sprite) {
        fire(sprite, loopHandlerList);
    }

    void doInteraction(Sprite sprite) {
        fire(sprite, interactionHandlerList);
    }

    void doTimer(Sprite sprite) {
        fire(sprite, timerHandlerList);
    }

    void doKeys(Sprite sprite, Set<KeyCode> keysHold, Set<KeyCode> keysPressed, Set<KeyCode> keysReleased) {
        fireKeyboard(sprite, keysHold, keyHoldHandlerList);
        fireKeyboard(sprite, keysPressed, keyPressedHandlerList);
        fireKeyboard(sprite, keysReleased, keyReleasedHandlerList);
    }

    void doMouseButtons(Sprite sprite, Set<MouseButton> buttonsHold, Set<MouseButton> buttonsPressed, Set<MouseButton> buttonsReleased) {
        fireMouse(sprite, buttonsHold, mouseButtonHoldHandlerList);
        fireMouse(sprite, buttonsPressed, mouseButtonPressedHandlerList);
        fireMouse(sprite, buttonsReleased, mouseButtonReleasedHandlerList);
    }

    void doHalt(Sprite sprite) {
        fire(sprite, haltHandlerList);
    }

    void doMessage(Sprite sprite, String message) {
        fireMessage(sprite, message, messageHandlerList);
    }

    void doEvent(Sprite sprite, CustomEvent event) {
        fireEvent(sprite, event, eventHandlerList);
    }

    private void fireKeyboard(Sprite sprite, Set<KeyCode> keyCodes, List<KeyboardHandlerInfo> handlers) {
        if (handlers == null || handlers.isEmpty()) {
            return;
        }
        handlers.stream()
                .filter(i -> keyCodes.contains(i.getKeyCode()))
                .forEach(i -> i.getHandler().accept(sprite));
    }

    private void fireMouse(Sprite sprite, Set<MouseButton> buttons, List<MouseButtonHandlerInfo> handlers) {
        if (handlers == null || handlers.isEmpty()) {
            return;
        }
        handlers.stream()
                .filter(i -> buttons.contains(i.getButton()))
                .forEach(i -> i.getHandler().accept(sprite));
    }

    private void fire(Sprite sprite, List<Consumer<Sprite>> handlers) {
        if (handlers == null || handlers.isEmpty()) {
            return;
        }
        handlers.forEach(c -> c.accept(sprite));
    }

    private void fireMessage(Sprite sprite, String message, List<MessageHandlerInfo> handlers) {
        if (handlers == null || handlers.isEmpty()) {
            return;
        }
        handlers.stream()
                .filter(i -> Objects.equals(i.getMessage(), message))
                .forEach(i -> i.getHandler().accept(sprite));
    }

    private void fireEvent(Sprite sprite, CustomEvent event, List<CustomEventHandlerInfo> handlers) {
        if (handlers == null || handlers.isEmpty()) {
            return;
        }
        handlers.stream()
                .filter(i -> Objects.equals(i.getEvent(), event))
                .forEach(i -> i.getHandler().accept(sprite));
    }

    public void merge(StateHandlers other) {
        if (other.initHandlerList != null) {
            if (this.initHandlerList == null) {
                this.initHandlerList = new ArrayList<>(other.initHandlerList);
            } else {
                this.initHandlerList.addAll(other.initHandlerList);
            }
        }
        if (other.activationHandlerList != null) {
            if (this.activationHandlerList == null) {
                this.activationHandlerList = new ArrayList<>(other.activationHandlerList);
            } else {
                this.activationHandlerList.addAll(other.activationHandlerList);
            }
        }
        if (other.deactivationHandlerList != null) {
            if (this.deactivationHandlerList == null) {
                this.deactivationHandlerList = new ArrayList<>(other.deactivationHandlerList);
            } else {
                this.deactivationHandlerList.addAll(other.deactivationHandlerList);
            }
        }
        if (other.loopHandlerList != null) {
            if (this.loopHandlerList == null) {
                this.loopHandlerList = new ArrayList<>(other.loopHandlerList);
            } else {
                this.loopHandlerList.addAll(other.loopHandlerList);
            }
        }
        if (other.haltHandlerList != null) {
            if (this.haltHandlerList == null) {
                this.haltHandlerList = new ArrayList<>(other.haltHandlerList);
            } else {
                this.haltHandlerList.addAll(other.haltHandlerList);
            }
        }
        if (other.interactionHandlerList != null) {
            if (this.interactionHandlerList == null) {
                this.interactionHandlerList = new ArrayList<>(other.interactionHandlerList);
            } else {
                this.interactionHandlerList.addAll(other.interactionHandlerList);
            }
        }
        if (other.timerHandlerList != null) {
            if (this.timerHandlerList == null) {
                this.timerHandlerList = new ArrayList<>(other.timerHandlerList);
            } else {
                this.timerHandlerList.addAll(other.timerHandlerList);
            }
        }
        if (other.collisionHandlerList != null) {
            if (this.collisionHandlerList == null) {
                this.collisionHandlerList = new ArrayList<>(other.collisionHandlerList);
            } else {
                this.collisionHandlerList.addAll(other.collisionHandlerList);
            }
        }
        if (other.keyHoldHandlerList != null) {
            if (this.keyHoldHandlerList == null) {
                this.keyHoldHandlerList = new ArrayList<>(other.keyHoldHandlerList);
            } else {
                this.keyHoldHandlerList.addAll(other.keyHoldHandlerList);
            }
        }
        if (other.keyPressedHandlerList != null) {
            if (this.keyPressedHandlerList == null) {
                this.keyPressedHandlerList = new ArrayList<>(other.keyPressedHandlerList);
            } else {
                this.keyPressedHandlerList.addAll(other.keyPressedHandlerList);
            }
        }
        if (other.keyReleasedHandlerList != null) {
            if (this.keyReleasedHandlerList == null) {
                this.keyReleasedHandlerList = new ArrayList<>(other.keyReleasedHandlerList);
            } else {
                this.keyReleasedHandlerList.addAll(other.keyReleasedHandlerList);
            }
        }
        if (other.mouseButtonHoldHandlerList != null) {
            if (this.mouseButtonHoldHandlerList == null) {
                this.mouseButtonHoldHandlerList = new ArrayList<>(other.mouseButtonHoldHandlerList);
            } else {
                this.mouseButtonHoldHandlerList.addAll(other.mouseButtonHoldHandlerList);
            }
        }
        if (other.mouseButtonPressedHandlerList != null) {
            if (this.mouseButtonPressedHandlerList == null) {
                this.mouseButtonPressedHandlerList = new ArrayList<>(other.mouseButtonPressedHandlerList);
            } else {
                this.mouseButtonPressedHandlerList.addAll(other.mouseButtonPressedHandlerList);
            }
        }
        if (other.mouseButtonReleasedHandlerList != null) {
            if (this.mouseButtonReleasedHandlerList == null) {
                this.mouseButtonReleasedHandlerList = new ArrayList<>(other.mouseButtonReleasedHandlerList);
            } else {
                this.mouseButtonReleasedHandlerList.addAll(other.mouseButtonReleasedHandlerList);
            }
        }
        if (other.messageHandlerList != null) {
            if (this.messageHandlerList == null) {
                this.messageHandlerList = new ArrayList<>(other.messageHandlerList);
            } else {
                this.messageHandlerList.addAll(other.messageHandlerList);
            }
        }
        if (other.eventHandlerList != null) {
            if (this.eventHandlerList == null) {
                this.eventHandlerList = new ArrayList<>(other.eventHandlerList);
            } else {
                this.eventHandlerList.addAll(other.eventHandlerList);
            }
        }
    }
}
