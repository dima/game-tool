package icehockey;

import guru.bug.game.Direction;
import guru.bug.game.Game;
import guru.bug.game.GameApplication;
import guru.bug.game.Sprite;
import guru.bug.game.ai.GravityAI;
import guru.bug.game.background.VolcanoBackground;
import guru.bug.game.sprite.*;
import javafx.scene.input.KeyCode;

/**
 * @author Dimitrijs Fedotovs <a href="http://www.bug.guru">www.bug.guru</a>
 * @version 1.0
 * @since 1.0
 */
public class IceHockey extends Game {

    private static final char CH_WALL = '#';
    private static final char CH_LADDER = 'H';
    private static final char CH_FIRE = 'W';

    public static void main(String[] args) {
        GameApplication.launch(new IceHockey(), args);
    }

    @Override
    public void setup() {
        setBackground(new VolcanoBackground());
        register(CH_WALL, WallSprite::new)
                .onInit(w -> w.setColor(WallColor.GRAY));
        register(CH_FIRE, FlameSprite::new);
        register(CH_LADDER, LadderSprite::new)
                .onInit(l -> l.setColor(LadderColor.JADE));

        register('1', PortalSprite::new);
        register('d', GhostSprite::new);
        register('k', KeySprite::new);
        register('b', FruitSprite::new);
        register('z', BugSprite::new);
        register('h', HedgehogSprite::new);
        register('c', ChestSprite::new);
        register('s', SwordSprite::new);

        register('m', MonsterSprite::new, new GravityAI().wall(CH_WALL).ladder(CH_LADDER))
                .onInit(m -> m.setOutfit(MonsterOutfit.ROUND_HEAD))
                .onCollision(ai::halt, CH_FIRE)
                .onKeyHold(m -> move(m, Direction.E), KeyCode.RIGHT)
                .onKeyHold(m -> move(m, Direction.W), KeyCode.LEFT)
                .onKeyReleased(m -> m.setSpeed(0), KeyCode.RIGHT)
                .onKeyReleased(m -> m.setSpeed(0), KeyCode.LEFT)
                .defineState(GravityAI.WALK)
                .onKeyHold(m -> move(m, Direction.S), KeyCode.DOWN)
                .onKeyHold(m -> move(m, Direction.N), KeyCode.UP)
                .onKeyReleased(m -> m.setSpeed(0), KeyCode.DOWN)
                .onKeyReleased(m -> m.setSpeed(0), KeyCode.UP)
                .onKeyPressed(m -> m.activate(GravityAI.JUMP), KeyCode.SPACE);

        load("/icehockey/level.txt");
    }

    private void move(Sprite m, Direction e) {
        m.setSpeed(3);
        m.setDirection(e);
        m.setRotation(e);
        ai.followDirection(m);
    }

    @Override
    public void loop() {

    }
}
